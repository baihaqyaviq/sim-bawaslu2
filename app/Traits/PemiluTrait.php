<?php


namespace App\Traits;


use Modules\Pemilu\Entities\Pemilu;

trait PemiluTrait
{
    public function initializePemiluTrait()
    {
        $this->fillable[] = 'pemilu_id';
        $this->appends[] = 'pemilu';
    }

    /**
     * Relation to SimPemilu
     *
     * @return BelongsTo
     */
    public function getPemiluAttribute()
    {
        $data = $this->belongsTo(Pemilu::class, 'pemilu_id', 'id')->first();
        if (!$data) {
            return null;
        }

        return collect([
            'id' => $data->id,
            'nama_pemilu' => $data->nama_pemilu,
            'full_nama' => $data->full_nama
        ]);
    }

}