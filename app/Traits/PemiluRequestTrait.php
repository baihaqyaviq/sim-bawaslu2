<?php


namespace App\Traits;

trait PemiluRequestTrait
{
    public function getPemiluId(){
        if ($this->has('pemilu')){
            return $this->get('pemilu')['id'];
        }
        return null;
    }
}