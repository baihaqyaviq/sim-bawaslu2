<?php


namespace App\Traits;


use App\CertificateCounter;
use Carbon\Carbon;

trait NomoratorTrait
{
    public static function generateNomor($code, $autoIncrement = true)
    {
        $now = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
        $bulan = $now->month;
        $tahun = $now->year;
        $romawi = [
            '1' => 'I', '7' => 'VII',
            '2' => 'II', '8' => 'VIII',
            '3' => 'III', '9' => 'IX',
            '4' => 'IV', '10' => 'X',
            '5' => 'V', '11' => 'XI',
            '6' => 'VI', '12' => 'XII',
        ];

        // todo dari database
        $certificate = CertificateCounter::where('code', $code)->first();
        $nourut = $certificate->current_value + 1;

        if ($now->is('first day of january') && $certificate->year < $tahun) {
            $nourut = 1;
            $certificate->year = $tahun;
        }

        $format = "{$nourut}/{$certificate->code}/PM/{$romawi[$bulan]}/{$tahun}";

        if ($autoIncrement) {
            $certificate->current_value = $nourut;
            $certificate->save();
        }

        return $format;
    }
}