<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateCounter extends Model
{
    protected $table = 'certificate_counter';
}
