<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            {{--pemilu--}}
            <li class="nav-title">{{ trans('pemilu::admin.sidebar') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/pemilu-pemilus') }}"><i
                            class="nav-icon icon-energy"></i> {{ trans('pemilu::admin.pemilu.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/pemilu-parpols') }}"><i
                            class="nav-icon icon-ghost"></i> {{ trans('pemilu::admin.parpol.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/pemilu-dapils') }}"><i
                            class="nav-icon icon-compass"></i> {{ trans('pemilu::admin.dapil.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/pemilu-tps') }}"><i
                            class="nav-icon icon-plane"></i> {{ trans('pemilu::admin.tps.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/pemilu-calons') }}"><i
                            class="nav-icon icon-flag"></i> {{ trans('pemilu::admin.calon.title') }}</a></li>

            {{--laporan--}}
            <li class="nav-title">{{ trans('laporan::admin.sidebar') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/laporan-panwas') }}"><i
                            class="nav-icon icon-compass"></i> {{ trans('laporan::admin.panwas.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/laporan-pengawasans') }}"><i
                            class="nav-icon icon-plane"></i> {{ trans('laporan::admin.pengawasan.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/laporan-pelanggarans') }}"><i
                            class="nav-icon icon-ghost"></i> {{ trans('laporan::admin.pelanggaran.title') }}</a></li>

            {{--situng--}}
            <li class="nav-title">{{ trans('situng::admin.sidebar') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/situng-komponen-hitungs') }}"><i
                            class="nav-icon icon-drop"></i> {{ trans('situng::admin.komponen-hitung.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/situng-hasil-hitungs') }}"><i
                            class="nav-icon icon-plane"></i> {{ trans('situng::admin.hasil-hitung.title') }}</a></li>


            <li class="nav-title">{{ trans('ppid::admin.sidebar') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-articles') }}"><i
                            class="nav-icon icon-diamond"></i> {{ trans('ppid::admin.article.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-categories') }}"><i
                            class="nav-icon icon-plane"></i> {{ trans('ppid::admin.category.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-tags') }}"><i
                            class="nav-icon icon-plane"></i> {{ trans('ppid::admin.tag.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-faq-categories') }}"><i
                            class="nav-icon icon-flag"></i> {{ trans('ppid::admin.faq-category.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-faq-questions') }}"><i
                            class="nav-icon icon-drop"></i> {{ trans('ppid::admin.faq-question.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/ppid-pages') }}"><i class="nav-icon icon-diamond"></i> {{ trans('ppid::admin.page.title') }}</a></li>
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}

            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.settings') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i
                            class="nav-icon icon-user"></i> {{ __('Manage access') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i
                            class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li>
            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            {{--<li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li>--}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
