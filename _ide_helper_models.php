<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\CertificateCounter
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $type
 * @property int|null $current_value
 * @property string|null $year
 * @property string|null $type_letter
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter query()
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereCurrentValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereTypeLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CertificateCounter whereYear($value)
 */
	class CertificateCounter extends \Eloquent {}
}

namespace App{
/**
 * App\RefAgama
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefAgama newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefAgama newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefAgama query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefAgama whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefAgama whereNama($value)
 */
	class RefAgama extends \Eloquent {}
}

namespace App{
/**
 * App\RefJabatan
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefJabatan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJabatan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJabatan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJabatan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefJabatan whereNama($value)
 */
	class RefJabatan extends \Eloquent {}
}

namespace App{
/**
 * App\RefJenjangPendidikan
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefJenjangPendidikan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJenjangPendidikan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJenjangPendidikan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefJenjangPendidikan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefJenjangPendidikan whereNama($value)
 */
	class RefJenjangPendidikan extends \Eloquent {}
}

namespace App{
/**
 * App\RefLembagaPengangkat
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefLembagaPengangkat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLembagaPengangkat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLembagaPengangkat query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLembagaPengangkat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefLembagaPengangkat whereNama($value)
 */
	class RefLembagaPengangkat extends \Eloquent {}
}

namespace App{
/**
 * App\RefLevelWilayah
 *
 * @property int $id_level_wilayah
 * @property string|null $nm_level_wilayah
 * @method static \Illuminate\Database\Eloquent\Builder|RefLevelWilayah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLevelWilayah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLevelWilayah query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefLevelWilayah whereIdLevelWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefLevelWilayah whereNmLevelWilayah($value)
 */
	class RefLevelWilayah extends \Eloquent {}
}

namespace App{
/**
 * App\RefPangkatGolongan
 *
 * @property int $id
 * @property string|null $kode
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefPangkatGolongan whereNama($value)
 */
	class RefPangkatGolongan extends \Eloquent {}
}

namespace App{
/**
 * App\RefPekerjaan
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefPekerjaan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPekerjaan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPekerjaan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPekerjaan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefPekerjaan whereNama($value)
 */
	class RefPekerjaan extends \Eloquent {}
}

namespace App{
/**
 * App\RefPenghasilan
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefPenghasilan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPenghasilan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPenghasilan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefPenghasilan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefPenghasilan whereNama($value)
 */
	class RefPenghasilan extends \Eloquent {}
}

namespace App{
/**
 * App\RefStatusKepegawaian
 *
 * @property int $id
 * @property string $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusKepegawaian newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusKepegawaian newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusKepegawaian query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusKepegawaian whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusKepegawaian whereNama($value)
 */
	class RefStatusKepegawaian extends \Eloquent {}
}

namespace App{
/**
 * App\RefStatusPerkawinan
 *
 * @property int $id
 * @property string|null $nama
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusPerkawinan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusPerkawinan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusPerkawinan query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusPerkawinan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefStatusPerkawinan whereNama($value)
 */
	class RefStatusPerkawinan extends \Eloquent {}
}

namespace App{
/**
 * App\RefWilayah
 *
 * @property int $id_wilayah
 * @property string $kode_wilayah
 * @property string|null $nm_wilayah
 * @property string|null $id_induk_wilayah
 * @property int $id_level_wilayah
 * @property string|null $kodepos
 * @property-read \Illuminate\Database\Eloquent\Collection|RefWilayah[] $children
 * @property-read int|null $children_count
 * @property-read mixed $object
 * @property-read \App\RefLevelWilayah $levelWilayah
 * @property-read RefWilayah|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah query()
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereIdIndukWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereIdLevelWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereIdWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereKodeWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereKodepos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah whereNmWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RefWilayah withParent()
 */
	class RefWilayah extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 */
	class User extends \Eloquent {}
}

