import AppForm from '@/app-components/Form/AppForm';

Vue.component('komponen-hitung-form', {
    mixins: [AppForm],
    props: ['pemilu', 'parent'],
    data: function() {
        return {
            form: {
                parent:  '' ,
                nama:  '' ,
                code:  '' ,
                type:  '' ,
                sort:  '' ,
                group:  '' ,
                pemilu:  '' ,
            }
        }
    }

});