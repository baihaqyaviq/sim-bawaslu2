import AppForm from '@/app-components/Form/AppForm';

Vue.component('hasil-hitung-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function() {
        return {
            form: {
                model_type:  '' ,
                model_id:  '' ,
                hasil:  '' ,
                form_type:  '' ,
                wilayah:  '' ,
                tps:  '' ,
                pemilu:  '' ,
            }
        }
    }

});