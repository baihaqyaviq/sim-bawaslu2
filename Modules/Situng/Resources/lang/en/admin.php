<?php

return [
    'sidebar' => 'Situng',
    'hasil-hitung' => [
        'title' => 'Hasil Hitung',

        'actions' => [
            'index' => 'Hasil Hitung',
            'create' => 'New Hasil Hitung',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'model_type' => 'Model type',
            'model_id' => 'Model',
            'hasil' => 'Hasil',
            'form_type' => 'Form type',
            'id_wilayah' => 'Id wilayah',
            'tps_id' => 'Tps',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    'komponen-hitung' => [
        'title' => 'Komponen Hitung',

        'actions' => [
            'index' => 'Komponen Hitung',
            'create' => 'New Komponen Hitung',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'parent_id' => 'Komponen Induk',
            'nama' => 'Nama',
            'code' => 'Kode',
            'type' => 'Tipe',
            'sort' => 'Urut',
            'group' => 'Group',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];