@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('situng::admin.komponen-hitung.actions.edit', ['name' => $komponenHitung->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <komponen-hitung-form
                    :action="'{{ $komponenHitung->resource_url }}'"
                    :data="{{ $komponenHitung->toJson() }}"
                    :pemilu="{{$pemilu->toJson()}}"
                    :parent="{{$parent->toJson()}}"
                    v-cloak
                    inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                      novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('situng::admin.komponen-hitung.actions.edit', ['name' => $komponenHitung->id]) }}
                    </div>

                    <div class="card-body">
                        @include('situng::admin.komponen-hitung.components.form-elements')
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </komponen-hitung-form>

        </div>

    </div>

@endsection