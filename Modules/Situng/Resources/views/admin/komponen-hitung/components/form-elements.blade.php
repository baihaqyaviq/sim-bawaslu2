<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('parent'), 'has-success': fields.parent && fields.parent.valid }">
    <label for="parent" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.parent_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.parent"
                :options="parent"
                :multiple="false"
                track-by="id"
                label="full_nama"
                tag-placeholder="{{ __('Select Komponen Induk') }}"
                placeholder="{{ __('Nama Komponen Induk') }}">
        </multiselect>

        <div v-if="errors.has('parent')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('parent') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama'), 'has-success': fields.nama && fields.nama.valid }">
    <label for="nama" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.nama') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama'), 'form-control-success': fields.nama && fields.nama.valid}" id="nama" name="nama" placeholder="{{ trans('situng::admin.komponen-hitung.columns.nama') }}">
        <div v-if="errors.has('nama')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('code'), 'has-success': fields.code && fields.code.valid }">
    <label for="code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.code') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input code="text" v-model="form.code" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('code'), 'form-control-success': fields.code && fields.code.valid}" id="code" name="code" placeholder="{{ trans('situng::admin.komponen-hitung.columns.code') }}">
        <div v-if="errors.has('code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('code') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('type'), 'has-success': fields.type && fields.type.valid }">
    <label for="type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.type') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.type" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('type'), 'form-control-success': fields.type && fields.type.valid}" id="type" name="type" placeholder="{{ trans('situng::admin.komponen-hitung.columns.type') }}">
        <div v-if="errors.has('type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('sort'), 'has-success': fields.sort && fields.sort.valid }">
    <label for="sort" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.sort') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input sort="text" v-model="form.sort" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('sort'), 'form-control-success': fields.sort && fields.sort.valid}" id="sort" name="sort" placeholder="{{ trans('situng::admin.komponen-hitung.columns.sort') }}">
        <div v-if="errors.has('sort')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('sort') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('group'), 'has-success': fields.group && fields.group.valid }">
    <label for="group" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.komponen-hitung.columns.group') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input group="text" v-model="form.group" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('group'), 'form-control-success': fields.group && fields.group.valid}" id="group" name="group" placeholder="{{ trans('situng::admin.komponen-hitung.columns.group') }}">
        <div v-if="errors.has('group')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('group') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>

