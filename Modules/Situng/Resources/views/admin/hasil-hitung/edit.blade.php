@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('situng::admin.hasil-hitung.actions.edit', ['name' => $hasilHitung->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <hasil-hitung-form
                    :action="'{{ $hasilHitung->resource_url }}'"
                    :data="{{ $hasilHitung->toJson() }}"
                    :pemilu="{{$pemilu->toJson()}}"
                    v-cloak
                    inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                      novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('situng::admin.hasil-hitung.actions.edit', ['name' => $hasilHitung->id]) }}
                    </div>

                    <div class="card-body">
                        @include('situng::admin.hasil-hitung.components.form-elements')
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </hasil-hitung-form>

        </div>

    </div>

@endsection