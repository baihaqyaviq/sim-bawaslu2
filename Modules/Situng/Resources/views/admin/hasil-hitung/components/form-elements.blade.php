<div class="form-group row align-items-center" :class="{'has-danger': errors.has('model_type'), 'has-success': fields.model_type && fields.model_type.valid }">
    <label for="model_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.model_type') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.model_type" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('model_type'), 'form-control-success': fields.model_type && fields.model_type.valid}" id="model_type" name="model_type" placeholder="{{ trans('situng::admin.hasil-hitung.columns.model_type') }}">
        <div v-if="errors.has('model_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('model_type') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('model_id'), 'has-success': fields.model_id && fields.model_id.valid }">
    <label for="model_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.model_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.model_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('model_id'), 'form-control-success': fields.model_id && fields.model_id.valid}" id="model_id" name="model_id" placeholder="{{ trans('situng::admin.hasil-hitung.columns.model_id') }}">
        <div v-if="errors.has('model_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('model_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('hasil'), 'has-success': fields.hasil && fields.hasil.valid }">
    <label for="hasil" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.hasil') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.hasil" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('hasil'), 'form-control-success': fields.hasil && fields.hasil.valid}" id="hasil" name="hasil" placeholder="{{ trans('situng::admin.hasil-hitung.columns.hasil') }}">
        <div v-if="errors.has('hasil')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('hasil') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('form_type'), 'has-success': fields.form_type && fields.form_type.valid }">
    <label for="form_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.form_type') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.form_type" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('form_type'), 'form-control-success': fields.form_type && fields.form_type.valid}" id="form_type" name="form_type" placeholder="{{ trans('situng::admin.hasil-hitung.columns.form_type') }}">
        <div v-if="errors.has('form_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('form_type') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('id_wilayah'), 'has-success': fields.id_wilayah && fields.id_wilayah.valid }">
    <label for="id_wilayah" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.id_wilayah') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.id_wilayah" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('id_wilayah'), 'form-control-success': fields.id_wilayah && fields.id_wilayah.valid}" id="id_wilayah" name="id_wilayah" placeholder="{{ trans('situng::admin.hasil-hitung.columns.id_wilayah') }}">
        <div v-if="errors.has('id_wilayah')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('id_wilayah') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tps_id'), 'has-success': fields.tps_id && fields.tps_id.valid }">
    <label for="tps_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('situng::admin.hasil-hitung.columns.tps_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tps_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tps_id'), 'form-control-success': fields.tps_id && fields.tps_id.valid}" id="tps_id" name="tps_id" placeholder="{{ trans('situng::admin.hasil-hitung.columns.tps_id') }}">
        <div v-if="errors.has('tps_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tps_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>

