<?php

namespace Modules\Situng\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KomponenHitung extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    public $incrementing = false;

    protected $table = 'sim_komponen_hitung';

    protected $fillable = [
        'parent_id',
        'nama',
        'type',
        'code',
        'sort',
        'group',
    ];


    protected $dates = [
        'tahun',
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url', 'full_nama'];
    protected $with = ['parent'];

    /* ************************ ACCESSOR ************************* */

    public function getFullNamaAttribute()
    {
        return "{$this->code}. {$this->nama}";
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/situng-komponen-hitungs/' . $this->getKey());
    }

    public function parent()
    {
        $dad = $this->belongsTo(KomponenHitung::class, 'parent_id', 'id')
            ->with('parent')
            ->select(['id', 'nama', 'code']);

        return $dad;
    }
}
