<?php

namespace Modules\Situng\Entities;

use App\RefWilayah;
use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Pemilu\Entities\Tps;

class HasilHitung extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    public $incrementing = false;

    protected $table = 'sim_hasil_hitung';

    protected $fillable = [
        'model_type',
        'model_id',
        'hasil',
        'form_type',
        'id_wilayah',
        'tps_id',

    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/situng-hasil-hitungs/' . $this->getKey());
    }

    /* ************************ RELATIONS ************************ */

    /**
     * Relation to SimTps
     *
     * @return BelongsTo
     */
    public function tps()
    {
        return $this->belongsTo(Tps::class, 'tps_id', 'id');
    }

    /**
     * Relation to RefWilayah
     *
     * @return BelongsTo
     */
    public function wilayah()
    {
        return $this->belongsTo(RefWilayah::class, 'id_wilayah', 'id');
    }


}
