<?php

namespace Modules\Situng\Http\Requests\Admin\KomponenHitung;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateKomponenHitung extends FormRequest
{

    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.komponen-hitung.edit', $this->komponenHitung);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent_id' => ['nullable', 'integer'],
            'nama' => ['nullable', 'string'],
            'tipe' => ['nullable', 'string'],
            'tahun' => ['nullable', 'date'],
            'pemilu' => ['required'],
            'user_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
