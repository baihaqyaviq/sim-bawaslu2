<?php
namespace Modules\Situng\Http\Requests\Admin\HasilHitung;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreHasilHitung extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.hasil-hitung.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'model_type' => ['nullable', 'string'],
            'model_id' => ['nullable', 'integer'],
            'hasil' => ['nullable', 'integer'],
            'form_type' => ['nullable', 'string'],
            'id_wilayah' => ['nullable', 'string'],
            'tps_id' => ['nullable', 'integer'],
            'pemilu' => ['required'],
            'user_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
