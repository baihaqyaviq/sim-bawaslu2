<?php

namespace Modules\Situng\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Situng\Http\Requests\Admin\KomponenHitung\BulkDestroyKomponenHitung;
use Modules\Situng\Http\Requests\Admin\KomponenHitung\DestroyKomponenHitung;
use Modules\Situng\Http\Requests\Admin\KomponenHitung\IndexKomponenHitung;
use Modules\Situng\Http\Requests\Admin\KomponenHitung\StoreKomponenHitung;
use Modules\Situng\Http\Requests\Admin\KomponenHitung\UpdateKomponenHitung;
use Modules\Situng\Entities\KomponenHitung;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class KomponenHitungController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexKomponenHitung $request
     * @return array|Factory|View
     */
    public function index(IndexKomponenHitung $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(KomponenHitung::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'parent_id', 'nama', 'code', 'type', 'sort', 'group', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nama']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('situng::admin.komponen-hitung.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.komponen-hitung.create');

        return view('situng::admin.komponen-hitung.create', [
            'pemilu' => Pemilu::all(),
            'parent' => KomponenHitung::whereNull('parent_id')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreKomponenHitung $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreKomponenHitung $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Store the KomponenHitung
        $komponenHitung = KomponenHitung::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/situng-komponen-hitungs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/situng-komponen-hitungs');
    }

    /**
     * Display the specified resource.
     *
     * @param KomponenHitung $komponenHitung
     * @throws AuthorizationException
     * @return void
     */
    public function show(KomponenHitung $komponenHitung)
    {
        $this->authorize('admin.komponen-hitung.show', $komponenHitung);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param KomponenHitung $komponenHitung
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(KomponenHitung $komponenHitung)
    {
        $this->authorize('admin.komponen-hitung.edit', $komponenHitung);

        //dd($komponenHitung->toArray());

        return view('situng::admin.komponen-hitung.edit', [
            'komponenHitung' => $komponenHitung,
            'pemilu' => Pemilu::all(),
            'parent' => KomponenHitung::whereNull('parent_id')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateKomponenHitung $request
     * @param KomponenHitung $komponenHitung
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateKomponenHitung $request, KomponenHitung $komponenHitung)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Update changed values KomponenHitung
        $komponenHitung->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/situng-komponen-hitungs'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/situng-komponen-hitungs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyKomponenHitung $request
     * @param KomponenHitung $komponenHitung
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyKomponenHitung $request, KomponenHitung $komponenHitung)
    {
        $komponenHitung->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyKomponenHitung $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyKomponenHitung $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('komponenHitungs')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
