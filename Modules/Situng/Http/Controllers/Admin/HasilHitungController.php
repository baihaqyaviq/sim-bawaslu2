<?php

namespace Modules\Situng\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Situng\Http\Requests\Admin\HasilHitung\BulkDestroyHasilHitung;
use Modules\Situng\Http\Requests\Admin\HasilHitung\DestroyHasilHitung;
use Modules\Situng\Http\Requests\Admin\HasilHitung\IndexHasilHitung;
use Modules\Situng\Http\Requests\Admin\HasilHitung\StoreHasilHitung;
use Modules\Situng\Http\Requests\Admin\HasilHitung\UpdateHasilHitung;
use Modules\Situng\Entities\HasilHitung;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class HasilHitungController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexHasilHitung $request
     * @return array|Factory|View
     */
    public function index(IndexHasilHitung $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(HasilHitung::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'model_type', 'model_id', 'hasil', 'form_type', 'id_wilayah', 'tps_id', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'model_type', 'form_type', 'id_wilayah']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('situng::admin.hasil-hitung.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.hasil-hitung.create');

        return view('situng::admin.hasil-hitung.create',[
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreHasilHitung $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreHasilHitung $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Store the HasilHitung
        $hasilHitung = HasilHitung::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/situng-hasil-hitungs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/situng-hasil-hitungs');
    }

    /**
     * Display the specified resource.
     *
     * @param HasilHitung $hasilHitung
     * @throws AuthorizationException
     * @return void
     */
    public function show(HasilHitung $hasilHitung)
    {
        $this->authorize('admin.hasil-hitung.show', $hasilHitung);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param HasilHitung $hasilHitung
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(HasilHitung $hasilHitung)
    {
        $this->authorize('admin.hasil-hitung.edit', $hasilHitung);


        return view('situng::admin.hasil-hitung.edit', [
            'hasilHitung' => $hasilHitung,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateHasilHitung $request
     * @param HasilHitung $hasilHitung
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateHasilHitung $request, HasilHitung $hasilHitung)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Update changed values HasilHitung
        $hasilHitung->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/situng-hasil-hitungs'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/situng-hasil-hitungs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyHasilHitung $request
     * @param HasilHitung $hasilHitung
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyHasilHitung $request, HasilHitung $hasilHitung)
    {
        $hasilHitung->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyHasilHitung $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyHasilHitung $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('hasilHitungs')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
