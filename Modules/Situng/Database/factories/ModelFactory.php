<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Situng\Entities\HasilHitung::class, static function (Faker\Generator $faker) {
    return [
        'model_type' => $faker->sentence,
        'model_id' => $faker->randomNumber(5),
        'hasil' => $faker->randomNumber(5),
        'form_type' => $faker->sentence,
        'id_wilayah' => $faker->sentence,
        'tps_id' => $faker->randomNumber(5),
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Situng\Entities\KomponenHitung::class, static function (Faker\Generator $faker) {
    return [
        'parent_id' => $faker->randomNumber(5),
        'nama' => $faker->sentence,
        'tipe' => $faker->sentence,
        'tahun' => $faker->date(),
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
