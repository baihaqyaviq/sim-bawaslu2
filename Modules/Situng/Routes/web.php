<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('situng')->group(function() {
    Route::get('/', 'SitungController@index');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('situng-hasil-hitungs')->name('situng-hasil-hitungs/')->group(static function() {
            Route::get('/',                                             'HasilHitungController@index')->name('index');
            Route::get('/create',                                       'HasilHitungController@create')->name('create');
            Route::post('/',                                            'HasilHitungController@store')->name('store');
            Route::get('/{hasilHitung}/edit',                           'HasilHitungController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'HasilHitungController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{hasilHitung}',                               'HasilHitungController@update')->name('update');
            Route::delete('/{hasilHitung}',                             'HasilHitungController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('situng-komponen-hitungs')->name('situng-komponen-hitungs/')->group(static function() {
            Route::get('/',                                             'KomponenHitungController@index')->name('index');
            Route::get('/create',                                       'KomponenHitungController@create')->name('create');
            Route::post('/',                                            'KomponenHitungController@store')->name('store');
            Route::get('/{komponenHitung}/edit',                        'KomponenHitungController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'KomponenHitungController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{komponenHitung}',                            'KomponenHitungController@update')->name('update');
            Route::delete('/{komponenHitung}',                          'KomponenHitungController@destroy')->name('destroy');
        });
    });
});