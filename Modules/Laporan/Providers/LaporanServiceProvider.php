<?php

namespace Modules\Laporan\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class LaporanServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Laporan', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Laporan', 'Config/config.php') => config_path('laporan.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Laporan', 'Config/config.php'), 'laporan'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/laporan');

        $sourcePath = module_path('Laporan', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');


        /**
         * modified
         * @see https://github.com/nWidart/laravel-modules/issues/642
         */
        $this->loadViewsFrom(array_filter(array_merge(array_map(function ($path) {
            return $path . '/modules/laporan';
        }, \Config::get('view.paths')), [$sourcePath]), function ($path) {
            return file_exists($path);
        }), 'laporan');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/laporan');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'laporan');
        } else {
            $this->loadTranslationsFrom(module_path('Laporan', 'Resources/lang'), 'laporan');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Laporan', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
