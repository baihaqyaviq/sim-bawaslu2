<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('laporan')->group(function() {
    Route::get('/', 'LaporanController@index');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('laporan-panwas')->name('laporan-panwas/')->group(static function() {
            Route::get('/',                                             'PanwasController@index')->name('index');
            Route::get('/create',                                       'PanwasController@create')->name('create');
            Route::post('/',                                            'PanwasController@store')->name('store');
            Route::get('/{panwas}/edit',                                 'PanwasController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PanwasController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{panwas}',                                     'PanwasController@update')->name('update');
            Route::delete('/{panwas}',                                   'PanwasController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('laporan-pelanggarans')->name('laporan-pelanggarans/')->group(static function() {
            Route::get('/',                                             'PelanggaranController@index')->name('index');
            Route::get('/{pelanggaran}/print',                           'PelanggaranController@print')->name('print');
            Route::get('/create',                                       'PelanggaranController@create')->name('create');
            Route::post('/',                                            'PelanggaranController@store')->name('store');
            Route::get('/{pelanggaran}/edit',                           'PelanggaranController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PelanggaranController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{pelanggaran}',                               'PelanggaranController@update')->name('update');
            Route::delete('/{pelanggaran}',                             'PelanggaranController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('laporan-pengawasans')->name('laporan-pengawasans/')->group(static function() {
            Route::get('/',                                             'PengawasanController@index')->name('index');
            Route::get('/{pengawasan}/print',                           'PengawasanController@print')->name('print');
            Route::get('/create',                                       'PengawasanController@create')->name('create');
            Route::post('/',                                            'PengawasanController@store')->name('store');
            Route::get('/{pengawasan}/edit',                            'PengawasanController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PengawasanController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{pengawasan}',                                'PengawasanController@update')->name('update');
            Route::delete('/{pengawasan}',                              'PengawasanController@destroy')->name('destroy');
        });
    });
});