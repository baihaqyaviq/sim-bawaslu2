@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('laporan::admin.pelanggaran.actions.index'))

@section('body')

    <pelanggaran-listing
            :data="{{ $data->toJson() }}"
            :url="'{{ url('admin/laporan-pelanggarans') }}'"
            inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('laporan::admin.pelanggaran.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0"
                           href="{{ url('admin/laporan-pelanggarans/create') }}" role="button"><i
                                    class="fa fa-plus"></i>&nbsp; {{ trans('laporan::admin.pelanggaran.actions.create') }}
                        </a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control"
                                                   placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                   v-model="search"
                                                   @keyup.enter="filter('search', $event.target.value)"/>
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary"
                                                        @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">

                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                <tr>
                                    <th class="bulk-checkbox">
                                        <input class="form-check-input" id="enabled" type="checkbox"
                                               v-model="isClickedAll" v-validate="''" data-vv-name="enabled"
                                               name="enabled_fake_element"
                                               @click="onBulkItemsClickedAllWithPagination()">
                                        <label class="form-check-label" for="enabled">
                                            #
                                        </label>
                                    </th>

                                    <th is='sortable'
                                        :column="'pengawasan_id'">{{ trans('laporan::admin.pelanggaran.columns.pengawasan_id') }}</th>
                                    <th is='sortable'
                                        :column="'peristiwa'">{{ trans('laporan::admin.pelanggaran.columns.peristiwa') }}</th>
                                    <th is='sortable'
                                        :column="'tempat_kejadian'">{{ trans('laporan::admin.pelanggaran.columns.tempat_kejadian') }}</th>
                                    <th is='sortable'
                                        :column="'waktu_kejadian'">{{ trans('laporan::admin.pelanggaran.columns.waktu_kejadian') }}</th>
                                    <th is='sortable'
                                        :column="'saksi_saksi'">{{ trans('laporan::admin.pelanggaran.columns.saksi_saksi') }}</th>
                                    <th is='sortable'
                                        :column="'alat_bukti'">{{ trans('laporan::admin.pelanggaran.columns.alat_bukti') }}</th>
                                    <th is='sortable'
                                        :column="'barang_bukti'">{{ trans('laporan::admin.pelanggaran.columns.barang_bukti') }}</th>
                                    <th is='sortable'
                                        :column="'pemilu_id'">{{ trans('laporan::admin.pelanggaran.columns.pemilu_id') }}</th>

                                    <th></th>
                                </tr>
                                <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                    <td class="bg-bulk-info d-table-cell text-center" colspan="15">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a
                                                        href="#" class="text-primary"
                                                        @click="onBulkItemsClickedAll('/admin/laporan-pelanggarans')"
                                                        v-if="(clickedBulkItemsCount < pagination.state.total)"> <i
                                                            class="fa"
                                                            :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span
                                                        class="text-primary">|</span> <a
                                                        href="#" class="text-primary"
                                                        @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                        <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3"
                                                        @click="bulkDelete('/admin/laporan-pelanggarans/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in collection" :key="item.id"
                                    :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                    <td class="bulk-checkbox">
                                        <input class="form-check-input" :id="'enabled' + item.id" type="checkbox"
                                               v-model="bulkItems[item.id]" v-validate="''"
                                               :data-vv-name="'enabled' + item.id"
                                               :name="'enabled' + item.id + '_fake_element'"
                                               @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                        <label class="form-check-label" :for="'enabled' + item.id">
                                        </label>
                                    </td>

                                    <td>@{{ item.pengawasan_id }}</td>
                                    <td>@{{ item.peristiwa }}</td>
                                    <td>@{{ item.tempat_kejadian }}</td>
                                    <td>@{{ item.waktu_kejadian }}</td>
                                    <td>
                                        <detail-tooltip :data="item.saksi_saksi" :text="item.saksi_saksi.length + ' Saksi'"
                                                        v-if="item.saksi_saksi"></detail-tooltip>
                                    </td>
                                    <td>
                                        <detail-tooltip :data="item.alat_bukti" :text="item.alat_bukti.length + ' Alat Bukti'"
                                                        v-if="item.alat_bukti"></detail-tooltip>
                                    </td>
                                    <td>
                                        <detail-tooltip :data="item.barang_bukti" :text="item.barang_bukti.length + ' Barang Bukti'"
                                                        v-if="item.barang_bukti"></detail-tooltip>
                                    </td>
                                    <td>
                                        <detail-tooltip :data="item.pemilu" :text="item.pemilu.nama_pemilu"
                                                        v-if="item.pemilu"></detail-tooltip>
                                    </td>

                                    <td>
                                        <div class="row no-gutters">
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info"
                                                   :href="item.resource_url + '/edit'"
                                                   title="{{ trans('brackets/admin-ui::admin.btn.edit') }}"
                                                   role="button"><i class="fa fa-edit"></i></a>
                                            </div>
                                            <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                <button type="submit" class="btn btn-sm btn-danger"
                                                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i
                                                            class="fa fa-trash-o"></i></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner"
                                   href="{{ url('admin/laporan-pelanggarans/create') }}" role="button"><i
                                            class="fa fa-plus"></i>&nbsp; {{ trans('laporan::admin.pelanggaran.actions.create') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </pelanggaran-listing>

@endsection