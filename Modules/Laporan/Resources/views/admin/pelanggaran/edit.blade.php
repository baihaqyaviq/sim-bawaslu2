@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('laporan::admin.pelanggaran.actions.edit', ['name' => $pelanggaran->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <pelanggaran-form
                :action="'{{ $pelanggaran->resource_url }}'"
                :data="{{ $pelanggaran->toJson() }}"
                :pemilu="{{$pemilu->toJson()}}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('laporan::admin.pelanggaran.actions.edit', ['name' => $pelanggaran->id]) }}
                    </div>

                    <div class="card-body">
                        @include('laporan::admin.pelanggaran.components.form-elements')
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(Modules\Laporan\Entities\Pelanggaran::class)->getMediaCollection('photo_alatbukti'),
                            'media' => $pelanggaran->getThumbs200ForCollection('photo_alatbukti'),
                            'label' => 'Gallery'
                        ])
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </pelanggaran-form>

        </div>
    
</div>

@endsection