@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('laporan::admin.pelanggaran.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <pelanggaran-form
            :action="'{{ url('admin/laporan-pelanggarans') }}'"
            :pemilu="{{$pemilu->toJson()}}"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('laporan::admin.pelanggaran.actions.create') }}
                </div>

                <div class="card-body">
                    @include('laporan::admin.pelanggaran.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </pelanggaran-form>

        </div>

        </div>

    
@endsection