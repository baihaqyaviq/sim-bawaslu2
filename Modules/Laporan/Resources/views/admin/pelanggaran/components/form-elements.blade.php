{{--<div class="form-group row align-items-center" :class="{'has-danger': errors.has('parent_id'), 'has-success': fields.parent_id && fields.parent_id.valid }">
    <label for="parent_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.parent_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.parent_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('parent_id'), 'form-control-success': fields.parent_id && fields.parent_id.valid}" id="parent_id" name="parent_id" placeholder="{{ trans('laporan::admin.pelanggaran.columns.parent_id') }}">
        <div v-if="errors.has('parent_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('parent_id') }}</div>
    </div>
</div>--}}

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pengawasan'), 'has-success': fields.pengawasan && fields.pengawasan.valid }">
    <label for="pengawasan" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.pengawasan_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.pengawasan"
                     id="ajax"
                     label="nomor"
                     track-by="id"
                     placeholder="Type to search"
                     open-direction="bottom"
                     :options="pengawasan"
                     :multiple="false"
                     :searchable="true"
                     :loading="isLoading"
                     :internal-search="false"
                     :options-limit="30"
                     :limit="3"
                     :limit-text="limitText"
                     :max-height="300"
                     :show-no-results="false"
                     :hide-selected="true"
                     @search-change="asyncFind">
            <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
        </multiselect>

        <div v-if="errors.has('pengawasan')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pengawasan') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('peristiwa'), 'has-success': fields.peristiwa && fields.peristiwa.valid }">
    <label for="peristiwa" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.peristiwa') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.peristiwa" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('peristiwa'), 'form-control-success': fields.peristiwa && fields.peristiwa.valid}" id="peristiwa" name="peristiwa" placeholder="{{ trans('laporan::admin.pelanggaran.columns.peristiwa') }}">
        <div v-if="errors.has('peristiwa')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('peristiwa') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tempat_kejadian'), 'has-success': fields.tempat_kejadian && fields.tempat_kejadian.valid }">
    <label for="tempat_kejadian" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.tempat_kejadian') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tempat_kejadian" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tempat_kejadian'), 'form-control-success': fields.tempat_kejadian && fields.tempat_kejadian.valid}" id="tempat_kejadian" name="tempat_kejadian" placeholder="{{ trans('laporan::admin.pelanggaran.columns.tempat_kejadian') }}">
        <div v-if="errors.has('tempat_kejadian')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tempat_kejadian') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('waktu_kejadian'), 'has-success': fields.waktu_kejadian && fields.waktu_kejadian.valid }">
    <label for="waktu_kejadian" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.waktu_kejadian') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.waktu_kejadian" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('waktu_kejadian'), 'form-control-success': fields.waktu_kejadian && fields.waktu_kejadian.valid}" id="waktu_kejadian" name="waktu_kejadian" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('waktu_kejadian')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('waktu_kejadian') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('pelaku'), 'has-success': fields.pelaku && fields.pelaku.valid }">
    <label for="pelaku" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.pelaku') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.pelaku" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pelaku'), 'form-control-success': fields.pelaku && fields.pelaku.valid}" id="pelaku" name="pelaku" placeholder="{{ trans('laporan::admin.pelanggaran.columns.pelaku') }}">
        <div v-if="errors.has('pelaku')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pelaku') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('alamat_peristiwa'), 'has-success': fields.alamat_peristiwa && fields.alamat_peristiwa.valid }">
    <label for="alamat_peristiwa" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.alamat_peristiwa') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <textarea type="text" v-model="form.alamat_peristiwa" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('alamat_peristiwa'), 'form-control-success': fields.alamat_peristiwa && fields.alamat_peristiwa.valid}" id="alamat_peristiwa" name="alamat_peristiwa" placeholder="{{ trans('laporan::admin.pelanggaran.columns.alamat_peristiwa') }}"></textarea>
        <div v-if="errors.has('alamat_peristiwa')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alamat_peristiwa') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('saksi_saksi'), 'has-success': fields.saksi_saksi && fields.saksi_saksi.valid }">
    <label for="saksi_saksi" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.saksi_saksi') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div v-for="(saksi, index) in form.saksi_saksi" :key="index">
            <div class="card">
                <header class="card-header">
                    Saksi @{{index+1}}
                    <div class="card-header-actions" v-if="index > 0">
                        <button @click="removeSaksi" type="button" class="card-header-action btn btn-danger">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </header>
                <div class="card-body">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('saksi_saksi'), 'has-success': fields.saksi_saksi && fields.saksi_saksi.valid }">
                        <div class="col-md-6">
                            <label for="bentuk" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.saksi_saksi.nama') }}</label>
                            <input type="text" v-model="saksi.nama" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('saksi_saksi'), 'form-control-success': fields.saksi_saksi && fields.saksi_saksi.valid}" id="nama" :name="`saksi_saksi[${index}][nama]`" placeholder="{{ trans('laporan::admin.pelanggaran.saksi_saksi.nama') }}">
                            <div v-if="errors.has('saksi_saksi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('saksi_saksi') }}</div>
                        </div>

                        <div class="col-md-6">
                            <label for="alamat" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.saksi_saksi.alamat') }}</label>
                            <input type="text" v-model="saksi.alamat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('saksi_saksi'), 'form-control-success': fields.saksi_saksi && fields.saksi_saksi.valid}" id="alamat" :name="`saksi_saksi[${index}][alamat]`" placeholder="{{ trans('laporan::admin.pelanggaran.saksi_saksi.alamat') }}">
                            <div v-if="errors.has('saksi_saksi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('saksi_saksi') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button @click="addSaksi" type="button" class="btn btn-sm btn-primary">
            <i class="fa fa-plus"></i> Add
        </button>
        <div v-if="errors.has('saksi_saksi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('saksi_saksi') }}</div>
    </div>
</div>

{{--@include('brackets/admin-ui::admin.includes.media-uploader', [
   'mediaCollection' => app(Modules\Laporan\Entities\Pelanggaran::class)->getMediaCollection('barang_bukti'),
   'label' => trans('laporan::admin.pelanggaran.alat_bukti.photo')
])--}}

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('alat_bukti'), 'has-success': fields.alat_bukti && fields.alat_bukti.valid }">
    <label for="alat_bukti" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.alat_bukti') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div v-for="(alatbukti, index) in form.alat_bukti" :key="index">
            <div class="card">
                <header class="card-header">
                    Alat Bukti @{{index+1}}
                    <div class="card-header-actions" v-if="index > 0">
                        <button @click="removeAlatBukti" type="button" class="card-header-action btn btn-danger">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </header>
                <div class="card-body">
                    {{--@include('brackets/admin-ui::admin.includes.media-uploader', [
                       'mediaCollection' => app(Modules\Laporan\Entities\Pelanggaran::class)->getMediaCollection('alat_bukti'),
                       'label' => trans('laporan::admin.pelanggaran.alat_bukti.photo')
                   ])--}}
                    <div class="label">
                        <i class="fa fa-file-image-o"></i>
                        {{ trans('laporan::admin.pelanggaran.alat_bukti.photo') }}
                        <small>{{ trans('brackets/admin-ui::admin.media_uploader.max_number_of_files', ['maxNumberOfFiles' => 6]) }}</small>
                        <small>{{ trans('brackets/admin-ui::admin.media_uploader.max_size_pre_file', ['maxFileSize' => 3]) }}</small>
                    </div>
                    <media-upload
                            ref="alat_bukti_uploader"
                            :collection="`alat_bukti_${index+1}`"
                            :url="'{{ route('brackets/media::upload') }}'"
                            :max-number-of-files="6"
                            :max-file-size-in-mb="3"
                            :accepted-file-types="`image/*`"
                            @if(isset($media) && $media->count() > 0)
                            :uploaded-images="{{ $media->toJson() }}"
                            @endif
                    ></media-upload>
                    <div class="form-group " :class="{'has-danger': errors.has('alat_bukti'), 'has-success': fields.alat_bukti && fields.alat_bukti.valid }">
                        {{--<div class="col-md-6">
                            <label for="bentuk" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.alat_bukti.photo') }}</label>
                            <input type="text" v-model="alatbukti.photo" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('alat_bukti'), 'form-control-success': fields.alat_bukti && fields.alat_bukti.valid}" id="photo_alatbukti" :name="`alat_bukti[${index}][photo]`" placeholder="{{ trans('laporan::admin.pelanggaran.alat_bukti.photo') }}">
                            <div v-if="errors.has('alat_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alat_bukti') }}</div>
                        </div>--}}
                        <label for="alamat" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.alat_bukti.keterangan') }}</label>
                        <textarea v-model="alatbukti.keterangan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('alat_bukti'), 'form-control-success': fields.alat_bukti && fields.alat_bukti.valid}" id="keterangan_alatbukti" :name="`alat_bukti[${index}][keterangan]`" placeholder="{{ trans('laporan::admin.pelanggaran.alat_bukti.keterangan') }}"> </textarea>
                        <div v-if="errors.has('alat_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alat_bukti') }}</div>
                    </div>
                </div>
            </div>
        </div>

        <button @click="addAlatBukti" type="button" class="btn btn-sm btn-primary">
            <i class="fa fa-plus"></i> Add
        </button>
        <div v-if="errors.has('alat_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alat_bukti') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('barang_bukti'), 'has-success': fields.barang_bukti && fields.barang_bukti.valid }">
    <label for="barang_bukti" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.barang_bukti') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div v-for="(barangbukti, index) in form.barang_bukti" :key="index">
            <div class="card">
                <header class="card-header">
                    Barang Bukti @{{index+1}}
                    <div class="card-header-actions" v-if="index > 0">
                        <button @click="removeBarangBukti" type="button" class="card-header-action btn btn-danger">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </header>
                <div class="card-body">
                    <div class="label">
                        <i class="fa fa-file-image-o"></i>
                        {{ trans('laporan::admin.pelanggaran.barang_bukti.photo') }}
                        <small>{{ trans('brackets/admin-ui::admin.media_uploader.max_number_of_files', ['maxNumberOfFiles' => 6]) }}</small>
                        <small>{{ trans('brackets/admin-ui::admin.media_uploader.max_size_pre_file', ['maxFileSize' => 3]) }}</small>
                    </div>
                    <media-upload
                            ref="barang_bukti_uploader"
                            :collection="`barang_bukti_${index+1}`"
                            :url="'{{ route('brackets/media::upload') }}'"
                            :max-number-of-files="6"
                            :max-file-size-in-mb="3"
                            :accepted-file-types="`image/*`"
                            @if(isset($media) && $media->count() > 0)
                            :uploaded-images="{{ $media->toJson() }}"
                            @endif
                    ></media-upload>
                    <div class="form-group" :class="{'has-danger': errors.has('barang_bukti'), 'has-success': fields.barang_bukti && fields.barang_bukti.valid }">
                       {{-- <div class="col-md-6">
                            <label for="bentuk" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.barang_bukti.photo') }}</label>
                            <input type="text" v-model="barangbukti.photo" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('barang_bukti'), 'form-control-success': fields.barang_bukti && fields.barang_bukti.valid}" id="photo_barangbukti" :name="`barang_bukti[${index}][photo]`" placeholder="{{ trans('laporan::admin.pelanggaran.barang_bukti.photo') }}">
                            <div v-if="errors.has('barang_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('barang_bukti') }}</div>
                        </div>--}}

                        <label for="alamat" class="col-form-label text-md-right">{{ trans('laporan::admin.pelanggaran.barang_bukti.keterangan') }}</label>
                        <input type="text" v-model="barangbukti.keterangan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('barang_bukti'), 'form-control-success': fields.barang_bukti && fields.barang_bukti.valid}" id="keterangan_barangbukti" :name="`barang_bukti[${index}][keterangan]`" placeholder="{{ trans('laporan::admin.pelanggaran.barang_bukti.keterangan') }}">
                        <div v-if="errors.has('barang_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('barang_bukti') }}</div>
                    </div>
                </div>
            </div>
        </div>

        <button @click="addBarangBukti" type="button" class="btn btn-sm btn-primary">
            <i class="fa fa-plus"></i> Add
        </button>
        <div v-if="errors.has('barang_bukti')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('barang_bukti') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('uraian'), 'has-success': fields.uraian && fields.uraian.valid }">
    <label for="uraian" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.uraian') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.uraian" v-validate="''" id="uraian" name="uraian" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('uraian')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('uraian') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pelanggaran.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>