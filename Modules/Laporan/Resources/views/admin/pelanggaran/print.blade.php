@extends('laporan::admin.pengawasan.print')

@section('pelanggaran')
    <div class="wrapper mdtopmargin nobottommargin">
        <div class="col-10">
            <p class="nomargin"><strong><span class="rightpadding">IV.</span>Informasi Dugaan Pelanggaran</strong>
            </p>
        </div>
    </div>

    <div class="smpadding">
        <div class="wrapper">
            <div class="col-10">
                <p class="nomargin"><strong>
                        <span class="rightpadding">1.</span>Peristiwa
                    </strong>
                </p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-5">
                <p class="nomargin smpadding">a. Peristiwa</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pelanggaran['peristiwa']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-5">
                <p class="nomargin smpadding">b. Tempat Kejadian</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pelanggaran['tempat_kejadian']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-5">
                <p class="nomargin smpadding">c. Waktu Kejadian</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pelanggaran['waktu_kejadian']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-5">
                <p class="nomargin smpadding">d. Pelaku</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pelanggaran['pelaku']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-5">
                <p class="nomargin smpadding">e. Alamat</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pelanggaran['alamat_peristiwa']) }}</p>
            </div>
        </div>

        <div class="wrapper nobottommargin">
            <div class="col-10">
                <p class="nomargin nobottommargin"><strong>
                        <span class="rightpadding">2.</span>Saksi-saksi
                    </strong>
                </p>
            </div>
        </div>
        <div class="wrapper notopmargin">
            <div class="nomargin" style="padding-left:0.2 rem">
                @foreach($pelanggaran->saksi_saksi as $saksi)
                    <div class="wrapper notopmargin">
                        <div class="col-10">
                            <p class="nomargin notopmargin"><strong>
                                    Saksi {{Helper::ConverToRoman($loop->iteration)}}
                                </strong></p>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="col-5">
                            <p class="nomargin">a. Nama</p>
                        </div>
                        <div class="col-10">
                            <p class="nomargin tab"><span>:</span> {{ ucwords($saksi['nama']) }}</p>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="col-5">
                            <p class="nomargin">b. Alamat</p>
                        </div>
                        <div class="col-10">
                            <p class="nomargin tab"><span>:</span> {{ ucwords($saksi['alamat']) }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="wrapper">
            <div class="col-10">
                <p class="nomargin"><strong>
                        <span class="rightpadding">3.</span>Alat Bukti
                    </strong>
                </p>
            </div>
        </div>

        <div class="wrapper notopmargin">
            <div class="nomargin" style="padding-left:0.2 rem">
                @foreach($pelanggaran->alat_bukti as $alatbukti)
                    @if($alatbukti['photo'] || $alatbukti['keterangan'])
                        <div class="wrapper">
                            <div class="col-12">
                                <p class="notopmargin lgbottommargin"><strong>
                                        Alat Bukti {{Helper::ConverToRoman($loop->iteration)}}
                                    </strong></p>
                            </div>
                        </div>
                        @foreach($photoAlatBukti['alat_bukti_'.$loop->iteration] as $photo)
                            <img class="smtopmargin" src="{{public_path($photo['thumb'])}}">
                        @endforeach
                        <div class="wrapper">
                            <div class="col-2">
                                <p class="nomargin"><strong>Keterangan:</strong></p>
                            </div>
                            <div class="col-10">
                                <p class="nomargin">{{ ucfirst($alatbukti['keterangan']) }}</p>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="wrapper">
            <div class="col-10">
                <p class="nomargin"><strong>
                        <span class="rightpadding">4.</span>Barang Bukti
                    </strong>
                </p>
            </div>
        </div>

        <div class="wrapper notopmargin">
            <div class="nomargin" style="padding-left:0.2 rem">
                @foreach($pelanggaran->barang_bukti as $barangbukti)
                    @if($barangbukti['photo'] || $barangbukti['keterangan'])
                        <div class="wrapper">
                            <div class="col-10">
                                <p class="notopmargin lgbottommargin"><strong>
                                        Barang Bukti {{Helper::ConverToRoman($loop->iteration)}}
                                    </strong></p>
                            </div>
                        </div>
                        @foreach($photoBarangBukti['barang_bukti_'.$loop->iteration] as $photo)
                            <img class="smtopmargin" src="{{public_path($photo['thumb'])}}">
                        @endforeach
                        <div class="wrapper">
                            <div class="col-2">
                                <p class="nomargin"><strong>Keterangan:</strong></p>
                            </div>
                            <div class="col-10">
                                <p class="nomargin">{{ ucfirst($barangbukti['keterangan']) }}</p>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="wrapper mdtopmargin nobottommargin">
            <div class="col-10">
                <p class="nomargin"><strong>
                        <span class="rightpadding">5.</span>Uraian singkat dugaan pelanggaran
                    </strong>
                </p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-10">
                <p class="nobottommargin text-justify">{!! $pelanggaran->uraian !!}</p>
            </div>
        </div>
    </div>
@endsection