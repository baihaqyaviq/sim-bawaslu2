<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nik'), 'has-success': fields.nik && fields.nik.valid }">
    <label for="nik" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.nik') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nik" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nik'), 'form-control-success': fields.nik && fields.nik.valid}" id="nik" name="nik" placeholder="{{ trans('laporan::admin.panwas.columns.nik') }}">
        <div v-if="errors.has('nik')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nik') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama'), 'has-success': fields.nama && fields.nama.valid }">
    <label for="nama" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.nama') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama'), 'form-control-success': fields.nama && fields.nama.valid}" id="nama" name="nama" placeholder="{{ trans('laporan::admin.panwas.columns.nama') }}">
        <div v-if="errors.has('nama')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('alamat'), 'has-success': fields.alamat && fields.alamat.valid }">
    <label for="alamat" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.alamat') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <textarea type="text" v-model="form.alamat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('alamat'), 'form-control-success': fields.alamat && fields.alamat.valid}" id="alamat" name="alamat" placeholder="{{ trans('laporan::admin.panwas.columns.alamat') }}"></textarea>
        <div v-if="errors.has('alamat')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alamat') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('jenis_kelamin'), 'has-success': fields.jenis_kelamin && fields.jenis_kelamin.valid }">
    <label for="jenis_kelamin" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.jenis_kelamin') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.jenis_kelamin" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('jenis_kelamin'), 'form-control-success': fields.jenis_kelamin && fields.jenis_kelamin.valid}" id="jenis_kelamin" name="jenis_kelamin" placeholder="{{ trans('laporan::admin.panwas.columns.jenis_kelamin') }}">
        <div v-if="errors.has('jenis_kelamin')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('jenis_kelamin') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tanggal_lahir'), 'has-success': fields.tanggal_lahir && fields.tanggal_lahir.valid }">
    <label for="tanggal_lahir" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.tanggal_lahir') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.tanggal_lahir" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('tanggal_lahir'), 'form-control-success': fields.tanggal_lahir && fields.tanggal_lahir.valid}" id="tanggal_lahir" name="tanggal_lahir" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('tanggal_lahir')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tanggal_lahir') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tempat_lahir'), 'has-success': fields.tempat_lahir && fields.tempat_lahir.valid }">
    <label for="tempat_lahir" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.tempat_lahir') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tempat_lahir" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tempat_lahir'), 'form-control-success': fields.tempat_lahir && fields.tempat_lahir.valid}" id="tempat_lahir" name="tempat_lahir" placeholder="{{ trans('laporan::admin.panwas.columns.tempat_lahir') }}">
        <div v-if="errors.has('tempat_lahir')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tempat_lahir') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('agama_id'), 'has-success': fields.agama_id && fields.agama_id.valid }">
    <label for="agama_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.agama_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.agama_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('agama_id'), 'form-control-success': fields.agama_id && fields.agama_id.valid}" id="agama_id" name="agama_id" placeholder="{{ trans('laporan::admin.panwas.columns.agama_id') }}">
        <div v-if="errors.has('agama_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('agama_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('status_kawin_id'), 'has-success': fields.status_kawin_id && fields.status_kawin_id.valid }">
    <label for="status_kawin_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.status_kawin_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.status_kawin_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status_kawin_id'), 'form-control-success': fields.status_kawin_id && fields.status_kawin_id.valid}" id="status_kawin_id" name="status_kawin_id" placeholder="{{ trans('laporan::admin.panwas.columns.status_kawin_id') }}">
        <div v-if="errors.has('status_kawin_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status_kawin_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('jabatan'), 'has-success': fields.jabatan && fields.jabatan.valid }">
    <label for="jabatan" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.jabatan') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.jabatan" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('jabatan'), 'form-control-success': fields.jabatan && fields.jabatan.valid}" id="jabatan" name="jabatan" placeholder="{{ trans('laporan::admin.panwas.columns.jabatan') }}">
        <div v-if="errors.has('jabatan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('jabatan') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tingkat'), 'has-success': fields.tingkat && fields.tingkat.valid }">
    <label for="tingkat" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.tingkat') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tingkat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tingkat'), 'form-control-success': fields.tingkat && fields.tingkat.valid}" id="tingkat" name="tingkat" placeholder="{{ trans('laporan::admin.panwas.columns.tingkat') }}">
        <div v-if="errors.has('tingkat')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tingkat') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tgl_spt'), 'has-success': fields.tgl_spt && fields.tgl_spt.valid }">
    <label for="tgl_spt" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.tgl_spt') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.tgl_spt" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('tgl_spt'), 'form-control-success': fields.tgl_spt && fields.tgl_spt.valid}" id="tgl_spt" name="tgl_spt" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('tgl_spt')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tgl_spt') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('no_spt'), 'has-success': fields.no_spt && fields.no_spt.valid }">
    <label for="no_spt" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.no_spt') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.no_spt" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('no_spt'), 'form-control-success': fields.no_spt && fields.no_spt.valid}" id="no_spt" name="no_spt" placeholder="{{ trans('laporan::admin.panwas.columns.no_spt') }}">
        <div v-if="errors.has('no_spt')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('no_spt') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.panwas.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#pendidikan">
                    <i class="fa fa-graduation-cap"></i> Pendidikan
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#pekerjaan">
                    <i class="fa fa-user-md"></i> Pekerjaan
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#organisasi">
                    <i class="fa fa-paperclip"></i> Organisasi
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#penghargaan">
                    <i class="fa fa-user-secret"></i> Penghargaan
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#karyatulis">
                    <i class="fa fa-book"></i> Karya Tulis
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="tab-content">
    <div class="tab-pane active" id="pendidikan" role="tabpanel">
        <div class="form-group row">
            <div class="col-lg-12">
                <div v-for="(pendidikan, index) in form.pendidikan" :key="index">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('pendidikan'), 'has-success': fields.pendidikan && fields.pendidikan.valid }">
                        <div class="col-md-6">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pendidikan.nama_sekolah') }}</label>
                            <input type="text" v-model="pendidikan.nama_sekolah" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pendidikan'), 'form-control-success': fields.pendidikan && fields.pendidikan.valid}" id="nama_sekolah" :name="`pendidikan[${index}][nama_sekolah]`" placeholder="{{ trans('laporan::admin.panwas.pendidikan.nama_sekolah') }}">
                            <div v-if="errors.has('pendidikan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pendidikan') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pendidikan.jenjang') }}</label>
                            <input type="text" v-model="pendidikan.jenjang" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pendidikan'), 'form-control-success': fields.pendidikan && fields.pendidikan.valid}" id="jenjang" :name="`pendidikan[${index}][jenjang]`" placeholder="{{ trans('laporan::admin.panwas.pendidikan.jenjang') }}">
                            <div v-if="errors.has('pendidikan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pendidikan') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pendidikan.tahun_lulus') }}</label>
                            <input type="text" v-model="pendidikan.tahun_lulus" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pendidikan'), 'form-control-success': fields.pendidikan && fields.pendidikan.valid}" id="tahun_lulus" :name="`pendidikan[${index}][tahun_lulus]`" placeholder="{{ trans('laporan::admin.panwas.pendidikan.tahun_lulus') }}">
                            <div v-if="errors.has('pendidikan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pendidikan') }}</div>
                        </div>
                        <div class="col-md-1">
                            <button @click="removePendidikan" v-if="index > 0" type="button" class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <button @click="addPendidikan" type="button" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus"></i> Add
                </button>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="pekerjaan" role="tabpanel">
        <div class="form-group row">
            <div class="col-lg-12">
                <div v-for="(pekerjaan, index) in form.pekerjaan" :key="index">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('pekerjaan'), 'has-success': fields.pekerjaan && fields.pekerjaan.valid }">
                        <div class="col-md-6">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pekerjaan.nama_pekerjaan') }}</label>
                            <input type="text" v-model="pekerjaan.nama_pekerjaan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pekerjaan'), 'form-control-success': fields.pekerjaan && fields.pekerjaan.valid}" id="nama_pekerjaan" :name="`pekerjaan[${index}][nama_pekerjaan]`" placeholder="{{ trans('laporan::admin.panwas.pekerjaan.nama_pekerjaan') }}">
                            <div v-if="errors.has('pekerjaan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pekerjaan') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pekerjaan.tahun_masuk') }}</label>
                            <input type="text" v-model="pekerjaan.tahun_masuk" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pekerjaan'), 'form-control-success': fields.pekerjaan && fields.pekerjaan.valid}" id="tahun_masuk" :name="`pekerjaan[${index}][tahun_masuk]`" placeholder="{{ trans('laporan::admin.panwas.pekerjaan.tahun_masuk') }}">
                            <div v-if="errors.has('pekerjaan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pekerjaan') }}</div>

                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.pekerjaan.tahun_keluar') }}</label>
                            <input type="text" v-model="pekerjaan.tahun_keluar" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('pekerjaan'), 'form-control-success': fields.pekerjaan && fields.pekerjaan.valid}" id="tahun_keluar" :name="`pekerjaan[${index}][tahun_keluar]`" placeholder="{{ trans('laporan::admin.panwas.pekerjaan.tahun_keluar') }}">
                            <div v-if="errors.has('pekerjaan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pekerjaan') }}</div>
                        </div>
                        <div class="col-md-1">
                            <button @click="removePekerjaan" v-if="index > 0" type="button" class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
                <div class="col">
                    <button @click="addPekerjaan" type="button" class="btn btn-sm btn-primary">
                        <i class="fa fa-plus"></i> Add
                    </button>
                </div>
            </div>
    </div>
    <div class="tab-pane" id="organisasi" role="tabpanel">
        <div class="form-group row">
            <div class="col-lg-12">
                <div v-for="(organisasi, index) in form.organisasi" :key="index">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('organisasi'), 'has-success': fields.organisasi && fields.organisasi.valid }">
                        <div class="col-md-6">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.organisasi.nama_organisasi') }}</label>
                            <input type="text" v-model="organisasi.nama_organisasi" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('organisasi'), 'form-control-success': fields.organisasi && fields.organisasi.valid}" id="nama_organisasi" :name="`organisasi[${index}][nama_organisasi]`" placeholder="{{ trans('laporan::admin.panwas.organisasi.nama_organisasi') }}">
                            <div v-if="errors.has('organisasi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('organisasi') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.organisasi.tahun_awal') }}</label>
                            <input type="text" v-model="organisasi.tahun_awal" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('organisasi'), 'form-control-success': fields.organisasi && fields.organisasi.valid}" id="tahun_awal" :name="`organisasi[${index}][tahun_awal]`" placeholder="{{ trans('laporan::admin.panwas.organisasi.tahun_awal') }}">
                            <div v-if="errors.has('organisasi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('organisasi') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.organisasi.tahun_akhir') }}</label>
                            <input type="text" v-model="organisasi.tahun_akhir" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('organisasi'), 'form-control-success': fields.organisasi && fields.organisasi.valid}" id="tahun_akhir" :name="`organisasi[${index}][tahun_akhir]`" placeholder="{{ trans('laporan::admin.panwas.organisasi.tahun_akhir') }}">
                            <div v-if="errors.has('organisasi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('organisasi') }}</div>
                        </div>
                        <div class="col-md-1">
                            <button @click="removeOrganisasi" v-if="index > 0" type="button" class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <button @click="addOrganisasi" type="button" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus"></i> Add
                </button>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="penghargaan" role="tabpanel">
        <div class="form-group row">
            <div class="col-lg-12">
                <div v-for="(penghargaan, index) in form.penghargaan" :key="index">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('penghargaan'), 'has-success': fields.penghargaan && fields.penghargaan.valid }">
                        <div class="col-md-8">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.penghargaan.nama_penghargaan') }}</label>
                            <input type="text" v-model="penghargaan.nama_penghargaan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('penghargaan'), 'form-control-success': fields.penghargaan && fields.penghargaan.valid}" id="nama_penghargaan" :name="`penghargaan[${index}][nama_penghargaan]`" placeholder="{{ trans('laporan::admin.panwas.penghargaan.nama_penghargaan') }}">
                            <div v-if="errors.has('penghargaan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('penghargaan') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.penghargaan.tahun') }}</label>
                            <input type="text" v-model="penghargaan.tahun" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('penghargaan'), 'form-control-success': fields.penghargaan && fields.penghargaan.valid}" id="tahun" :name="`penghargaan[${index}][tahun]`" placeholder="{{ trans('laporan::admin.panwas.penghargaan.tahun') }}">
                            <div v-if="errors.has('penghargaan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('penghargaan') }}</div>
                        </div>
                        <div class="col-md-1">
                            <button @click="removePenghargaan" v-if="index > 0" type="button" class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <button @click="addPenghargaan" type="button" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus"></i> Add
                </button>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="karyatulis" role="tabpanel">
        <div class="form-group row">
            <div class="col-lg-12">
                <div v-for="(karyatulis, index) in form.karyatulis" :key="index">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('karyatulis'), 'has-success': fields.karyatulis && fields.karyatulis.valid }">
                        <div class="col-md-8">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.karyatulis.nama_karya') }}</label>
                            <input type="text" v-model="karyatulis.nama_karya" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('karyatulis'), 'form-control-success': fields.karyatulis && fields.karyatulis.valid}" id="nama_penghargaan" :name="`karyatulis[${index}][nama_karya]`" placeholder="{{ trans('laporan::admin.panwas.karyatulis.nama_karya') }}">
                            <div v-if="errors.has('karyatulis')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('karyatulis') }}</div>
                        </div>
                        <div class="col-md-2">
                            <label for="no_spt" class="col-form-label text-md-right">{{ trans('laporan::admin.panwas.karyatulis.tahun') }}</label>
                            <input type="text" v-model="karyatulis.tahun" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('karyatulis'), 'form-control-success': fields.karyatulis && fields.karyatulis.valid}" id="tahun" :name="`karyatulis[${index}][tahun]`" placeholder="{{ trans('laporan::admin.panwas.karyatulis.tahun') }}">
                            <div v-if="errors.has('karyatulis')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('karyatulis') }}</div>
                        </div>
                        <div class="col-md-1">
                            <button @click="removeKaryatulis" v-if="index > 0" type="button" class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col">
                <button @click="addKaryatulis" type="button" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus"></i> Add
                </button>
            </div>
        </div>
    </div>
</div>
