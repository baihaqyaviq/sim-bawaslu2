<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LAPORAN HASIL PENGAWASAN PEMILU</title>
</head>
<style>
    body, html {
        height: 100%;
        font-family: Arial, Helvetica, sans-serif;
        margin: 0.5cm;
        /* -webkit-print-color-adjust: exact; */
    }

    .page-break {
        page-break-after: always;
    }

    @page {
        /*size: 8.27in 11.69in;*/
        size: 21.6cm 33cm;
        margin: 0.5cm;
    }

    p {
        margin-top: 8px;
        margin-bottom: 8px;
    }

    h5 {
        font-weight: normal;
    }

    @media print {
        @page {
            size: portrait;
            size: 21.6cm 33cm;
        }

        html {
            height: auto;
            -webkit-print-color-adjust: exact;
        }

        .header {
            page-break-before: always;
        }
    }

    .footer {
        page-break-after: always;
    }

    .wrapper {
        padding-top: .25em;
        padding-bottom: .25em;
        display: table;
        width: 100%;
    }

    .wrapper div {
        padding-left: 1rem;
        padding-right: 1rem;
    }

    .col-1 {
        display: table-cell;
        width: 8.33333333333%;
    }

    .col-2 {
        display: table-cell;
        width: 16.6666666667%;
    }

    .col-3 {
        display: table-cell;
        width: 25%;
    }

    .col-4 {
        display: table-cell;
        width: 33.3333333333%;
    }

    .col-5 {
        display: table-cell;
        width: 41.6666666667%;
    }

    .col-6 {
        display: table-cell;
        width: 50%;
    }

    .col-7 {
        display: table-cell;
        width: 58.3333333333%;
    }

    .col-8 {
        display: table-cell;
        width: 66.6666666667%;
    }

    .col-9 {
        display: table-cell;
        width: 75%;
    }

    .col-10 {
        display: table-cell;
        width: 83.3333333333%;
    }

    .col-11 {
        display: table-cell;
        width: 91.6666666667%;
    }

    .col-12 {
        display: table-cell;
        width: 100%;
    }

    .top-wrapper {
        margin-top: 50px;
    }

    .text-center {
        text-align: center;
    }

    .text-justiry {
        text-align: justify;
    }

    .bolder p {
        font-weight: 900;
    }

    .nomargin {
        margin: 0;
    }

    .notopmargin {
        margin-top: 0;
    }

    .nobottommargin {
        margin-bottom: 0;
    }

    .lgtopmargin {
        margin-top: 40px;
    }

    .mdtopmargin {
        margin-top: 20px;
    }

    .smtopmargin {
        margin-top: 10px;
    }

    .xstopmargin {
        margin-top: 5px;
    }

    .smbottommargin {
        margin-bottom: 10px;
    }

    .lgbottommargin {
        margin-bottom: 40px;
    }

    .nopadding {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }

    .mdpadding {
        padding-left: 2rem;
        padding-right: 2rem;
    }

    .smpadding {
        padding-left: 1rem;
        padding-right: 1rem;
    }

    .smtoppadding {
        padding-top: 10px;
    }

    .rightpadding {
        margin-left: 0;
        padding-left: 0;
        padding-right: 0.5rem;
    }

    p, ul li, ol li {
        font-size: 12pt;
    }

    table {
        border-collapse: collapse;
    }

    table th, table td {
        border: 1px solid #000;
        text-align: center;
        font-size: 10pt;
    }

    .sub-eng {
        font-size: 10pt;
        font-style: italic;
        font-weight: normal;
    }

    /*.header-table {*/
    /*letter-spacing: 3px;*/
    /*}*/

    ul {
        list-style-type: none;
    }

    .with-border {
        border: solid 2px #000000;
        padding-left: 0.25rem;
        padding-right: 0.25rem;
        font-weight: bold;
    }

    .allborder {
        border: solid 1px #000000;
    }

    .bottomborder {
        border-bottom: solid 1px #000000;
    }

    .keterangan:before {
        content: "";
        position: absolute;
        left: 2rem;
        height: 1px;
        width: 15%; /* or 100px */
        border-top: 3px solid black;
    }

    .keterangan p {
        font-size: 10pt;
        margin: 0;
    }

    ol.nomargin {
        padding-left: 1.25rem;
        padding-right: 1.25rem;
    }

    .print:last-child {
        page-break-after: auto;
    }

    p.tab > span {
        display: inline-block;
        min-width: 20px;
    }

    p.form > span {
        display: inline-block;
        min-width: 275px;
    }

    p.subform:after {
        content: "";
        position: absolute;
        right: 5rem;
        height: 1px;
        width: 15%; /* or 100px */
        border-top: 1px solid black;
    }

    .box {
        border: solid 1px #000000;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .bold {
        font-weight: bold;
    }
</style>
<body>
<div class="print page-break">
    <div class="wrapper" style="margin-top: -0.7cm">
        <h4 class="nomargin">FORM A</h4>
    </div>
    <div class="wrapper">
        <div class="col-10 text-center">
            <h3 class="nomargin">LAPORAN HASIL PENGAWASAN PEMILU</h3>
            <h5 class="nomargin">NOMOR: {{$pengawasan->nomor}}</h5>
        </div>
    </div>

    <div class="wrapper lgtopmargin">
        <div class="col-10">
            <p class="nomargin"><strong><span class="rightpadding">I.</span>Data Pengawas Pemilu</strong></p>
        </div>
    </div>

    <div class="smpadding">
        <div class="wrapper">
            <div class="col-7">
                <p class="nomargin">Tahapan yang diawasi</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pengawasan['tahapan_yg_diawasi']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-7">
                <p class="nomargin">Nama Pelaksana Tugas</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pengawasan['nama_pelaksana']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-7">
                <p class="nomargin">Jabatan</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pengawasan['jabatan']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-7">
                <p class="nomargin">Nomor Surat Perintah Tugas</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pengawasan['nomor_surat_perintah']) }}</p>
            </div>
        </div>
        <div class="wrapper">
            <div class="col-7">
                <p class="nomargin">Alamat</p>
            </div>
            <div class="col-10">
                <p class="nomargin tab"><span>:</span> {{ ucwords($pengawasan['alamat']) }}</p>
            </div>
        </div>
    </div>

    <div class="wrapper mdtopmargin nobottommargin">
        <div class="col-10">
            <p class="nomargin"><strong><span class="rightpadding">II.</span>Kegiatan Pengawasan</strong></p>
        </div>
    </div>

    <div class="smpadding">
        @foreach($pengawasan->kegiatan_pengawasan as $kegiatan)
            <div class="wrapper">
                <div class="col-10">
                    <p class="nomargin"><strong>
                            <span class="rightpadding">{{$loop->iteration}}.</span>Kegiatan {{Helper::ConverToRoman($loop->iteration)}}
                        </strong>
                    </p>
                </div>
            </div>
            <div class="wrapper">
                <div class="col-5">
                    <p class="nomargin smpadding">a. Bentuk</p>
                </div>
                <div class="col-10">
                    <p class="nomargin tab"><span>:</span> {{ ucwords($kegiatan['bentuk']) }}</p>
                </div>
            </div>
            <div class="wrapper">
                <div class="col-5">
                    <p class="nomargin smpadding">b. Tujuan</p>
                </div>
                <div class="col-10">
                    <p class="nomargin tab"><span>:</span> {{ ucwords($kegiatan['tujuan']) }}</p>
                </div>
            </div>
            <div class="wrapper">
                <div class="col-5">
                    <p class="nomargin smpadding">c. Sasaran</p>
                </div>
                <div class="col-10">
                    <p class="nomargin tab"><span>:</span> {{ ucwords($kegiatan['sasaran']) }}</p>
                </div>
            </div>
            <div class="wrapper">
                <div class="col-5">
                    <p class="nomargin smpadding">d. Waktu dan Tempat</p>
                </div>
                <div class="col-10">
                    <p class="nomargin tab"><span>:</span> {{ ucwords($kegiatan['waktu_tempat']) }}</p>
                </div>
            </div>
        @endforeach
    </div>

    <div class="wrapper mdtopmargin nobottommargin">
        <div class="col-10">
            <p class="nobottommargin"><strong>III. Uraian Singkat Hasil Pengawasan</strong></p>
        </div>
    </div>
    <div class="wrapper">
        <div class="col-10">
            <p class="nobottommargin text-justify">{!! $pengawasan->uraian !!}</p>
        </div>
    </div>

    @if(isset($pelanggaran))
</div>
<div class="print page-break">
    @yield('pelanggaran')
    @endif

    <div class="wrapper lgtopmargin">
        <div class="col-5"></div>
        <div class="col-5 text-center">
            <p class="nobottommargin">Wonosobo, {{ \Carbon\Carbon::now()->formatLocalized('%d %B %Y')}}</p>
            <p class="nobottommargin"><strong>Petugas</strong></p>
        </div>
    </div>

    <div class="wrapper lgtopmargin">
        <div class="col-5"></div>
        <div class="col-5 text-center">
            <p class="nomargin mdtopmargin">_________________________</p>
        </div>
    </div>
    @if(isset($pelanggaran))
</div>
@endif
</div>
</body>
</html>