{{--<div class="form-group row align-items-center" :class="{'has-danger': errors.has('parent_id'), 'has-success': fields.parent_id && fields.parent_id.valid }">
    <label for="parent_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.parent_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.parent_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('parent_id'), 'form-control-success': fields.parent_id && fields.parent_id.valid}" id="parent_id" name="parent_id" placeholder="{{ trans('laporan::admin.pengawasan.columns.parent_id') }}">
        <div v-if="errors.has('parent_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('parent_id') }}</div>
    </div>
</div>--}}

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('tahapan_yg_diawasi'), 'has-success': fields.tahapan_yg_diawasi && fields.tahapan_yg_diawasi.valid }">
    <label for="tahapan_yg_diawasi" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.tahapan_yg_diawasi') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.tahapan_yg_diawasi" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('tahapan_yg_diawasi'), 'form-control-success': fields.tahapan_yg_diawasi && fields.tahapan_yg_diawasi.valid}" id="tahapan_yg_diawasi" name="tahapan_yg_diawasi" placeholder="{{ trans('laporan::admin.pengawasan.columns.tahapan_yg_diawasi') }}">
        <div v-if="errors.has('tahapan_yg_diawasi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tahapan_yg_diawasi') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama_pelaksana'), 'has-success': fields.nama_pelaksana && fields.nama_pelaksana.valid }">
    <label for="nama_pelaksana" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.nama_pelaksana') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama_pelaksana" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama_pelaksana'), 'form-control-success': fields.nama_pelaksana && fields.nama_pelaksana.valid}" id="nama_pelaksana" name="nama_pelaksana" placeholder="{{ trans('laporan::admin.pengawasan.columns.nama_pelaksana') }}">
        <div v-if="errors.has('nama_pelaksana')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama_pelaksana') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('jabatan'), 'has-success': fields.jabatan && fields.jabatan.valid }">
    <label for="jabatan" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.jabatan') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.jabatan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('jabatan'), 'form-control-success': fields.jabatan && fields.jabatan.valid}" id="jabatan" name="jabatan" placeholder="{{ trans('laporan::admin.pengawasan.columns.jabatan') }}">
        <div v-if="errors.has('jabatan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('jabatan') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nomor_surat_perintah'), 'has-success': fields.nomor_surat_perintah && fields.nomor_surat_perintah.valid }">
    <label for="nomor_surat_perintah" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.nomor_surat_perintah') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nomor_surat_perintah" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nomor_surat_perintah'), 'form-control-success': fields.nomor_surat_perintah && fields.nomor_surat_perintah.valid}" id="nomor_surat_perintah" name="nomor_surat_perintah" placeholder="{{ trans('laporan::admin.pengawasan.columns.nomor_surat_perintah') }}">
        <div v-if="errors.has('nomor_surat_perintah')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nomor_surat_perintah') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('alamat'), 'has-success': fields.alamat && fields.alamat.valid }">
    <label for="alamat" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.alamat') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea v-model="form.alamat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nomor_surat_perintah'), 'form-control-success': fields.nomor_surat_perintah && fields.nomor_surat_perintah.valid}" id="alamat" name="alamat"></textarea>
        </div>
        <div v-if="errors.has('alamat')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alamat') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('kegiatan_pengawasan'), 'has-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid }">
    <label for="kegiatan_pengawasan" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.kegiatan_pengawasan') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div v-for="(pengawasan, index) in form.kegiatan_pengawasan" :key="index">
            <div class="card">
                <header class="card-header">
                    Kegiatan @{{index+1}}
                    <div class="card-header-actions" v-if="index > 0">
                        <button @click="removePengawasan" type="button" class="card-header-action btn btn-danger">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </header>
                <div class="card-body">
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('kegiatan_pengawasan'), 'has-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid }">
                        <div class="col-md-6">
                            <label for="bentuk" class="col-form-label text-md-right">{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.bentuk') }}</label>
                            <input type="text" v-model="pengawasan.bentuk" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('kegiatan_pengawasan'), 'form-control-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid}" id="bentuk" :name="`kegiatan_pengawasan[${index}][bentuk]`" placeholder="{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.bentuk') }}">
                            <div v-if="errors.has('kegiatan_pengawasan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('kegiatan_pengawasan') }}</div>
                        </div>

                        <div class="col-md-6">
                            <label for="tujuan" class="col-form-label text-md-right">{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.tujuan') }}</label>
                            <input type="text" v-model="pengawasan.tujuan" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('kegiatan_pengawasan'), 'form-control-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid}" id="tujuan" :name="`kegiatan_pengawasan[${index}][tujuan]`" placeholder="{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.tujuan') }}">
                            <div v-if="errors.has('kegiatan_pengawasan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('kegiatan_pengawasan') }}</div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('kegiatan_pengawasan'), 'has-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid }">
                        <div class="col-md-6">
                            <label for="sasaran" class="col-form-label text-md-right">{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.sasaran') }}</label>
                            <input type="text" v-model="pengawasan.sasaran" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('kegiatan_pengawasan'), 'form-control-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid}" id="sasaran" :name="`kegiatan_pengawasan[${index}][sasaran]`" placeholder="{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.sasaran') }}">
                            <div v-if="errors.has('kegiatan_pengawasan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('kegiatan_pengawasan') }}</div>
                        </div>

                        <div class="col-md-6">
                            <label for="waktu_tempat" class="col-form-label text-md-right">{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.waktu_tempat') }}</label>
                            <input type="text" v-model="pengawasan.waktu_tempat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('kegiatan_pengawasan'), 'form-control-success': fields.kegiatan_pengawasan && fields.kegiatan_pengawasan.valid}" id="waktu_tempat" :name="`kegiatan_pengawasan[${index}][waktu_tempat]`" placeholder="{{ trans('laporan::admin.pengawasan.kegiatan_pengawasan.waktu_tempat') }}">
                            <div v-if="errors.has('kegiatan_pengawasan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('kegiatan_pengawasan') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button @click="addPengawasan" type="button" class="btn btn-sm btn-primary">
            <i class="fa fa-plus"></i> Add
        </button>
        <div v-if="errors.has('kegiatan_pengawasan')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('kegiatan_pengawasan') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('uraian'), 'has-success': fields.uraian && fields.uraian.valid }">
    <label for="uraian" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.uraian') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.uraian" v-validate="''" id="uraian" name="uraian" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('uraian')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('uraian') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('laporan::admin.pengawasan.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>


