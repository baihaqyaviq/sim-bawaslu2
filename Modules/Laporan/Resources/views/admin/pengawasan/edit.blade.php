@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('laporan::admin.pengawasan.actions.edit', ['name' => $pengawasan->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <pengawasan-form
                :action="'{{ $pengawasan->resource_url }}'"
                :data="{{ $pengawasan->toJson() }}"
                :pemilu="{{$pemilu->toJson()}}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('laporan::admin.pengawasan.actions.edit', ['name' => $pengawasan->id]) }}
                    </div>

                    <div class="card-body">
                        @include('laporan::admin.pengawasan.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </pengawasan-form>

        </div>
    
</div>

@endsection