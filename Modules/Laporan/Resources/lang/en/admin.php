<?php

return [
    'sidebar' => 'Laporan',
    'panwas' => [
        'title' => 'Panwas',

        'actions' => [
            'index' => 'Panwas',
            'create' => 'New Panwas',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat lahir',
            'tanggal_lahir' => 'Tanggal lahir',
            'jenis_kelamin' => 'Jenis kelamin',
            'agama_id' => 'Agama',
            'pekerjaan_id' => 'Pekerjaan',
            'status_kawin_id' => 'Status kawin',
            'no_spt' => 'No spt',
            'tgl_spt' => 'Tgl spt',
            'tingkat' => 'Tingkat',
            'jabatan' => 'Jabatan',
            'pendidikan' => 'Pendidikan',
            'pekerjaan' => 'Pekerjaan',
            'organisasi' => 'Organisasi',
            'penghargaan' => 'Penghargaan',
            'karyatulis' => 'Karya Tulis',
            /*'pendidikan' =>[
                'nama_sekolah' => 'Nama Sekolah',
                'jenjang' => 'Jenjang',
                'tahun_lulus' => 'Tahun Lulus'
            ],
            'pekerjaan' => [
                'nama_pekerjaan' => 'Nama Pekerjaan',
                'tahun_masuk' => 'Tahun Masuk',
                'tahun_keluar' => 'Tahun Keluar'
            ],
            'organisasi' => [
                'nama_organisasi' => 'Nama Organisasi',
                'tahun_awal' => 'Tahun Awal',
                'tahun_akhir' => 'Tahun Akhir'
            ],
            'penghargaan' => [
                'nama_penghargaan' => 'Nama Penghargaan',
                'tahun' => 'Tahun'
            ],
            'karyatulis' => [
                'nama_karya' => 'Nama Karya',
                'tahun' => 'Tahun'
            ],*/
            'pemilu_id' => 'Pemilu',
            'created_by' => 'Created by',
            'updated_by' => 'Updated by',
            'deleted_by' => 'Deleted by',
        ],

        'pendidikan' => [
            'nama_sekolah' => 'Nama Sekolah',
            'jenjang' => 'Jenjang',
            'tahun_lulus' => 'Tahun Lulus'
        ],
        'pekerjaan' => [
            'nama_pekerjaan' => 'Nama Pekerjaan',
            'tahun_masuk' => 'Tahun Masuk',
            'tahun_keluar' => 'Tahun Keluar'
        ],
        'organisasi' => [
            'nama_organisasi' => 'Nama Organisasi',
            'tahun_awal' => 'Tahun Awal',
            'tahun_akhir' => 'Tahun Akhir'
        ],
        'penghargaan' => [
            'nama_penghargaan' => 'Nama Penghargaan',
            'tahun' => 'Tahun'
        ],
        'karyatulis' => [
            'nama_karya' => 'Nama Karya',
            'tahun' => 'Tahun'
        ],
    ],

    'pelanggaran' => [
        'title' => 'Pelanggaran',

        'actions' => [
            'index' => 'Pelanggaran',
            'create' => 'New Pelanggaran',
            'edit' => 'Edit :name',
        ],

        'saksi_saksi' => [
            'nama' => 'Nama',
            'alamat' => 'Alamat'
        ],
        'alat_bukti' => [
            'keterangan' => 'Keterangan',
            'photo' => 'Photo'
        ],
        'barang_bukti' => [
            'keterangan' => 'Keterangan',
            'photo' => 'Photo'
        ],

        'columns' => [
            'id' => 'ID',
            'parent_id' => 'Parent',
            'pengawasan_id' => 'Pengawasan',
            'peristiwa' => 'Peristiwa',
            'tempat_kejadian' => 'Tempat kejadian',
            'waktu_kejadian' => 'Waktu kejadian',
            'pelaku' => 'Pelaku',
            'alamat_peristiwa' => 'Alamat peristiwa',
            'saksi_saksi' => 'Saksi saksi',
            'alat_bukti' => 'Alat bukti',
            'barang_bukti' => 'Barang bukti',
            'uraian' => 'Uraian',
            'pemilu_id' => 'Pemilu',
            'created_by' => 'Created by',
            'updated_by' => 'Updated by',
            'deleted_by' => 'Deleted by',

        ],
    ],

    'pengawasan' => [
        'title' => 'Pengawasan',

        'actions' => [
            'index' => 'Pengawasan',
            'create' => 'New Pengawasan',
            'edit' => 'Edit :name',
        ],

        'kegiatan_pengawasan' => [
            'bentuk' => 'Bentuk',
            'tujuan' => 'Tujuan',
            'sasaran' => 'Sasaran',
            'waktu_tempat' => 'Waktu & Tempat'
        ],

        'columns' => [
            'id' => 'ID',
            'parent_id' => 'Parent',
            'tahapan_yg_diawasi' => 'Tahapan yg diawasi',
            'nama_pelaksana' => 'Nama pelaksana',
            'jabatan' => 'Jabatan',
            'nomor_surat_perintah' => 'Nomor surat perintah',
            'alamat' => 'Alamat',
            'kegiatan_pengawasan' => 'Kegiatan pengawasan',
            'uraian' => 'Uraian',
            'pemilu_id' => 'Pemilu',
            'created_by' => 'Created by',
            'updated_by' => 'Updated by',
            'deleted_by' => 'Deleted by',

        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];