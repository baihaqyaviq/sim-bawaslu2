import AppListing from '@/app-components/Listing/AppListing';

Vue.component('panwas-listing', {
    mixins: [AppListing]
});