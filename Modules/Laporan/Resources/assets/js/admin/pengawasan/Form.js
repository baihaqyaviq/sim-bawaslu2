import AppForm from '@/app-components/Form/AppForm';

Vue.component('pengawasan-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function () {
        return {
            form: {
                parent_id: '',
                tahapan_yg_diawasi: '',
                nama_pelaksana: '',
                jabatan: '',
                nomor_surat_perintah: '',
                alamat: '',
                kegiatan_pengawasan: [{
                    bentuk: '',
                    tujuan: '',
                    sasaran: '',
                    waktu_tempat: ''
                }],
                uraian: '',
                pemilu: '',
            }
        }
    },
    methods: {
        addPengawasan() {
            this.form.kegiatan_pengawasan.push({
                bentuk: '',
                tujuan: '',
                sasaran: '',
                waktu_tempat: ''
            })
        },
        removePengawasan() {
            this.form.kegiatan_pengawasan.pop();
        },
    }
});