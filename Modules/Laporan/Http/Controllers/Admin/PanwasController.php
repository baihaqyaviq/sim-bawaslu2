<?php

namespace Modules\Laporan\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Laporan\Http\Requests\Admin\Panwas\BulkDestroyPanwas;
use Modules\Laporan\Http\Requests\Admin\Panwas\DestroyPanwas;
use Modules\Laporan\Http\Requests\Admin\Panwas\IndexPanwas;
use Modules\Laporan\Http\Requests\Admin\Panwas\StorePanwas;
use Modules\Laporan\Http\Requests\Admin\Panwas\UpdatePanwas;
use Modules\Laporan\Entities\Panwas;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Modules\Pemilu\Entities\Pemilu;

class PanwasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexPanwas $request
     * @return array|Factory|View
     */
    public function index(IndexPanwas $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Panwas::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['nik', 'nama', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nik', 'nama']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('laporan::admin.panwas.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.panwas.create');

        return view('laporan::admin.panwas.create',[
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePanwas $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StorePanwas $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Store the Panwas
        $panwas = Panwas::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/laporan-panwas'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/laporan-panwas');
    }

    /**
     * Display the specified resource.
     *
     * @param Panwas $panwas
     * @throws AuthorizationException
     * @return void
     */
    public function show(Panwas $panwas)
    {
        $this->authorize('admin.panwas.show', $panwas);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Panwas $panwas
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Panwas $panwas)
    {
        $this->authorize('admin.panwas.edit', $panwas);
        
        return view('laporan::admin.panwas.edit', [
            'panwas' => $panwas,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePanwas $request
     * @param Panwas $panwas
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdatePanwas $request, Panwas $panwas)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Update changed values Panwas
        $panwas->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/laporan-panwas'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/laporan-panwas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyPanwas $request
     * @param Panwas $panwas
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyPanwas $request, Panwas $panwas)
    {
        $panwas->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyPanwas $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyPanwas $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('panwas')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
