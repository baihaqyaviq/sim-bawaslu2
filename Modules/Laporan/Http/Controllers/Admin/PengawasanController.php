<?php

namespace Modules\Laporan\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Modules\Laporan\Http\Requests\Admin\Pengawasan\BulkDestroyPengawasan;
use Modules\Laporan\Http\Requests\Admin\Pengawasan\DestroyPengawasan;
use Modules\Laporan\Http\Requests\Admin\Pengawasan\IndexPengawasan;
use Modules\Laporan\Http\Requests\Admin\Pengawasan\StorePengawasan;
use Modules\Laporan\Http\Requests\Admin\Pengawasan\UpdatePengawasan;
use Modules\Laporan\Entities\Pengawasan;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Modules\Pemilu\Entities\Pemilu;
use PDF;

class PengawasanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexPengawasan $request
     * @return array|Factory|View
     */
    public function index(IndexPengawasan $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Pengawasan::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            ['id', 'nomor', 'parent_id', 'tahapan_yg_diawasi', 'nama_pelaksana', 'jabatan', 'nomor_surat_perintah', 'kegiatan_pengawasan', 'pemilu_id', 'created_by', 'updated_by', 'deleted_by'],

            // set columns to searchIn
            ['id', 'nomor']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return response()->json(['data' => $data]);
        }

        return view('laporan::admin.pengawasan.index', ['data' => $data]);
    }

    public function print(Pengawasan $pengawasan)
    {
        $nomor = str_replace('/', '_', $pengawasan->nomor);
        $pdf = PDF::loadView('laporan::admin.pengawasan.print', compact('pengawasan'));
        return $pdf->stream("Laporan_LHP_{$nomor}.pdf");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.pengawasan.create');

        return view('laporan::admin.pengawasan.create', [
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePengawasan $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StorePengawasan $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Store the Pengawasan
        $pengawasan = Pengawasan::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/laporan-pengawasans'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/laporan-pengawasans');
    }

    /**
     * Display the specified resource.
     *
     * @param Pengawasan $pengawasan
     * @return void
     * @throws AuthorizationException
     */
    public function show(Pengawasan $pengawasan)
    {
        $this->authorize('admin.pengawasan.show', $pengawasan);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pengawasan $pengawasan
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Pengawasan $pengawasan)
    {
        $this->authorize('admin.pengawasan.edit', $pengawasan);


        return view('laporan::admin.pengawasan.edit', [
            'pengawasan' => $pengawasan,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePengawasan $request
     * @param Pengawasan $pengawasan
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdatePengawasan $request, Pengawasan $pengawasan)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Update changed values Pengawasan
        $pengawasan->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/laporan-pengawasans'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/laporan-pengawasans');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyPengawasan $request
     * @param Pengawasan $pengawasan
     * @return ResponseFactory|RedirectResponse|Response
     * @throws Exception
     */
    public function destroy(DestroyPengawasan $request, Pengawasan $pengawasan)
    {
        $pengawasan->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyPengawasan $request
     * @return Response|bool
     * @throws Exception
     */
    public function bulkDestroy(BulkDestroyPengawasan $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('pengawasans')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
