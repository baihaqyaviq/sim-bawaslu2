<?php

namespace Modules\Laporan\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Modules\Laporan\Entities\Pengawasan;
use Modules\Laporan\Http\Requests\Admin\Pelanggaran\BulkDestroyPelanggaran;
use Modules\Laporan\Http\Requests\Admin\Pelanggaran\DestroyPelanggaran;
use Modules\Laporan\Http\Requests\Admin\Pelanggaran\IndexPelanggaran;
use Modules\Laporan\Http\Requests\Admin\Pelanggaran\StorePelanggaran;
use Modules\Laporan\Http\Requests\Admin\Pelanggaran\UpdatePelanggaran;
use Modules\Laporan\Entities\Pelanggaran;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Modules\Pemilu\Entities\Pemilu;
use PDF;

class PelanggaranController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexPelanggaran $request
     * @return array|Factory|View
     */
    public function index(IndexPelanggaran $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Pelanggaran::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            ['id', 'parent_id', 'pengawasan_id', 'peristiwa', 'tempat_kejadian', 'waktu_kejadian', 'saksi_saksi', 'alat_bukti', 'barang_bukti', 'pemilu_id', 'created_by', 'updated_by', 'deleted_by'],

            // set columns to searchIn
            ['id', 'peristiwa', 'tempat_kejadian', 'alamat_peristiwa', 'waktu_kejadian', 'saksi_saksi', 'alat_bukti', 'barang_bukti', 'uraian']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('laporan::admin.pelanggaran.index', ['data' => $data]);
    }

    public function print(Pelanggaran $pelanggaran)
    {
        $nomor = str_replace('/', '_', $pelanggaran->pengawasan->nomor);
        $pengawasan = $pelanggaran->pengawasan;
        $photoAlatBukti = $pelanggaran->getMedia('alat_bukti')->mapToGroups(function ($item) {
            return [$item->custom_properties['collection_name'] => [
                'original' => $item->getUrl(),
                'thumb' => $item->getUrl('thumb'),
                'name' => $item->name,
                'file_name' => $item->file_name,
                'collection_name' => $item->custom_properties['collection_name'],
            ]];
        });

        $photoBarangBukti = $pelanggaran->getMedia('barang_bukti')->mapToGroups(function ($item) {
            return [$item->custom_properties['collection_name'] => [
                'original' => $item->getUrl(),
                'thumb' => $item->getUrl('thumb'),
                'name' => $item->name,
                'file_name' => $item->file_name,
                'collection_name' => $item->custom_properties['collection_name'],
            ]];
        });

        //dd($photoAlatBukti, $photoBarangBukti);

        $pdf = PDF::loadView('laporan::admin.pelanggaran.print', compact('pelanggaran', 'pengawasan', 'photoAlatBukti', 'photoBarangBukti'));
        return $pdf->stream("Laporan_LHP_{$nomor}.pdf");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.pelanggaran.create');

        return view('laporan::admin.pelanggaran.create', [
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePelanggaran $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StorePelanggaran $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['pengawasan_id'] = $request->getPengawasanId();

        // Store the Pelanggaran
        $pelanggaran = Pelanggaran::create($sanitized);

        //dd($pelanggaran->getMedia('alat_bukti'), $request->barang_bukti, $request->alat_bukti);

        if ($request->ajax()) {
            return ['redirect' => url('admin/laporan-pelanggarans'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/laporan-pelanggarans');
    }

    /**
     * Display the specified resource.
     *
     * @param Pelanggaran $pelanggaran
     * @return void
     * @throws AuthorizationException
     */
    public function show(Pelanggaran $pelanggaran)
    {
        $this->authorize('admin.pelanggaran.show', $pelanggaran);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pelanggaran $pelanggaran
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Pelanggaran $pelanggaran)
    {
        $this->authorize('admin.pelanggaran.edit', $pelanggaran);


        return view('laporan::admin.pelanggaran.edit', [
            'pelanggaran' => $pelanggaran,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePelanggaran $request
     * @param Pelanggaran $pelanggaran
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdatePelanggaran $request, Pelanggaran $pelanggaran)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['pengawasan_id'] = $request->getPengawasanId();

        // Update changed values Pelanggaran
        $pelanggaran->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/laporan-pelanggarans'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/laporan-pelanggarans');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyPelanggaran $request
     * @param Pelanggaran $pelanggaran
     * @return ResponseFactory|RedirectResponse|Response
     * @throws Exception
     */
    public function destroy(DestroyPelanggaran $request, Pelanggaran $pelanggaran)
    {
        $pelanggaran->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyPelanggaran $request
     * @return Response|bool
     * @throws Exception
     */
    public function bulkDestroy(BulkDestroyPelanggaran $request): Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('pelanggarans')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
