<?php
namespace Modules\Laporan\Http\Requests\Admin\Panwas;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StorePanwas extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.panwas.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nik' => ['nullable', 'string'],
            'nama' => ['nullable', 'string'],
            'alamat' => ['nullable', 'string'],
            'tempat_lahir' => ['nullable', 'string'],
            'tanggal_lahir' => ['nullable', 'date'],
            'jenis_kelamin' => ['nullable', 'string'],
            'agama_id' => ['nullable', 'integer'],
            'status_kawin_id' => ['nullable', 'integer'],
            'no_spt' => ['nullable', 'string'],
            'tgl_spt' => ['nullable', 'date'],
            'tingkat' => ['nullable', 'string'],
            'jabatan' => ['nullable', 'integer'],
            'pendidikan' => ['nullable'],
            'pekerjaan' => ['nullable'],
            'organisasi' => ['nullable'],
            'penghargaan' => ['nullable'],
            'karyatulis' => ['nullable'],
            'pemilu' =>  ['required'],

        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
