<?php

namespace Modules\Laporan\Http\Requests\Admin\Panwas;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexPanwas extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.panwas.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'orderBy' => 'in:id,nik,nama,alamat,tempat_lahir,tanggal_lahir,jenis_kelamin,agama_id,pekerjaan_id,status_kawin_id,no_spt,tgl_spt,tingkat,jabatan,penghargaan,pendidikan,pekerjaan,organisasi,karyatulis,pemilu_id,created_by,updated_by,deleted_by|nullable',
            'orderDirection' => 'in:asc,desc|nullable',
            'search' => 'string|nullable',
            'page' => 'integer|nullable',
            'per_page' => 'integer|nullable',

        ];
    }
}
