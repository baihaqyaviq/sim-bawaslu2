<?php
namespace Modules\Laporan\Http\Requests\Admin\Pengawasan;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StorePengawasan extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.pengawasan.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent_id' => ['nullable', 'integer'],
            'tahapan_yg_diawasi' => ['nullable', 'string'],
            'nama_pelaksana' => ['nullable', 'string'],
            'jabatan' => ['nullable', 'string'],
            'nomor_surat_perintah' => ['nullable', 'string'],
            'alamat' => ['nullable', 'string'],
            'kegiatan_pengawasan' => ['nullable'],
            'uraian' => ['nullable', 'string'],
            'pemilu' => ['required'],

        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
