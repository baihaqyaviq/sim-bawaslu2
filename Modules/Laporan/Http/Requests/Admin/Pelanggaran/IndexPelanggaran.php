<?php

namespace Modules\Laporan\Http\Requests\Admin\Pelanggaran;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexPelanggaran extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.pelanggaran.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'orderBy' => 'in:id,parent_id,pengawasan_id,peristiwa,tempat_kejadian,waktu_kejadian,saksi_saksi,alat_bukti,barang_bukti,pemilu_id,created_by,updated_by,deleted_by|nullable',
            'orderDirection' => 'in:asc,desc|nullable',
            'search' => 'string|nullable',
            'page' => 'integer|nullable',
            'per_page' => 'integer|nullable',

        ];
    }
}
