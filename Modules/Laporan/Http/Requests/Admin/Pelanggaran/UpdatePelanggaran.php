<?php

namespace Modules\Laporan\Http\Requests\Admin\Pelanggaran;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePelanggaran extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.pelanggaran.edit', $this->pelanggaran);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent_id' => ['nullable', 'integer'],
            'pengawasan_id' => ['nullable', 'integer'],
            'peristiwa' => ['nullable', 'string'],
            'pelaku' => ['nullable', 'string'],
            'tempat_kejadian' => ['nullable', 'string'],
            'alamat_peristiwa' => ['nullable', 'string'],
            'waktu_kejadian' => ['nullable', 'string'],
            'saksi_saksi' => ['nullable'],
            'alat_bukti' => ['nullable'],
            'barang_bukti' => ['nullable'],
            'uraian' => ['nullable', 'string'],
            'pemilu' => ['required'],
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getPengawasanId(){
        if ($this->has('pengawasan')){
            return $this->get('pengawasan')['id'];
        }
        return null;
    }
}
