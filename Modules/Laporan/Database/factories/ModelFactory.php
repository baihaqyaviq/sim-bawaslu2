<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Laporan\Entities\Panwas::class, static function (Faker\Generator $faker) {
    return [
        'nik' => $faker->sentence,
        'nama' => $faker->sentence,
        'alamat' => $faker->sentence,
        'tempat_lahir' => $faker->sentence,
        'tanggal_lahir' => $faker->date(),
        'jenis_kelamin' => $faker->sentence,
        'agama_id' => $faker->randomNumber(5),
        'pekerjaan_id' => $faker->randomNumber(5),
        'status_kawin_id' => $faker->randomNumber(5),
        'no_spt' => $faker->sentence,
        'tgl_spt' => $faker->date(),
        'tingkat' => $faker->sentence,
        'jabatan' => $faker->randomNumber(5),
        'penghargaan' => $faker->sentence,
        'pendidikan' => $faker->sentence,
        'pekerjaan' => $faker->sentence,
        'organisasi' => $faker->sentence,
        'karyatulis' => $faker->sentence,
        'pemilu_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'created_by' => $faker->randomNumber(5),
        'updated_by' => $faker->randomNumber(5),
        'deleted_by' => $faker->randomNumber(5),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Laporan\Entities\Pelanggaran::class, static function (Faker\Generator $faker) {
    return [
        'parent_id' => $faker->randomNumber(5),
        'pengawasan_id' => $faker->randomNumber(5),
        'peristiwa' => $faker->sentence,
        'tempat_kejadian' => $faker->sentence,
        'alamat_peristiwa' => $faker->text(),
        'waktu_kejadian' => $faker->sentence,
        'saksi_saksi' => $faker->sentence,
        'alat_bukti' => $faker->sentence,
        'barang_bukti' => $faker->sentence,
        'uraian' => $faker->text(),
        'pemilu_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'created_by' => $faker->randomNumber(5),
        'updated_by' => $faker->randomNumber(5),
        'deleted_by' => $faker->randomNumber(5),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Laporan\Entities\Pengawasan::class, static function (Faker\Generator $faker) {
    return [
        'parent_id' => $faker->randomNumber(5),
        'tahapan_yg_diawasi' => $faker->sentence,
        'nama_pelaksana' => $faker->sentence,
        'jabatan' => $faker->sentence,
        'nomor_surat_perintah' => $faker->sentence,
        'alamat' => $faker->text(),
        'kegiatan_pengawasan' => $faker->sentence,
        'uraian' => $faker->text(),
        'pemilu_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'created_by' => $faker->randomNumber(5),
        'updated_by' => $faker->randomNumber(5),
        'deleted_by' => $faker->randomNumber(5),
        
        
    ];
});
