<?php

namespace Modules\Laporan\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elifbyte\AdminGenerator\Generate\Traits\Uuid;

class Panwas extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    protected $table = 'sim_panwas';

    protected $fillable = [
        'nik',
        'nama',
        'alamat',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'agama_id',
        'pekerjaan_id',
        'status_kawin_id',
        'no_spt',
        'tgl_spt',
        'tingkat',
        'jabatan',
        'penghargaan',
        'pendidikan',
        'pekerjaan',
        'organisasi',
        'karyatulis',

    ];

    protected $casts = [
        'pendidikan' => 'array',
        'pekerjaan' => 'array',
        'organisasi' => 'array',
        'penghargaan' => 'array',
        'karyatulis' => 'array',
    ];

    protected $dates = [
        'tanggal_lahir',
        'tgl_spt',
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/laporan-panwas/' . $this->getKey());
    }

    /* ************************ RELATIONS ************************ */

    /**
     * Relation to RefAgama
     *
     * @return BelongsTo
     */
    public function agama()
    {
        return $this->belongsTo(Modules\Laporan\Entities\RefAgama::class, 'agama_id', 'id');
    }

    /**
     * Relation to RefPekerjaan
     *
     * @return BelongsTo
     */
    public function pekerjaan()
    {
        return $this->belongsTo(Modules\Laporan\Entities\RefPekerjaan::class, 'pekerjaan_id', 'id');
    }

    /**
     * Relation to RefStatusPerkawinan
     *
     * @return BelongsTo
     */
    public function status_perkawinan()
    {
        return $this->belongsTo(Modules\Laporan\Entities\RefStatusPerkawinan::class, 'status_perkawinan_id', 'id');
    }


}
