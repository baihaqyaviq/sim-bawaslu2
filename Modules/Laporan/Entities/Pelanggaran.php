<?php

namespace Modules\Laporan\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Brackets\Media\Exceptions\FileCannotBeAdded\TooManyFiles;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\MediaCollection;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elifbyte\AdminGenerator\Generate\Traits\Uuid;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Pelanggaran extends Model implements HasMedia
{
    use SoftDeletes, PemiluTrait, UserTrait;

    use ProcessMediaTrait, AutoProcessMediaTrait, HasMediaCollectionsTrait, HasMediaThumbsTrait;

    protected $table = 'sim_pelanggaran';

    protected $fillable = [
        'parent_id',
        'pengawasan_id',
        'peristiwa',
        'pelaku',
        'tempat_kejadian',
        'alamat_peristiwa',
        'waktu_kejadian',
        'saksi_saksi',
        'alat_bukti',
        'barang_bukti',
        'uraian',
    ];

    protected $casts = [
        'saksi_saksi' => 'array',
        'alat_bukti' => 'array',
        'barang_bukti' => 'array',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    protected $with = ['pengawasan'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('alat_bukti')
            ->accepts('image/*')
            ->maxNumberOfFiles(6);

        $this->addMediaCollection('barang_bukti')
            ->accepts('image/*')
            ->maxNumberOfFiles(6);
    }

    public function registerMediaConversions(Media $media = null)
    {
        //$this->autoRegisterThumb200();
        $this->addMediaConversion('thumb')
            ->width(350)
            ->height(200)
            ->optimize();
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/laporan-pelanggarans/' . $this->getKey());
    }

    /* ************************ RELATIONS ************************ */

    /**
     * Relation to SimPengawasan
     *
     * @return BelongsTo
     */
    public function pengawasan()
    {
        return $this->belongsTo(Pengawasan::class, 'pengawasan_id', 'id');
    }

    /* ************************ OVERRIDE METHOD ************************ */

    /**
     * override ProcessMediaTrait
     * @author aviq baihaqy
     * @param Collection $inputMedia
     */
    public function processMedia(Collection $inputMedia): void
    {
        //First validate input
        $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
            $this->validate(collect($inputMedia->get($mediaCollection->getName())), $mediaCollection);
        });

        //Then process each media
        $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
            collect($inputMedia->get($mediaCollection->getName()))->each(function ($photo, $index) use ($mediaCollection) {
                collect($photo['photo'])->each(function ($inputMedium) use ($mediaCollection, $index) {
                    $inputMedium['meta_data'][$mediaCollection->getName()] = $index + 1;
                    $inputMedium['meta_data']['collection_name'] = $inputMedium['collection_name'];

                    $this->processMedium($inputMedium, $mediaCollection);
                });
            });
        });
    }

    /**
     * override ProcessMediaTrait
     * @author aviq baihaqy
     * @param Collection $inputMediaForMediaCollection
     * @param MediaCollection $mediaCollection
     * @throws TooManyFiles
     */
    public function validate(Collection $inputMediaForMediaCollection, MediaCollection $mediaCollection): void
    {
        $this->validateCollectionMediaCount($inputMediaForMediaCollection, $mediaCollection);

        $inputMediaForMediaCollection->each(function ($photo) use ($mediaCollection) {
            collect($photo['photo'])->each(function ($inputMedium) use ($mediaCollection) {
                if ($inputMedium['action'] === 'add') {
                    $mediumFileFullPath = Storage::disk('uploads')->getDriver()->getAdapter()->applyPathPrefix($inputMedium['path']);
                    $this->validateTypeOfFile($mediumFileFullPath, $mediaCollection);
                    $this->validateSize($mediumFileFullPath, $mediaCollection);
                }
            });
        });
    }

    /**
     * override ProcessMediaTrait
     * @author aviq baihaqy
     * @param Collection $inputMediaForMediaCollection
     * @param MediaCollection $mediaCollection
     * @throws TooManyFiles
     */
    public function validateCollectionMediaCount(
        Collection $inputMediaForMediaCollection,
        MediaCollection $mediaCollection
    ): void
    {
        if ($mediaCollection->getMaxNumberOfFiles()) {

            $inputMediaForMediaCollection->each(function ($photo) use ($mediaCollection) {

                $forAddMediaCount = collect($photo['photo'])->filter(static function ($medium) {
                    return $medium['action'] === 'add';
                })->count();

                $forDeleteMediaCount = collect($photo['photo'])->filter(static function ($medium) {
                    return $medium['action'] === 'delete' ? 1 : 0;
                })->count();

                $afterUploadCount = ($forAddMediaCount - $forDeleteMediaCount);

                //dd($afterUploadCount, $alreadyUploadedMediaCount, $forDeleteMediaCount, $mediaCollection->getMaxNumberOfFiles());

                if ($afterUploadCount > $mediaCollection->getMaxNumberOfFiles()) {
                    throw TooManyFiles::create($mediaCollection->getMaxNumberOfFiles(), $mediaCollection->getName());
                }
            });

        }
    }

}
