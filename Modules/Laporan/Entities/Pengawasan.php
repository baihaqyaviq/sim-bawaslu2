<?php

namespace Modules\Laporan\Entities;

use App\Traits\NomoratorTrait;
use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elifbyte\AdminGenerator\Generate\Traits\Uuid;

class Pengawasan extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait, NomoratorTrait;

    protected $table = 'sim_pengawasan';

    protected $fillable = [
        'parent_id',
        'tahapan_yg_diawasi',
        'nama_pelaksana',
        'jabatan',
        'nomor_surat_perintah',
        'alamat',
        'kegiatan_pengawasan',
        'uraian',
    ];

    protected $casts = [
        'kegiatan_pengawasan' => 'array'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->nomor = Pengawasan::generateNomor('LHP');
        });
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/laporan-pengawasans/' . $this->getKey());
    }

}
