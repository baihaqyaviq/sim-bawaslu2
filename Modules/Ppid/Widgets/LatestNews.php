<?php

namespace Modules\Ppid\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Vedmant\FeedReader\Facades\FeedReader;

class LatestNews extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //$webRss = FeedReader::read('http://simplepie.org/blog/feed');
        $webRss = FeedReader::read(env('WEB') . '/rss.xml')->get_items();
        //dd($webRss);
        //$webRss->init();
        //$webRss->handle_content_type();

        //dd($webRss->get_item()->get_description());
        //dd(substr(strip_tags($webRss->get_description()), 0, 200));
        return view('ppid::public.widgets.latest_news', compact('webRss'));
    }
}
