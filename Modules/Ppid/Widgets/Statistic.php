<?php

namespace Modules\Ppid\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Modules\Ppid\Entities\PermohonanInformasi;
use Modules\Ppid\Entities\PermohonanKeberatan;

class Statistic extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $informasi = PermohonanInformasi::all();
        $keberatan = PermohonanKeberatan::all();
        $informasiStatus = $informasi->countBy('status');
        $keberatanStatus = $keberatan->countBy('status');

        // $statistic = collect([
        //     'count_informasi' => $informasi->count(),
        //     'count_keberatan' => $keberatan->count(),
        //     'count_ditolak' => ($informasiStatus->count() ? $informasiStatus['ditolak'] : 0 ) + ($keberatanStatus->count() ? $keberatanStatus['ditolak'] : 0),
        //     'count_dikabulkan' => ($informasiStatus->count() ? $informasiStatus['dikabulkan'] : 0) + ($keberatanStatus->count() ? $keberatanStatus['dikabulkan'] : 0),
        // ]);

        $statistic = collect([
            'count_informasi' => 14,
            'count_keberatan' => 0,
            'count_ditolak' => 0,
            'count_dikabulkan' => 14,
        ]);

        return view('ppid::public.widgets.statistic', compact('statistic'));
    }
}
