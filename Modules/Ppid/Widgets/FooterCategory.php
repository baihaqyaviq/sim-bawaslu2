<?php

namespace Modules\Ppid\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Modules\Ppid\Entities\Category;

class FooterCategory extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'slug'=>''
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $category = Category::with('children')->where('slug', $this->config['slug'])->first();

        return view('ppid::public.widgets.footer_category', compact('category'));
    }
}
