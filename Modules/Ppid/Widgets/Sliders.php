<?php

namespace Modules\Ppid\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Storage;

class Sliders extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $contents = collect(Storage::cloud()->listContents('1MJEXoELwIJacmN37JrLHgopQkQmvupeD', false))
            ->sortBy('timestamp');
        $contents->splice(5);

        return view('ppid::public.widgets.sliders', compact('contents'));
    }
}
