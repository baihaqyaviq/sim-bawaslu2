<?php

namespace Modules\Ppid\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Gbuckingham89\YouTubeRSSParser\Parser;
use Illuminate\Support\Arr;

class LatestVideos extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $parser = new Parser();
        $rss_url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' . Arr::last(explode('/',env('YOUTUBE')));
        $youtube = $parser->loadUrl($rss_url);
        $video = collect($youtube->videos)->sortByDesc('updated_at')->take(5);

        return view('ppid::public.widgets.latest_video', compact('video'));
    }
}
