<?php

return [
    'sidebar' => 'PPID',
    'page' => [
        'title' => 'Pages',

        'actions' => [
            'index' => 'Pages',
            'create' => 'New Page',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'views_count' => 'Views count',
            'slug' => 'Slug',
            'body' => 'Body',
            'template' => 'Template',
            'status' => 'Status',
            'icon' => 'Icon',

        ],
    ],
    'article' => [
        'title' => 'Articles',

        'actions' => [
            'index' => 'Articles',
            'create' => 'New Article',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'short_text' => 'Short text',
            'full_text' => 'Full text',
            'category_id' => 'Category',
            'views_count' => 'Views count',
            'slug' => 'Slug',

        ],
    ],

    'category' => [
        'title' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',

        ],
    ],

    'tag' => [
        'title' => 'Tags',

        'actions' => [
            'index' => 'Tags',
            'create' => 'New Tag',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',

        ],
    ],

    'faq-category' => [
        'title' => 'Faq Categories',

        'actions' => [
            'index' => 'Faq Categories',
            'create' => 'New Faq Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'category' => 'Category',

        ],
    ],

    'faq-question' => [
        'title' => 'Faq Questions',

        'actions' => [
            'index' => 'Faq Questions',
            'create' => 'New Faq Question',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'question' => 'Question',
            'answer' => 'Answer',
            'category_id' => 'Category',

        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
