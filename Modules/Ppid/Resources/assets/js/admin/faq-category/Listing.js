import AppListing from '@/app-components/Listing/AppListing';

Vue.component('faq-category-listing', {
    mixins: [AppListing]
});