import AppListing from '@/app-components/Listing/AppListing';

Vue.component('faq-question-listing', {
    mixins: [AppListing]
});