import AppForm from '@/app-components/Form/AppForm';

Vue.component('faq-question-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                question:  '' ,
                answer:  '' ,
                category_id:  '' ,
                
            }
        }
    }

});