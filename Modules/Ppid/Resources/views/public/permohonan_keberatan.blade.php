@extends('ppid::layouts.master')
@section('content')
    <div class="g-bg-img-hero" style="background-image: url(/themes/default/include/svg/svg-bg1.svg);">
        <!-- Entry Fees & How to Apply -->
        <div class="container g-pt-100 g-pb-100">
            <div class="row align-items-lg-center no-gutters">
                <div class="col-lg-12 g-mb-50 g-mb-20--lg">
                    @include('ppid::public.partials.notifications')

                    <form action="{{route('ppid/post/permohonan/keberatan')}}" method="post"
                          enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!-- Entry Fees & How to Apply -->
                        <div class="u-shadow-v36">
                            <div class="g-bg-white-opacity-0_7">
                                <header class="g-brd-bottom g-brd-main-opacity-0_1 text-center g-pa-30">
                                    <h3 class="h4 g-color-text g-font-primary g-font-weight-400 mb-0">Formulir
                                        Permohonan Kebearatan</h3>
                                </header>

                                <div class="g-pa-40">
                                    <p class="g-color-text text-center g-mb-40">Sampaikan permohonan
                                        keberatan dengan dengan benar agar permintaan dapat segera
                                        diproses</p>

                                    <!-- Select Inputs -->
                                    <div class="row">
                                        <div class="col-6 g-mb-20">
                                            <!-- Start In -->
                                            <label class="g-color-text g-font-size-15 mb-3">Kategori
                                                Permohonan</label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="2018"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up"
                                                    name="kategori_permohonan">
                                                @foreach ($kategori as $key => $label)
                                                    <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                            value="{{$key}}">{{$label}}
                                                    </option>
                                                @endforeach

                                            </select>
                                            <!-- End Start In -->
                                        </div>

                                        <div class="col-6 g-mb-20">
                                            <!-- I am -->
                                            <label class="g-color-text g-font-size-15 mb-3">Instansi</label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="a Canadian citizen"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up"
                                                    name="instansi">
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="1">Bawaslu Kab. Wonosobo
                                                </option>
                                            </select>
                                            <!-- End I am -->
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Nama</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <input type="text" name="nama"
                                                   class="w-100 g-brd-around g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"/>
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Alamat</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <input type="text" name="alamat"
                                                   class="w-100 g-brd-around g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-4 g-mb-20">
                                            <label class="g-color-text g-font-size-15 mb-3">Pekerjaan</label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="2018"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up"
                                                    name="pekerjaan">
                                                @foreach ($pekerjaan as $key => $label)
                                                    <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                            value="{{$key}}">{{$label}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-4 g-mb-20">
                                            <label class="g-color-text g-font-size-15 mb-3">No
                                                Hanphone</label>
                                            <input type="text" name="no_hp"
                                                   class="w-100 g-brd-around g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"/>
                                        </div>

                                        <div class="col-4 g-mb-20">
                                            <label class="g-color-text g-font-size-15 mb-3">Email</label>
                                            <input type="text" name="email"
                                                   class="w-100 g-brd-around g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"/>
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Nama Kuasa (Optional)</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <textarea
                                                    class="w-100 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    name="nama_kuasa"></textarea>
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Alamat Kuasa (Optional)</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <textarea
                                                    class="w-100 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    name="alamat_kuasa"></textarea>
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        No. HP Kuasa (Optional)</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <textarea
                                                    class="w-100 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    name="no_hp_kuasa"></textarea>
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Tujuan pengajuan informasi</h3>
                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <textarea
                                                    class="w-100 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    name="tujuan"></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 g-mb-20">
                                            <!-- Start In -->
                                            <label class="g-color-text g-font-size-15 mb-3">Alasan Penggajuan Keberatan</label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="2018"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up"
                                                    name="alasan">
                                                @foreach ($alasan as $key => $label)
                                                    <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                            value="{{$key}}">{{$label}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <!-- End Start In -->
                                        </div>
                                    </div>

                                    <h3 class="g-color-text g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Upload File (KTP/Surat Kuasa/Tambahan kasus posisi Max 2Mb PDF/PNG/JPG/JPEG)</h3>

                                    <div class="form-group mb-20">
                                        <div class="input-group u-file-attach-v1 g-brd-gray-light-v2">
                                            <input id="inputGroup1_2"
                                                   class="form-control form-control-md rounded-0 g-brd-main-opacity-0_1 g-color-text g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                   type="text" placeholder="ktp.jpg">

                                            <div class="input-group-append">
                                                <button class="btn btn-md g-color-blue g-color-white--hover g-bg-main-light-v1 rounded-0" type="submit">
                                                    Browse
                                                </button>
                                                <input type="file" name="upload_doc[]" multiple>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <footer>
                                <button class="btn btn-block g-color-blue g-color-white--hover g-bg-main-light-v1 g-font-size-16 text-center rounded-0 g-pa-20"
                                        href="#">
                                    Kirim Permohonan
                                    <i class="g-font-size-15 g-pos-rel g-top-4 ml-2 material-icons">arrow_forward</i>
                                </button>
                            </footer>
                        </div>
                        <!-- End Entry Fees & How to Apply -->
                    </form>
                </div>
            </div>
        </div>
        <!-- End Entry Fees & How to Apply -->
    </div>
@stop