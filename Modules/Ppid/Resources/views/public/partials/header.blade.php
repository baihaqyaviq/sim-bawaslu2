<!-- Header -->
<header id="js-header" class="u-header">
    <div class="u-header__section">
        <!-- Topbar -->
        <div class="g-bg-main g-pt-5 g-pb-5">
            <div class="container g-py-5">
                <ul class="list-inline d-flex align-items-center g-mb-0">

                    <!-- Jump To -->
                    <li class="list-inline-item g-pos-rel ml-lg-auto">
                        <a id="jump-to-dropdown-invoker"
                           class="d-block d-lg-none u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-py-7"
                           href="#"
                           aria-controls="jump-to-dropdown"
                           aria-haspopup="true"
                           aria-expanded="false"
                           data-dropdown-event="hover"
                           data-dropdown-target="#jump-to-dropdown"
                           data-dropdown-type="css-animation"
                           data-dropdown-duration="0"
                           data-dropdown-hide-on-scroll="true"
                           data-dropdown-animation-in="fadeIn"
                           data-dropdown-animation-out="fadeOut">
                            Menu
                            <i class="g-ml-3 fa fa-angle-down"></i>
                        </a>
                        <ul id="jump-to-dropdown"
                            class="list-unstyled u-shadow-v39 g-brd-around g-brd-4 g-brd-white g-bg-secondary g-pos-abs g-left-0 g-z-index-99 g-mt-13"
                            aria-labelledby="jump-to-dropdown-invoker">
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('ppid/permohonan/informasi')}}">Permohonan Informasi</a>
                            </li>
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('ppid/permohonan/keberatan')}}">Permohonan Keberatan</a>
                            </li>
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('ppid/get/kontak')}}">Kontak</a>
                            </li>
                        </ul>
                    </li>
                    <!-- End Jump To -->

                    <!-- Links -->
                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('ppid/permohonan/informasi')}}">Permohonan Informasi</a>
                    </li>

                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('ppid/permohonan/keberatan')}}">Permohonan Keberatan</a>
                    </li>

                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('ppid/get/kontak')}}">kontak</a>
                    </li>
                    <!-- End Links -->

                    <!-- Search -->
                    <li class="list-inline-item g-ml-15--lg ml-auto">
                        <form action="{{route('ppid/search')}}" method="get" id="searchform-1"
                              class="input-group u-shadow-v19 g-brd-primary--focus g-rounded-20">
                            <input
                                class="form-control g-brd-none g-bg-white g-font-size-12 text-uppercase g-rounded-left-20 g-pl-20 g-py-9"
                                type="text" placeholder="Search here ..." name="q">
                            <button
                                class="btn input-group-addon d-flex align-items-center g-brd-none g-color-white g-bg-primary g-bg-primary-light-v1--hover g-font-size-13 g-rounded-right-20 g-transition-0_2"
                                type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </li>
                    <!-- End Search -->
                </ul>
            </div>
        </div>
        <!-- End Topbar -->

        <div class="container">
            <!-- Nav -->
            <nav class="js-mega-menu navbar navbar-expand-lg g-px-0 g-py-5 g-py-0--lg">
                <!-- Logo -->
                <a class="navbar-brand g-max-width-170 g-max-width-200--lg" href="{{route('ppid/home')}}">
                    <img class="img-fluid g-hidden-lg-down" src="{{url('images/logo.png')}}"
                         alt="Logo">
                    <img class="img-fluid g-width-80 g-hidden-md-down g-hidden-xl-up"
                         src="{{url('images/logo.png')}}" alt="Logo">
                    <img class="img-fluid g-hidden-lg-up" src="{{url('images/logo.png')}}" alt="Logo">
                </a>
                <!-- End Logo -->

                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0"
                        type="button"
                        aria-label="Toggle navigation"
                        aria-expanded="false"
                        aria-controls="navBar"
                        data-toggle="collapse"
                        data-target="#navBar">
                <span class="hamburger hamburger--slider g-px-0">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Navigation -->
                <div id="navBar" class="collapse navbar-collapse">
                    <ul class="navbar-nav align-items-lg-center g-py-30 g-py-0--lg ml-auto">
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('ppid/home')}}">
                                Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('ppid/page')}}" class="nav-link g-px-0" id="nav-link-1"
                               aria-haspopup="true"
                               aria-expanded="false" aria-controls="nav-submenu-1">
                                Profile PPID
                            </a>
                            <!-- Submenu -->
                        {{--<ul class="hs-sub-menu list-unstyled g-text-transform-none u-shadow-v39 g-brd-around g-brd-7 g-brd-white g-bg-secondary g-min-width-200 g-mt-20 g-mt-10--lg--scrolling animated"
                            id="nav-submenu-1" aria-labelledby="nav-link-1" style="display: none;">
                            <li class="dropdown-item">
                                <a class="nav-link g-px-0" href="{{route('ppid/page',['slug'=>'profil-singkat-ppid'])}}">Profil Singkat PPID</a>
                            </li>
                            <li class="dropdown-item">
                                <a class="nav-link g-px-0" href="{{route('ppid/page',['slug'=>'visi-misi'])}}">Visi Misi</a>
                            </li>
                            <li class="dropdown-item">
                                <a class="nav-link g-px-0" href="{{route('ppid/page',['slug'=>'tugas-fungsi-wewenang'])}}">Tugas Fungsi & Wewenang</a>
                            </li>
                            <li class="dropdown-item">
                                <a class="nav-link g-px-0" href="{{route('ppid/page',['slug'=>'struktur-ppid'])}}">Struktur PPID</a>
                            </li>
                        </ul>--}}
                        <!-- End Submenu -->
                        </li>
                        <li class="nav-item hs-has-mega-menu"
                            data-animation-in="fadeIn"
                            data-animation-out="fadeOut"
                            data-position="left">
                            <a id="mega-menu-label-1"
                               class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="#"
                               aria-haspopup="true"
                               aria-expanded="false">
                                Informasi Publik
                                <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7"></i>
                            </a>

                            <!-- Mega Menu -->
                            <div
                                class="w-100 hs-mega-menu u-shadow-v39 g-brd-around g-brd-7 g-brd-white g-bg-secondary g-text-transform-none g-pa-20 g-pa-30--lg g-my-10 g-my-0--lg"
                                aria-labelledby="mega-menu-label-1">

                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <span class="d-block h4 g-brd-bottom g-brd-2 g-brd-main pb-2">Daftar Informasi Publik</span>
                                        @widget('ppid::menuCategory', ['slug' => 'informasi-publik'])
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <span
                                            class="d-block h4 g-brd-bottom g-brd-2 g-brd-main pb-2">Daftar Regulasi</span>
                                        @widget('ppid::menuCategory', ['slug' => 'daftar-regulasi'])
                                    </div>

                                </div>
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <li class="nav-item hs-has-mega-menu"
                            data-animation-in="fadeIn"
                            data-animation-out="fadeOut"
                            data-position="left">
                            <a id="mega-menu-label-1"
                               class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="#"
                               aria-haspopup="true"
                               aria-expanded="false">
                                Layanan Informasi
                                <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7"></i>
                            </a>

                            <!-- Mega Menu -->
                            <div
                                class="w-100 hs-mega-menu u-shadow-v39 g-brd-around g-brd-7 g-brd-white g-bg-secondary g-text-transform-none g-pa-20 g-pa-30--lg g-my-10 g-my-0--lg"
                                aria-labelledby="mega-menu-label-1">

                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <span class="d-block h4 g-brd-bottom g-brd-2 g-brd-main pb-2">Form Online</span>
                                        <ul class="list-unstyled g-pr-30 mb-0">
                                            <li class="py-2">
                                                <a class="d-flex g-color-main g-color-primary--hover g-text-underline--none--hover g-py-5"
                                                   href="{{ route('ppid/permohonan/informasi') }}">Permohonan Informasi
                                                </a>
                                            </li>
                                            <li class="py-2">
                                                <a class="d-flex g-color-main g-color-primary--hover g-text-underline--none--hover g-py-5"
                                                   href="{{ route('ppid/permohonan/keberatan') }}">Permohonan Keberatan
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <span class="d-block h4 g-brd-bottom g-brd-2 g-brd-main pb-2">Prosedur</span>
                                        @widget('ppid::menuCategory', ['slug' => 'prosedur'])
                                    </div>

                                </div>
                            </div>
                            <!-- End Mega Menu -->
                        </li>

                        <li class="nav-item hs-has-sub-menu hs-sub-menu-opened" data-animation-in="fadeIn" data-animation-out="fadeOut">
                            <a href="#" class="nav-link g-px-0 g-font-size-15 g-font-size-17--xl " id="nav-link-1" aria-haspopup="true"
                               aria-expanded="false" aria-controls="nav-submenu-1">
                                Link Terkait
                            </a>
                            <!-- Submenu -->
                            <ul class="hs-sub-menu list-unstyled g-text-transform-none u-shadow-v39 g-brd-around g-brd-7 g-brd-white g-bg-secondary g-min-width-200 g-mt-20 g-mt-10--lg--scrolling animated"
                                id="nav-submenu-1" aria-labelledby="nav-link-1" style="display: none;">
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" target="_blank" href="https://wonosobo.bawaslu.go.id/">Bawaslu
                                        Wonosobo</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" target="_blank" href="https://jateng.bawaslu.go.id/">Bawaslu
                                        Jawa Tengah</a>
                                </li>

                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" target="_blank" href="https://www.bawaslu.go.id/">Bawaslu
                                        Pusat</a>
                                </li>

                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" target="_blank"
                                       href="https://ppid.jateng.bawaslu.go.id/">PPID Bawaslu Jawa Tengah</a>
                                </li>
                                <li class="dropdown-item">
                                    <a class="nav-link g-px-0" target="_blank" href="https://ppid.bawaslu.go.id/">PPID
                                        Bawaslu Pusat</a>
                                </li>
                            </ul>
                            <!-- End Submenu -->
                        </li>

                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('ppid/faq/index')}}">
                                FAQ
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Navigation -->
            </nav>
            <!-- End Nav -->
        </div>
    </div>
</header>
<!-- End Header -->
