@extends('ppid::layouts.master')
@section('content')
    <!-- Alumni Articles -->
    <div class="g-pos-rel">
        <div class="container">
            <div class="row justify-content-lg-between">
                <div id="sub-navigation" class="col-md-4 col-lg-3 g-pt-50">

                    <div class="js-sticky-block u-secondary-navigation" data-start-point="#sub-navigation"
                         data-end-point="999999" data-type="responsive">
                        <h3 class="mb-4">Profil PPID</h3>
                        <!-- Links List -->
                        <ul id="js-scroll-nav" class="list-unstyled mb-5 nav">
                            @foreach ($pages as $page)
                                <li class="py-1 nav-item">
                                    <a class="nav-link d-block u-link-v5 u-shadow-v35--active g-color-text-light-v1 g-color-main--hover g-color-primary--active g-bg-white--hover g-bg-white--active g-font-size-15 g-rounded-20 g-px-20 g-py-8"
                                       href="#{{$page->slug}}">
                                        <i class="align-middle mr-3 {{$page->icon}} u-line-icon-pro"></i>
                                        {!! $page->title !!}
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                        <!-- Twitter Feed -->
                        {{--<a class="twitter-timeline" data-lang="id" data-height="400" data-dnt="true" tweet-limit="10"
                           href="https://twitter.com/BawasluWonosobo?ref_src=twsrc%5Etfw">Tweets by BawasluWonosobo</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--}}
                        <!-- End Twitter Feed -->
                        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                        <div class="elfsight-app-74dcd694-94e5-46e8-8701-067ab659b50e" style="width:310px"></div>
                        <!-- End Links List -->
                    </div>


                </div>

                <div class="col-md-8 g-pt-50 g-pb-70">
                    @foreach ($pages as $page)
                        <div id="{{$page->slug}}">
                            <h3 class="mb-4">{!! $page->title !!}</h3>
                            <article>
                                <p class="g-font-size-16">{!! $page->body!!}</p>
                            </article>
                        </div>
                        <hr>
                    @endforeach
                </div>
            </div>

            <div
                class="col-12 col-md-5 col-lg-4 h-100 g-bg-secondary-gradient-v1 g-pos-abs g-top-0 g-left-0 g-z-index-minus-1"></div>
        </div>
    </div>
    <!-- End Alumni Articles -->
@stop

@push('scripts')
    <script src="{{url('themes/default/js/components/hs.scroll-nav.js')}}"></script>
    <script src="{{url('themes/default/js/components/hs.sticky-block.js')}}"></script>

    <script>
        $(window).on('load', function () {
            // initialization of HSScrollNav
            $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
                duration: 700,
                over: $('.u-secondary-navigation')
            });

            // initialization of sticky blocks
            $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
        });
    </script>
@endpush
