@extends('ppid::layouts.master')
@section('content')
    <!-- Help Center -->
    <div class="g-bg-img-hero" style="background-image: url(/themes/default/include/svg/svg-bg1.svg);">
        <div class="container g-pt-100 g-pb-150">
            <div class="g-max-width-840 mx-auto">
                <!-- Heading -->
                <div class="g-max-width-840 text-center mx-auto g-mb-40">
                    <h1 class="g-font-size-40--md mb-3">Frequently Asked Questions</h1>
                </div>
                <!-- End Heading -->

                <ul class="nav justify-content-center u-nav-v5-1 u-nav-primary g-brd-bottom--md g-brd-gray-light-v4"
                    role="tablist" data-target="nav-5-1-primary-hor-center-border-bottom"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block u-btn-outline-primary">
                    @foreach ($categories as $category)
                        <li class="nav-item">
                            <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                               href="#nav-{{$category->id}}" role="tab">{{$category->category}}</a>
                        </li>
                    @endforeach
                </ul>

                <div id="nav-5-1-primary-hor-center-border-bottom" class="tab-content g-pt-20">
                    @foreach ($categories as $category)
                        <div class="tab-pane fade show @if($loop->first) active @endif" id="nav-{{$category->id}}"
                             role="tabpanel">
                            <div id="accordion-{{$category->id}}" class="u-accordion" role="tablist" aria-multiselectable="true">
                                @foreach ($category->faqQuestions as $question)
                                    <div class="card rounded-0 g-mb-5 u-block-hover u-shadow-v35 u-shadow-v37--hover g-brd-around g-brd-secondary-light-v2 rounded g-pos-rel">
                                        <div id="accordion-03-heading-{{ $question->id }}" class="u-accordion__header"
                                             role="tab">
                                            <h5 class="mb-0">
                                                <a class="d-flex u-link-v5 g-color-main g-color-primary--hover g-font-size-16 "
                                                   href="#accordion-{{$category->id}}-body-{{ $question->id }}" data-toggle="collapse"
                                                   data-parent="#accordion-{{$category->id}}"
                                                   aria-expanded="true"
                                                   aria-controls="accordion-{{$category->id}}-body-{{ $question->id }}">{{ $question->question }}
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="accordion-{{$category->id}}-body-{{ $question->id }}" class="collapse" role="tabpanel"
                                             aria-labelledby="accordion-{{$category->id}}-heading-{{ $question->id }}" data-parent="#accordion-{{$category->id}}">
                                            <div class="u-accordion__body g-brd-top g-brd-gray-light-v4 g-color-gray-dark-v1">
                                                {{ $question->answer }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Help Center -->
@stop