@extends('ppid::layouts.master')
@section('content')
    <div class="container g-pt-70">
        <div class="row">
            <div class="col-lg-4 g-mb-70">
                <div id="stickyblock-start">
                    <div class="js-sticky-block g-sticky-block--lg pt-4" data-responsive="true"
                         data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">

                        <!-- Sidebar Links -->
                        @if ($category->children)
                            <h3 class="h4 mb-4">Kategori</h3>

                            <ul class="list-unstyled g-mb-50">
                                @if ($category->parent)
                                    <li class="mb-1">
                                        <a class="d-block u-link-v5 g-color-white g-color-white--hover g-bg-primary g-bg-main--hover g-font-size-default rounded g-pl-30--hover g-px-20 g-py-7"
                                           href="{{ route('ppid/detail', [ $category->parent->slug]) }}">
                                            <i class="g-font-size-13 g-pos-rel g-top-2 mr-2 material-icons">arrow_forward</i>
                                            {{$category->parent->name}}
                                        </a>
                                    </li>
                                @endif
                                @foreach ($category->children as $cat)
                                    <li class="mb-1">
                                        <a class="d-block u-link-v5 g-color-text g-color-white--hover g-bg-secondary g-bg-main--hover g-font-size-default rounded g-pl-30--hover g-px-20 g-py-7"
                                           href="{{ route('ppid/detail', [ $cat->slug]) }}">
                                            <i class="g-font-size-13 g-pos-rel g-top-2 mr-2 material-icons">arrow_forward</i>
                                            {{$cat->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                    @endif
                    <!-- End Sidebar Links -->

                        {{--<a class="twitter-timeline" data-lang="id" data-height="400" data-dnt="true" tweet-limit="10"
                           href="https://twitter.com/BawasluWonosobo?ref_src=twsrc%5Etfw">Tweets by BawasluWonosobo</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--}}
                        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                        <div class="elfsight-app-74dcd694-94e5-46e8-8701-067ab659b50e"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 g-mb-70">
                <div class="mb-4">
                    <h2>{{$category->name}}</h2>
                </div>

                <div class="g-px-10 mb-5">
                    <!-- Nav tabs -->
                    @if($files)
                    <ul class="nav u-nav-v3-1" role="tablist" data-target="nav-3-1-default-hor-left"
                        data-tabs-mobile-type="slide-up-down"
                        data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">

                        @foreach ($files as $key => $file)
                            <li class="nav-item">
                                <a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#{{$key}}"
                                   role="tab">{{ strtoupper($key) }}</a>
                            </li>
                        @endforeach
                    </ul>
                    @endif
                    <!-- End Nav tabs -->

                    <!-- Tab panes -->
                    <div id="nav-1-2-primary-hor-center" class="tab-content g-pt-20">
                        @forelse ($files as $key => $file)
                            <div class="tab-pane fade show @if($loop->first) active @endif" id="{{$key}}"
                                 role="tabpanel">
                                @if ($key == 'dokumen')
                                    <div class="row g-bg-main g-color-white g-font-size-16 g-py-15">
                                        <div class="col-sm-1">
                                            <div class="d-flex align-items-center">
                                                <h2 class="h5 mb-0">
                                                    <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                       href="#">
                                                        No
                                                    </a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="d-flex align-items-center">
                                                <h2 class="h5 mb-0">
                                                    <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                       href="#">
                                                    </a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="d-flex align-items-center">
                                                <h2 class="h5 mb-0">
                                                    <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                       href="#">
                                                        Nama File
                                                    </a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="d-flex align-items-center">
                                                <h3 class="h5 mb-0">
                                                    <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                       href="#">
                                                        View
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Heading -->
                                    @foreach ($files['dokumen'] as $file)
                                        <div
                                            class="row g-brd-around g-brd-top-none g-brd-secondary-light-v2 g-font-size-16 g-py-15">
                                            <div class="col-sm-1">
                                                <span>{{$loop->iteration}}</span>
                                            </div>
                                            <div class="col-sm-1">
                                                <span><img src="{{$file['iconLink']}}"></span>
                                            </div>
                                            <div class="col-sm-8">
                                                <span>{{ Str::title($file['filename']) }}</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="{{$file['url']}}" target="_blank">View</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                                @if($key == 'gambar')
                                    <div class="masonry-grid">
                                        <div class="masonry-grid-sizer g-width-16_6x--sm"></div>
                                        @foreach ($files['gambar'] as $image)
                                            <div
                                                class="masonry-grid-item u-block-hover {{($image['imageMediaMetadata']->witdh > $image['imageMediaMetadata']->height)?'g-width-40x--sm' : 'g-width-25x--sm'}}">
                                                <a class="js-fancybox d-block u-bg-overlay g-bg-black-opacity-0_3--after g-opacity-0_8--hover g-transition-0_3"
                                                   href="javascript:;" data-fancybox="lightbox-gallery--07"
                                                   data-src="{{Str::replaceFirst('=s220','',$image['thumbnailLink'])}}"
                                                   data-caption="Lightbox Gallery">
                                                    <img class="img-fluid u-block-hover__main--zoom-v1"
                                                         src="{{$image['thumbnailLink']}}"
                                                         alt="{{$image['description']}}">
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                @if($key == 'lainnya')
                                        <div class="row g-bg-main g-color-white g-font-size-16 g-py-15">
                                            <div class="col-sm-1">
                                                <div class="d-flex align-items-center">
                                                    <h2 class="h5 mb-0">
                                                        <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                           href="#">
                                                            No
                                                        </a>
                                                    </h2>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <div class="d-flex align-items-center">
                                                    <h2 class="h5 mb-0">
                                                        <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                           href="#">
                                                        </a>
                                                    </h2>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="d-flex align-items-center">
                                                    <h2 class="h5 mb-0">
                                                        <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                           href="#">
                                                            Nama File
                                                        </a>
                                                    </h2>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="d-flex align-items-center">
                                                    <h3 class="h5 mb-0">
                                                        <a class="u-link-v5 g-color-white-opacity-0_8 g-color-white--hover"
                                                           href="#">
                                                            View
                                                        </a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Heading -->
                                        @foreach ($files['lainnya'] as $file)
                                            <div
                                                class="row g-brd-around g-brd-top-none g-brd-secondary-light-v2 g-font-size-16 g-py-15">
                                                <div class="col-sm-1">
                                                    <span>{{$loop->iteration}}</span>
                                                </div>
                                                <div class="col-sm-1">
                                                    <span><img src="{{$file['iconLink']}}"></span>
                                                </div>
                                                <div class="col-sm-8">
                                                    <span>{{ Str::title($file['filename']) }}</span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="{{$file['url']}}" target="_blank">View</a>
                                                </div>
                                            </div>
                                        @endforeach
                                @endif
                            </div>
                        @empty
                            <div class="alert alert-dismissible fade show g-bg-red g-color-white rounded-0" role="alert">
                                <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <div class="media">
                                    <span class="d-flex g-mr-10 g-mt-5">
                                      <i class="icon-ban g-font-size-25"></i>
                                    </span>
                                    <span class="media-body align-self-center">
                                      <strong>Maaf !</strong> Belum ada dokumen
                                    </span>
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <!-- End Tab panes -->
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src="{{url('themes/default/js/components/hs.sticky-block.js')}}"></script>
    <script src="{{url('themes/default/js/components/hs.tabs.js')}}"></script>
    <script src="{{url('vendor/masonry/dist/masonry.pkgd.min.js')}}"></script>
    <script src="{{url('vendor/imagesloaded/imagesloaded.js')}}"></script>

    <script>
        $(document).on('ready', function () {
            // initialization of sticky blocks
            setTimeout(function () {
                $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
            }, 300);
            // initialization of tabs
            $.HSCore.components.HSTabs.init('[role="tablist"]');

        });

        $('a[data-toggle=tab]').each(function () {
            var $this = $(this);

            $this.on('shown.bs.tab', function () {
                // initialization of masonry
                $('.masonry-grid').imagesLoaded().then(function () {
                    $('.masonry-grid').masonry({
                        // columnWidth: '.masonry-grid-sizer',
                        itemSelector: '.masonry-grid-item',
                        percentPosition: true,
                        horizontalOrder: true,
                    });
                });
            });
        });

        $(window).on('resize', function () {
            setTimeout(function () {
                $.HSCore.components.HSTabs.init('[role="tablist"]');
            }, 200);
        });
    </script>
@endpush
