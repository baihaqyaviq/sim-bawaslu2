<div class="g-bg-secondary">
    <div class="container g-mt-minus-70 g-pb-50">
        <div class="row">
            <div class="col-sm-6 col-lg-3 g-mb-30 g-mb-20--lg">
                <!-- Research Block -->
                <div class="g-pa-10">
                    <div class="g-width-220 g-height-220 u-shadow-v37 rounded-circle g-pos-rel mx-auto">
                        <!-- Research - SVG Shape -->
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px"
                             viewBox="0 0 187 187" enable-background="new 0 0 187 187" xml:space="preserve">
                              <circle fill="#ffffff" cx="93.5" cy="93.5" r="93.5"/>
                            <circle fill="#ffffff" stroke="#49c1aa" stroke-width="7" stroke-miterlimit="10" cx="93.5"
                                    cy="93.5" r="81"/>
                        </svg>
                        <!-- End Research - SVG Shape -->

                        <!-- Research - Info -->
                        <div class="text-center g-absolute-centered--y g-left-0 g-right-0 g-px-35">
                            <h4 class="h2 g-font-primary">{{$statistic['count_informasi']}}</h4>
                            <span class="d-block">Jumlah Permohonan Informasi</span>
                        </div>
                        <!-- Research - Info -->
                    </div>
                </div>
                <!-- End Research Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30 g-mb-20--lg">
                <!-- Research Block -->
                <div class="g-pa-10">
                    <div class="g-width-220 g-height-220 u-shadow-v37 rounded-circle g-pos-rel mx-auto">
                        <!-- Research - SVG Shape -->
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px"
                             viewBox="0 0 187 187" enable-background="new 0 0 187 187" xml:space="preserve">
                            <circle fill="#ffffff" cx="93.5" cy="93.5" r="93.5"/>
                            <circle fill="#ffffff" stroke="#a27fcc" stroke-width="7" stroke-miterlimit="10"
                                    cx="93.5"
                                    cy="93.5" r="81"/>
                        </svg>
                        <!-- End Research - SVG Shape -->

                        <!-- Research - Info -->
                        <div class="text-center g-absolute-centered--y g-left-0 g-right-0 g-px-35">
                            <h4 class="h2 g-font-primary">{{$statistic['count_keberatan']}}</h4>
                            <span class="d-block">Jumlah Permohonan keberatan</span>
                        </div>
                        <!-- Research - Info -->
                    </div>
                </div>
                <!-- End Research Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30 g-mb-20--lg">
                <!-- Research Block -->
                <div class="g-pa-10">
                    <div class="g-width-220 g-height-220 u-shadow-v37 rounded-circle g-pos-rel mx-auto">
                        <!-- Research - SVG Shape -->
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px"
                             viewBox="0 0 187 187" enable-background="new 0 0 187 187" xml:space="preserve">
                            <circle fill="#ffffff" cx="93.5" cy="93.5" r="93.5"/>
                            <circle fill="#ffffff" stroke="#61a9d1" stroke-width="7" stroke-miterlimit="10"
                                    cx="93.5"
                                    cy="93.5" r="81"/>
                        </svg>
                        <!-- End Research - SVG Shape -->

                        <!-- Research - Info -->
                        <div class="text-center g-absolute-centered--y g-left-0 g-right-0 g-px-35">
                            <h4 class="h2 g-font-primary">{{$statistic['count_dikabulkan']}}</h4>
                            <span class="d-block">Jumlah Permohonan dikabulkan</span>
                        </div>
                        <!-- Research - Info -->
                    </div>
                </div>
                <!-- End Research Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30 g-mb-20--lg">
                <!-- Research Block -->
                <div class="g-pa-10">
                    <div class="g-width-220 g-height-220 u-shadow-v37 rounded-circle g-pos-rel mx-auto">
                        <!-- Research - SVG Shape -->
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px"
                             viewBox="0 0 187 187" enable-background="new 0 0 187 187" xml:space="preserve">
                            <circle fill="#ffffff" cx="93.5" cy="93.5" r="93.5"/>
                            <circle fill="#ffffff" stroke="#d16680" stroke-width="7" stroke-miterlimit="10"
                                    cx="93.5"
                                    cy="93.5" r="81"/>
                        </svg>
                        <!-- End Research - SVG Shape -->

                        <!-- Research - Info -->
                        <div class="text-center g-absolute-centered--y g-left-0 g-right-0 g-px-35">
                            <h4 class="h2 g-font-primary">{{$statistic['count_ditolak']}}</h4>
                            <span class="d-block">Jumlah Permohonan di tolak</span>
                        </div>
                        <!-- Research - Info -->
                    </div>
                </div>
                <!-- End Research Block -->
            </div>
        </div>
        <div class="g-max-width-300 text-center mx-auto">
            <p class="g-color-secondary-light-v1 mb-0">Data Statistik Jumlah Permohonan Informasi dan Permohonan Keberatan</p>
        </div>
    </div>
</div>
