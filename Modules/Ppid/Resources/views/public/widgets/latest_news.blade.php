@if ($webRss)
<div class="container g-pt-50 g-pb-50">
    <div class="g-px-30--lg">
        <!-- Heading -->
        <div class="g-max-width-645 text-center mx-auto g-mb-30">
            <h2 class="h1 mb-3">Berita Terbaru</h2>
            <p>Berita Terbaru dari website resmi BAWASLU Kab. Wonosobo</p>
        </div>
        <!-- End Heading -->

        <!-- News Carousel -->
        <div class="js-carousel u-carousel-v5 g-mx-minus-15"
             data-slides-show="3"
             data-slides-scroll="1"
             data-arrows-classes="u-icon-v3 u-icon-size--sm g-absolute-centered--y g-color-primary g-color-white--hover g-bg-primary-opacity-0_1 g-bg-primary--hover rounded-circle g-pa-11"
             data-arrow-left-classes="fa fa-angle-left g-left-0 g-ml-minus-50--lg"
             data-arrow-right-classes="fa fa-angle-right g-right-0 g-mr-minus-50--lg"
             data-pagi-classes="u-carousel-indicators-v35 g-pos-rel text-center g-mt-30"
             data-responsive='[{
                 "breakpoint": 992,
                 "settings": {
                   "slidesToShow": 2
                 }
               }, {
                 "breakpoint": 768,
                 "settings": {
                   "slidesToShow": 1
                 }
               }, {
                 "breakpoint": 554,
                 "settings": {
                   "slidesToShow": 1
                 }
               }]'>

            <!-- News -->
            @foreach ($webRss as $web)
                {{--<div class="js-slide u-shadow-v38 g-bg-size-cover g-bg-pos-center rounded g-mx-15 g-my-30"
                     style="background-image: url(themes/default/img-temp/400x500/img1.jpg);">
                    <article class="align-self-end text-center g-pos-rel g-z-index-1 g-pa-40 mx-auto">
                        <h3 class="g-color-white">{{$web->get_title()}}</h3>
                        <div class="mt-4">
                            <span class="d-block g-color-white g-font-size-16 mb-2">Bawaslu Wonosobo</span>
                        </div>
                        <a class="u-link-v2" href="{{$web->get_permalink()}}" target="__blank"></a>
                    </article>
                </div>--}}
                <div class="js-slide g-flex-centered u-shadow-v38 rounded g-mx-15 g-my-30">
                    <article class="g-pa-40">
                        <blockquote class="g-brd-left-none g-color-main-dark-v3 g-font-size-18 g-pl-0 mb-5">
                            {!! substr(strip_tags($web->get_title()), 0, 45) !!}
                        </blockquote>
                        <div class="text-center mb-3">
                            <span class="d-block g-color-text-light-v1 g-font-size-16 mb-2">BAWASLU Kab. Wonosobo</span>
                        </div>
                        <a class="btn btn-block g-color-primary g-color-white--hover g-bg-primary-opacity-0_1 g-bg-primary--hover g-rounded-20 g-py-10"
                           href="{{$web->get_permalink()}}">Read more</a>
                    </article>
                </div>
            @endforeach
        <!-- End News -->

            {{--<!-- News -->
            <div class="js-slide g-flex-centered u-shadow-v38 rounded g-mx-15 g-my-30">
                <article class="g-pa-40">
                    <blockquote class="g-brd-left-none g-color-main-dark-v3 g-font-size-18 g-pl-0 mb-5">" Unify
                        welcomes the downtown to uptown in advance of the IT Technology. At the welcome ceremony,
                        Lenton announced a new award in the IT Technology in support of increased access to
                        postsecondary education. "
                    </blockquote>
                    <div class="text-center mb-3">
                        <span class="d-block g-color-text-light-v1 g-font-size-16 mb-2">Keith Margaret</span>
                        <div class="d-inline-block g-width-40 g-height-40">
                            <img class="img-fluid g-brd-around g-brd-2 g-brd-primary-opacity-0_1 rounded-circle"
                                 src="{{url('themes/default/img-temp/100x100/img2.jpg') }}" alt="Image Description">
                        </div>
                    </div>
                    <a class="btn btn-block g-color-primary g-color-white--hover g-bg-primary-opacity-0_1 g-bg-primary--hover g-rounded-20 g-py-10"
                       href="page-blog-single-item-1.html">Read more</a>
                </article>
            </div>
            <!-- End News -->

            <!-- News -->
            <div class="js-slide u-shadow-v38 g-bg-size-cover g-bg-pos-center rounded g-mx-15 g-my-30"
                 style="background-image: url(themes/default/img-temp/400x500/img3.jpg);">
                <article class="align-self-end text-center g-pos-rel g-z-index-1 g-pa-40 mx-auto">
                    <h3 class="g-color-white">The Fashion Gallery of Unify welcomes downtown to the future of
                        Ontario.</h3>
                    <div class="mt-4">
                        <span class="d-block g-color-white g-font-size-16 mb-2">Tina Krueger</span>
                        <div class="d-inline-block g-width-40 g-height-40">
                            <img class="img-fluid g-brd-around g-brd-2 g-brd-primary-opacity-0_1 rounded-circle"
                                 src="{{url('themes/default/img-temp/100x100/img5.jpg') }}" alt="Image Description">
                        </div>
                    </div>
                    <a class="u-link-v2" href="page-blog-single-item-1.html"></a>
                </article>
            </div>
            <!-- End News -->

            <!-- News -->
            <div class="js-slide g-flex-centered u-shadow-v38 rounded g-mx-15 g-my-30">
                <article class="g-pa-40">
                    <blockquote class="g-brd-left-none g-color-main-dark-v3 g-font-size-18 g-pl-0 mb-5">" Augustana
                        Long Range Development Plan Open House - Sept. 28. The University of Unify will be holding a
                        public open house that will provide an update on the amendment of land use plans for the
                        Augustana Campus. "
                    </blockquote>
                    <div class="text-center mb-3">
                        <span class="d-block g-color-text-light-v1 g-font-size-16 mb-2">Neyton Burchie</span>
                        <div class="d-inline-block g-width-40 g-height-40">
                            <img class="img-fluid g-brd-around g-brd-2 g-brd-primary-opacity-0_1 rounded-circle"
                                 src="{{url('themes/default/img-temp/100x100/img3.jpg') }}" alt="Image Description">
                        </div>
                    </div>
                    <a class="btn btn-block g-color-primary g-color-white--hover g-bg-primary-opacity-0_1 g-bg-primary--hover g-rounded-20 g-py-10"
                       href="page-blog-single-item-1.html">Read more</a>
                </article>
            </div>
            <!-- End News -->

            <!-- News -->
            <div class="js-slide u-shadow-v38 g-bg-size-cover g-bg-pos-center rounded g-mx-15 g-my-30"
                 style="background-image: url(themes/default/img-temp/400x500/img2.jpg);">
                <article class="align-self-end text-center g-pos-rel g-z-index-1 g-pa-40 mx-auto">
                    <h3 class="g-color-white">University of Unify joins new network to drive defence research</h3>
                    <div class="mt-4">
                        <span class="d-block g-color-white g-font-size-16 mb-2">Liza Nelson</span>
                        <div class="d-inline-block g-width-40 g-height-40">
                            <img class="img-fluid g-brd-around g-brd-2 g-brd-primary-opacity-0_1 rounded-circle"
                                 src="{{url('themes/default/img-temp/100x100/img4.jpg') }}" alt="Image Description">
                        </div>
                    </div>
                    <a class="u-link-v2" href="page-blog-single-item-1.html"></a>
                </article>
            </div>
            <!-- End News -->--}}
        </div>
        <!-- End News Carousel -->
    </div>
</div>
@endif

