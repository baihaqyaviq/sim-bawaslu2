<div class="col-6 col-md-3 g-mb-20">
    <!-- Footer Links -->
    <ul class="list-unstyled">
        <li class="g-py-5"><a
                    class="u-link-v5 g-color-footer-links g-color-primary--hover g-font-size-16"
                    href="{{ route('ppid/detail', [ $category->slug]) }}"><strong>{{$category->name}}</strong></a></li>
        @foreach ($category->children as $category)
            <li class="g-py-5"><a
                        class="u-link-v5 g-color-footer-links g-color-primary--hover g-font-size-16"
                        href="{{ route('ppid/detail', [ $category->slug]) }}">{{$category->name}}</a></li>
        @endforeach
    </ul>
    <!-- End Footer Links -->
</div>