<div class="g-bg-main g-pos-rel">
    <div class="container g-pos-rel g-z-index-1 g-pt-50 g-pb-120">
        <!-- Heading -->
        <div class="g-max-width-645 text-center mx-auto g-mb-60">
            <h2 class="h1 text-center g-color-white">Video Terbaru</h2>
        </div>
        <!-- End Heading -->

        <div class="row align-items-lg-center">
            <div class="col-lg-7 order-lg-2 g-mb-50">
                <div class="g-pos-rel g-pl-15--lg">
                    <img class="img-fluid w-100 u-shadow-v19 g-brd-around g-brd-7 g-brd-secondary rounded"
                         src="{{$video->first()->thumbnail_url}}" alt="{{$video->first()->title}}">

                    <!-- Video Button -->
                    <div class="g-absolute-centered--y g-left-0 g-right-0 text-center mx-auto">
                        <a class="js-fancybox d-inline-block u-block-hover g-bg-white g-rounded-50 g-text-underline--none--hover g-transition-0_3 g-px-20 g-py-10"
                           href="javascript:;"
                           data-src="{{$video->first()->url}}"
                           data-speed="350">
                        <span class="d-flex align-items-center">
                          <span
                              class="u-icon-v3 g-width-30 g-height-30 g-color-white g-bg-primary g-bg-main--hover rounded-circle mr-3">
                            <i class="g-font-size-13 g-ml-2 hs-icon hs-icon-play"></i>
                          </span>
                          <span class="g-color-primary g-color-main--hover">Play video</span>
                        </span>
                        </a>
                    </div>
                    <!-- End Video Button -->
                </div>
            </div>

            <div class="col-lg-5 order-lg-1 g-mb-50">
                <!-- Researches - Info -->
                <div class="mb-5">
                    <h2 class="h3 g-color-white mb-4">{{$video->first()->title}}</h2>
                    <p class="g-color-white-opacity-0_7">{!! $video->first()->description !!}</p>
                </div>
                <!-- End Researches - Info -->
            </div>
        </div>

        <div class="card-group d-block d-md-flex g-mx-minus-15">
            <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-b458a3a5-c216-4752-b822-eb8a3fa8727f"></div>

            {{--@foreach($video->take(-4) as $vid)
                <div class="card u-shadow-v36 g-brd-none g-mx-15">
                    <!-- Research Article -->
                    <article>
                        <div class="g-pos-rel">
                            <img class="img-fluid" src="{{$vid->thumbnail_url}}" alt="{{$vid->title}}">
                            <a class="js-fancybox g-absolute-centered" href="javascript:;"
                               data-src="{{$video->first()->url}}" data-speed="350" data-caption="Video">
                    <span
                        class="u-icon-v3 u-icon-size--lg u-block-hover--scale g-color-main g-color-white--hover g-bg-white-opacity-0_5 g-bg-white-opacity-0_3--hover g-font-size-25 rounded-circle">
                      <i class="g-pos-rel g-left-2 fa fa-play"></i>
                    </span>
                            </a>
                        </div>
                        <div class="card-body g-bg-white text-center g-pa-20">
                            <h5 class="h5 mb-0">{{$vid->title}}</h5>
                        </div>
                    </article>
                    <!-- End Research Article -->
                </div>
            @endforeach--}}

        </div>
    </div>


    <!-- SVG Background Shape -->
    <svg class="g-pos-abs g-bottom-0 g-left-0" xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 1920 323" enable-background="new 0 0 1920 323" xml:space="preserve">
        <polygon fill="#2c3856" points="0,323 1920,323 1920,0 "/>
        <polygon fill="#293451" points="-0.5,322.5 -0.5,131.5 658.3,212.3 "/>
     </svg>
    <!-- End SVG Background Shape -->
</div>
