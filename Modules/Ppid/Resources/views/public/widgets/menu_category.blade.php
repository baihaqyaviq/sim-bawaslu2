<ul class="list-unstyled g-pr-30 mb-0">
    @foreach ($category->children as $category)
        <li class="py-2">
            <a class="d-flex g-color-main g-color-primary--hover g-text-underline--none--hover g-py-5"
               href="{{ route('ppid/detail', [$category->slug]) }}">{{ Str::title($category->name) }}
            </a>
        </li>
    @endforeach
</ul>