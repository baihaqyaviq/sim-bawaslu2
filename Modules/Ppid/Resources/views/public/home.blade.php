@extends('ppid::layouts.master')
@section('content')
    <!-- Carousel Slider -->
    @widget('ppid::sliders', ['slug' => 'prosedur'])
    <!-- End Carousel Slider -->

    <!-- Find a informasi -->
    <div id="content" class="u-shadow-v34 g-bg-main g-pos-rel g-z-index-1 g-pt-40 g-pb-10">
        <div class="container">
            <form action="{{route('ppid/search')}}" method="get" class="row align-items-md-center">
                <div class="col-md-4 g-mb-30">
                    <h1 class="h2 g-color-white mb-0">Cari Informasi</h1>
                </div>

                <div class="col-md-6 g-mb-30">
                    <input class="form-control h-100 u-shadow-v19 g-brd-none g-bg-white g-font-size-16 g-rounded-30 g-px-25 g-py-13"
                           type="text" placeholder="Masukkan Nama Informasi" name="q">
                </div>

                <div class="col-md-2 g-mb-30">
                    <button class="btn u-shadow-v32 input-group-addon d-flex align-items-center g-brd-none g-color-white g-color-primary--hover g-bg-primary g-bg-white--hover g-font-size-16 g-rounded-30 g-transition-0_2 g-px-30 g-py-13"
                            type="submit">
                        Cari
                        <i class="ml-3 fa fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <!-- End Find a informasi -->

    <!-- News -->
    @widget('ppid::latestNews', ['slug' => 'prosedur'])
    <!-- End News -->

    <!-- latest videos -->
    @widget('ppid::latestVideos', ['slug' => 'prosedur'])
    <!-- End latest videos-->

    <!-- Research Statistics -->
    @widget('ppid::statistic', ['slug' => 'prosedur'])
    <!-- End Research Statistics -->

    <!-- Call to Action -->
    <div class="g-pos-rel">
        <div class="container text-center g-pt-100 g-pb-50">
            <!-- Heading -->
            <div class="g-max-width-645 mx-auto g-mb-40">
                <h2 class="h1 mb-3">PPID BAWASLU KAB. WONOSOBO</h2>
                <p>Sampaikan permintaan informasi anda dengan cara datang langsung ke Kantor BAWASLU Kab. Wonosobo atau
                    kirim langsung melalui form
                    permohonan informasi / permintaan keberatan dibawah ini</p>
            </div>
            <!-- End Heading -->

            <a class="g-mt-20 btn u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13"
               href="{{route('ppid/permohonan/informasi')}}">Permohonan Informasi</a>

            <!-- SVG Shape -->
            <svg class="g-width-35 d-none d-md-inline-block" version="1.1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 37 1" enable-background="new 0 0 37 1" xml:space="preserve">
            <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="0" y1="0.5" x2="37" y2="0.5">
                <stop offset="0" style="stop-color:#f5f6fa"/>
                <stop offset="1" style="stop-color:#b5b8cb"/>
            </linearGradient>
                <line fill="none" stroke="url(#SVGID_5_)" stroke-miterlimit="10" x1="37" y1="0.5" x2="0" y2="0.5"/>
          </svg>
            <!-- End SVG Shape -->

            <span class="align-middle g-color-primary mx-1 d-none d-md-inline-block">or</span>

            <!-- SVG Shape -->
            <svg class="g-width-35 d-none d-md-inline-block" version="1.1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 37 1" enable-background="new 0 0 37 1" xml:space="preserve">
            <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-10" y1="-1.5" x2="27" y2="-1.5"
                            gradientTransform="matrix(-1 0 0 -1 27 -1)">
                <stop offset="0" style="stop-color:#f5f6fa"/>
                <stop offset="1" style="stop-color:#b5b8cb"/>
            </linearGradient>
                <line fill="none" stroke="url(#SVGID_6_)" stroke-miterlimit="10" x1="0" y1="0.5" x2="37" y2="0.5"/>
          </svg>
            <!-- End SVG Shape -->

            <a class="g-mt-20 g-brd-1 g-brd-red btn u-shadow-v32 g-color-primary g-color-white--hover g-bg-white g-bg-main--hover g-rounded-30 g-px-35 g-py-13"
               href="{{route('ppid/permohonan/keberatan')}}">Permohonan Keberatan</a>
        </div>

        <!-- SVG Background Shape -->
        <svg class="g-pos-abs g-bottom-0 g-left-0 g-z-index-minus-1" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 1920 323" enable-background="new 0 0 1920 323" xml:space="preserve">
          <polygon fill="#f0f2f8" points="0,323 1920,323 1920,0 "/>
            <polygon fill="#f5f6fa" points="-0.5,322.5 -0.5,131.5 658.3,212.3 "/>
        </svg>
        <!-- End SVG Background Shape -->
    </div>
    <!-- End Call to Action -->
@stop
