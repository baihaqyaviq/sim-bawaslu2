@extends('ppid::layouts.master')
@section('content')
    <!-- Help Center -->
    <div class="g-bg-img-hero" style="background-image: url(/themes/default/include/svg/svg-bg1.svg);">
        <div class="container g-pt-100 g-pb-150">
            <div class="g-max-width-645 mx-auto">
                <!-- Heading -->
                <div class="g-max-width-645 text-center mx-auto g-mb-40">
                    <h1 class="g-font-size-40--md mb-3">Hi, Apa yang bisa kami bantu?</h1>
                    <p>Search our help center for quick answers.</p>
                </div>
                <!-- End Heading -->

                <!-- Help Form -->
                <form action="{{route('ppid/search')}}" method="get" class="input-group">
                    <input class="form-control u-shadow-v35 g-brd-secondary-light-v2 g-rounded-left-30 g-px-30 g-py-12"
                           type="text" placeholder="Start typing to see suggestions" name="q">
                    <div class="input-group-btn">
                        <button type="submit"
                                class="btn u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-right-30 g-px-25 g-py-10">
                            Cari <i class="ml-3 fa fa-search"></i></button>
                    </div>
                </form>
                <!-- End Help Form -->
            </div>
        </div>
    </div>
    <!-- End Help Center -->

    <!-- Help Promo Topics -->
    <div class="container g-pb-50">
        <div class="g-max-width-840 g-mt-minus-70 mx-auto">
            <div class="card-group d-block d-md-flex justify-content-lg-center g-mx-minus-15">
                <div class="card g-brd-none g-mx-15 g-mb-30">
                    <!-- Help Promo Topics -->
                    <div class="card-body u-block-hover u-shadow-v35 g-color-teal g-color-white--hover g-bg-teal-opacity-0_1 g-bg-teal--hover text-center rounded g-pa-30">
                <span class="u-icon-v1 u-icon-size--lg g-transition-0 mb-3">
                  <i class="icon-education-172 u-line-icon-pro"></i>
                </span>
                        <h2 class="h4">FAQ</h2>
                        <a class="u-link-v2" href="#"></a>
                    </div>
                    <!-- End Help Promo Topics -->
                </div>

                <div class="card g-brd-none g-mx-15 g-mb-30">
                    <!-- Help Promo Topics -->
                    <div class="card-body u-block-hover u-shadow-v35 g-color-purple g-color-white--hover g-bg-purple-opacity-0_1 g-bg-purple--hover text-center rounded g-pa-30">
                <span class="u-icon-v1 u-icon-size--lg g-transition-0 mb-3">
                  <i class="icon-education-007 u-line-icon-pro"></i>
                </span>
                        <h2 class="h4">Informasi Publik</h2>
                        <a class="u-link-v2" href="#"></a>
                    </div>
                    <!-- End Help Promo Topics -->
                </div>

                <div class="card g-brd-none g-mx-15 g-mb-30">
                    <!-- Help Promo Topics -->
                    <div class="card-body u-block-hover u-shadow-v35 g-color-blue g-color-white--hover g-bg-blue-opacity-0_1 g-bg-blue--hover text-center rounded g-pa-30">
                <span class="u-icon-v1 u-icon-size--lg g-transition-0 mb-3">
                  <i class="icon-education-143 u-line-icon-pro"></i>
                </span>
                        <h2 class="h4">Layanan Informasi</h2>
                        <a class="u-link-v2" href="#"></a>
                    </div>
                    <!-- End Help Promo Topics -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Help Promo Topics -->

    <!-- Help Content Topics -->
    <div class="container">
        <div class="g-max-width-840 mx-auto">
            @foreach ($searchResults as $result)
                <div class="u-block-hover u-shadow-v35 u-shadow-v37--hover g-brd-around g-brd-secondary-light-v2 rounded g-pos-rel g-pa-30 g-mb-30">
                    <div class="row align-items-center">
                        <div class="col-md-2 g-pr-0--md">
                <span class="u-icon-v1 u-icon-size--xl d-flex mx-auto">
                  <i class="{{$icons[$result->type]}} u-line-icon-pro"></i>
                </span>
                        </div>
                        <div class="col-md-9 g-pl-0--md">
                            <h2 class="g-color-primary--hover h4">{{$result->title}}</h2>
                            @if ($result->type == 'faq_questions')
                                <p class="g-color-text-light-v1">{!! $result->searchable->answer !!}</p>
                            @else
                                <p class="g-color-text-light-v1">{!! $result->searchable->description ?? 'Jenis Dokumen: ' . $result->searchable->extension !!}</p>
                            @endif
                        </div>
                        <div class="col-md-1">
                            <i class="d-flex float-md-right g-hidden-sm-down g-color-text-light-v1 g-color-primary--hover g-font-size-16 material-icons">arrow_forward</i>
                        </div>
                    </div>
                    @if ($result->type != 'faq_questions')
                        <a class="u-link-v2" href="{{$result->url}}" target="__blank"></a>
                    @endif
                </div>
            @endforeach

        </div>
    </div>
    <!-- End Help Content Topics -->
@stop