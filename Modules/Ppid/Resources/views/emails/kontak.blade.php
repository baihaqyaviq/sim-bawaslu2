@extends('ppid::emails.master')
@section('content')
    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff">
        <tr>
            <td align="center">
                <center>
                    <table class="w320" cellspacing="0" cellpadding="0" width="500">
                        <tr>
                            <td class="body-padding mobile-padding">

                                <p>
                                    Kepada Bawaslu Kab. Wonosobo <br>
                                    Berikut saya lampirkan identitas beserta pertanyaan/masukan yang saya ajukan.
                                </p>

                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Nama</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$name}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Pertanyaan</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body}}</td>
                                    </tr>
                                </table>
                                <p>atas kerjasamanya saya ucapkan terimakasih.</p>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
@stop