@extends('ppid::emails.master')
@section('content')
    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff">
        <tr>
            <td align="center">
                <center>
                    <table class="w320" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="body-padding mobile-padding">

                                <p>
                                    Kepada Bawaslu Kab. Wonosobo <br>
                                    Berikut saya lampirkan identitas beserta data pendukung permohonan informasi.
                                </p>

                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Nama</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->nama}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">No HP</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->no_hp}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Alamat</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->alamat}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Pekerjaan</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->refPekerjaan->nama}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Cara Memperoleh</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->caraMemperoleh->nama}}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" style="padding-bottom:20px;">Cara Mendapatkan</td>
                                        <td width="75%" style="padding-bottom:20px;">{{$body->caraMendapatkan->nama}}</td>
                                    </tr>
                                </table>
                                <strong>Rincian:</strong><br>
                                <p>{{$body->rincian}}</p>
                                <strong>Tujuan:</strong><br>
                                <p>{{$body->tujuan}}</p>
                                <p>atas kerjasamanya saya ucapkan terimakasih.</p>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
@stop