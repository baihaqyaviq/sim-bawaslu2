@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('ppid::admin.faq-category.actions.edit', ['name' => $faqCategory->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <faq-category-form
                :action="'{{ $faqCategory->resource_url }}'"
                :data="{{ $faqCategory->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('ppid::admin.faq-category.actions.edit', ['name' => $faqCategory->id]) }}
                    </div>

                    <div class="card-body">
                        @include('ppid::admin.faq-category.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </faq-category-form>

        </div>
    
</div>

@endsection