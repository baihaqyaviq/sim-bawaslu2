@extends('layouts.master')
@section('content')
    <div id="sub-navigation" class="g-pos-rel">
        <div class="js-sticky-block container u-secondary-navigation u-shadow-v19 g-bg-white g-line-height-1_3 g-py-20 g-pos-abs g-top-0 g-left-0 g-right-0"
             data-start-point="#sub-navigation" data-end-point="999999" data-type="responsive">
            <ul id="js-scroll-nav"
                class="nav flex-column flex-sm-row justify-content-sm-center text-center text-uppercase">
                <li class="nav-item g-brd-right--sm g-brd-gray-light-v4 g-color-primary--active">
                    <a href="#pkb" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi PKB</a>
                </li>
                <li class="nav-item g-brd-right--sm g-brd-gray-light-v4 g-color-primary--active">
                    <a href="#pdip" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi PDIP</a>
                </li>
                <li class="nav-item g-brd-right--sm g-brd-gray-light-v4 g-color-primary--active">
                    <a href="#ppp" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi PPP</a>
                </li>
                <li class="nav-item g-brd-right--sm g-brd-gray-light-v4 g-color-primary--active">
                    <a href="#gerindra" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi
                        Gerindra</a>
                </li>
                <li class="nav-item g-brd-right--sm g-brd-gray-light-v4 g-color-primary--active">
                    <a href="#pan" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi PAN</a>
                </li>
                <li class="nav-item g-color-primary--active">
                    <a href="#golkar" class="nav-link g-font-weight-500 g-font-size-default g-px-20">Fraksi Golkar</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="g-bg-secondary-gradient-v2">
        <div class="container g-pt-150 g-pb-170">
            <div class="g-max-width-960 mx-auto">
                <!-- Course Details -->
                <div id="pkb" class="row g-pb-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                        <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                          <i class="icon-education-092 u-line-icon-pro"></i>
                        </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <h3>Fraksi PKB</h3>
                            @include('partials/dewan', ['dewan' => $dewan['PKB']])
                        </div>
                    </div>
                </div>
                <!-- End Course Details -->

                <hr class="g-brd-secondary-light-v2 my-0">

                <!-- Career Paths -->
                <div id="pdip" class="row g-py-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                  <i class="icon-education-194 u-line-icon-pro"></i>
                </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <h3>Fraksi PDI Perjuangan</h3>
                            @include('partials/dewan', ['dewan' => $dewan['PDI PERJUANGAN']])
                        </div>
                    </div>
                </div>
                <!-- End Career Paths -->

                <hr class="g-brd-secondary-light-v2 my-0">

                <!-- Your Fee -->
                <div id="ppp" class="row g-py-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                  <i class="icon-finance-210 u-line-icon-pro"></i>
                </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <h3>Fraksi PPP</h3>
                            @include('partials/dewan', ['dewan' => $dewan['PPP']])
                        </div>
                    </div>
                </div>
                <!-- End Your Fee -->

                <hr class="g-brd-secondary-light-v2 my-0">

                <!-- Similar Courses -->
                <div id="gerindra" class="row g-py-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                  <i class="icon-education-056 u-line-icon-pro"></i>
                </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <div class="mb-4">
                                <h3>Fraksi Gerindra</h3>
                                @include('partials/dewan', ['dewan' => $dewan['GERINDRA']])
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Similar Courses -->

                <hr class="g-brd-secondary-light-v2 my-0">

                <div id="pan" class="row g-py-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                  <i class="icon-education-056 u-line-icon-pro"></i>
                </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <div class="mb-4">
                                <h3>Fraksi PAN</h3>
                                @include('partials/dewan', ['dewan' => $dewan['PAN']])
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="g-brd-secondary-light-v2 my-0">

                <div id="golkar" class="row g-py-80">
                    <div class="col-lg-1 g-mb-20 g-mb-0--lg">
                        <span class="u-icon-v1 u-icon-size--xl g-color-main g-bg-secondary g-font-size-30 rounded-circle">
                          <i class="icon-education-056 u-line-icon-pro"></i>
                        </span>
                    </div>

                    <div class="col-lg-11">
                        <div class="g-pl-15--lg">
                            <div class="mb-4">
                                <h3>Fraksi Golkar</h3>
                                @include('partials/dewan', ['dewan' => $dewan['GOLKAR']])
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="g-brd-secondary-light-v2 my-0">
            </div>
        </div>
    </div>
@stop

@push('styles')
    <link rel="stylesheet" href="{{url('vendor/slick-carousel/slick/slick.css') }}">
@endpush

@push('scripts')
    <script src="{{url('themes/default/js/components/hs.scroll-nav.js')}}"></script>
    <script src="{{url('themes/default/js/components/hs.sticky-block.js')}}"></script>
    <script src="{{url('vendor/slick-carousel/slick/slick.js') }}"></script>

    <script>
        $(window).on('load', function () {
            // initialization of HSScrollNav
            $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
                duration: 700,
                over: $('.u-secondary-navigation')
            });

            // initialization of sticky blocks
            $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
            // initialization of carousel
            $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');
        });
    </script>
@endpush
