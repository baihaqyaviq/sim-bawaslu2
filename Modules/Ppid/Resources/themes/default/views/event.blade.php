@extends('layouts.master')
@section('content')
    <div class="container g-pt-50 g-pb-30">
        <h2 class="g-mb-40">Agenda Kegiatan</h2>
        <div class="row">
            <div class="col-sm-6 col-lg-3 g-mb-30">
                <!-- Event Block -->
                <article class="u-block-hover u-shadow-v38">
                    <div class="g-min-height-300 g-bg-img-hero g-bg-cover g-bg-white-gradient-opacity-v1--after g-pos-rel" style="background-image: url(/themes/default/img-temp/400x500/img11.jpg);">
                        <div class="g-pos-abs g-bottom-0 g-left-0 g-right-0 g-z-index-1 g-pa-20">
                            <div class="d-flex justify-content-between">
                                <div class="mt-auto mb-2">
                                    <span class="d-block g-color-white g-line-height-1_4">9:00 am</span>
                                    <span class="d-block g-color-white g-line-height-1_4">12:00 pm</span>
                                </div>
                                <div class="text-center">
                                    <span class="g-color-white g-font-weight-500 g-font-size-40 g-line-height-0_7">24</span>
                                    <span class="g-color-white g-line-height-0_7">Nov</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="g-pa-25">
                        <h3 class="g-color-primary--hover g-font-size-18 mb-0">Core Research Facilities, Annual Symposium</h3>
                    </div>

                    <a class="u-link-v2 g-z-index-2" href="#"></a>
                </article>
                <!-- End Event Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30">
                <!-- Event Block -->
                <article class="u-block-hover u-shadow-v38">
                    <div class="g-min-height-300 g-bg-img-hero g-bg-cover g-bg-white-gradient-opacity-v1--after g-pos-rel" style="background-image: url(themes/default/img-temp/400x500/img12.jpg);">
                        <div class="g-pos-abs g-bottom-0 g-left-0 g-right-0 g-z-index-1 g-pa-20">
                            <div class="d-flex justify-content-between">
                                <div class="mt-auto mb-2">
                                    <span class="d-block g-color-white g-line-height-1_4">12:15 pm</span>
                                    <span class="d-block g-color-white g-line-height-1_4">12:45 pm</span>
                                </div>
                                <div class="text-center">
                                    <span class="g-color-white g-font-weight-500 g-font-size-40 g-line-height-0_7">07</span>
                                    <span class="g-color-white g-line-height-0_7">Dec</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="g-pa-25">
                        <h3 class="g-color-primary--hover g-font-size-18 mb-0">Ontario Ideas - Digital Rights: what are they, and why do we need them?</h3>
                    </div>

                    <a class="u-link-v2 g-z-index-2" href="#"></a>
                </article>
                <!-- End Event Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30">
                <!-- Event Block -->
                <article class="u-block-hover u-shadow-v38">
                    <div class="g-min-height-300 g-bg-img-hero g-bg-cover g-bg-white-gradient-opacity-v1--after g-pos-rel" style="background-image: url(themes/default/img-temp/400x500/img13.jpg);">
                        <div class="g-pos-abs g-bottom-0 g-left-0 g-right-0 g-z-index-1 g-pa-20">
                            <div class="d-flex justify-content-between">
                                <div class="mt-auto mb-2">
                                    <span class="d-block g-color-white g-line-height-1_4">03:00 pm</span>
                                    <span class="d-block g-color-white g-line-height-1_4">05:00 pm</span>
                                </div>
                                <div class="text-center">
                                    <span class="g-color-white g-font-weight-500 g-font-size-40 g-line-height-0_7">19</span>
                                    <span class="g-color-white g-line-height-0_7">Jan</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="g-pa-25">
                        <h3 class="g-color-primary--hover g-font-size-18 mb-0">USEAC Masterclass</h3>
                    </div>

                    <a class="u-link-v2 g-z-index-2" href="#"></a>
                </article>
                <!-- End Event Block -->
            </div>

            <div class="col-sm-6 col-lg-3 g-mb-30">
                <!-- Event Block -->
                <article class="u-block-hover u-shadow-v38">
                    <div class="g-min-height-300 g-bg-img-hero g-bg-cover g-bg-white-gradient-opacity-v1--after g-pos-rel" style="background-image: url(themes/default/img-temp/400x500/img14.jpg);">
                        <div class="g-pos-abs g-bottom-0 g-left-0 g-right-0 g-z-index-1 g-pa-20">
                            <div class="d-flex justify-content-between">
                                <div class="mt-auto mb-2">
                                    <span class="d-block g-color-white g-line-height-1_4">11:30 am</span>
                                    <span class="d-block g-color-white g-line-height-1_4">1:00 pm</span>
                                </div>
                                <div class="text-center">
                                    <span class="g-color-white g-font-weight-500 g-font-size-40 g-line-height-0_7">20</span>
                                    <span class="g-color-white g-line-height-0_7">Jan</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="g-pa-25">
                        <h3 class="g-color-primary--hover g-font-size-18 mb-0">2017 Unify College of the Arts Undergraduate Degree Show</h3>
                    </div>

                    <a class="u-link-v2 g-z-index-2" href="#"></a>
                </article>
                <!-- End Event Block -->
            </div>
        </div>
    </div>

    <div class="container g-brd-y g-brd-secondary-light-v2 g-pt-50 g-pb-60">
        <div class="row">
            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img5.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">Camino de Santiago: treading the ancient path to the end of the earth</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">13</span>
                                    <span class="g-line-height-0_7">Nov</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img6.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">Kopernik X Navicula: Music and Technology for Change</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">05</span>
                                    <span class="g-line-height-0_7">Dec</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>

            <div class="w-100"></div>

            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img10.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">From Euclid to the Computer: tracing a line through mathematics</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">09</span>
                                    <span class="g-line-height-0_7">Dec</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img9.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">Satellite Remote Sensing Summer School - SRSSS</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">17</span>
                                    <span class="g-line-height-0_7">Jan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>

            <div class="w-100"></div>

            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img7.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">Outside the Square: why so sad?</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">30</span>
                                    <span class="g-line-height-0_7">Jan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Event Listing -->
                <article class="u-shadow-v39">
                    <div class="row">
                        <div class="col-4">
                            <div class="g-min-height-170 g-bg-img-hero" style="background-image: url(themes/default/img-temp/200x200/img8.jpg);"></div>
                        </div>

                        <div class="col-8 g-min-height-170 g-flex-centered">
                            <div class="media align-items-center g-py-40">
                                <div class="d-flex col-8">
                                    <h3 class="g-line-height-1 mb-0"><a class="u-link-v5 g-color-main g-color-primary--hover g-font-size-18" href="#">Sydney Ideas - Luther and Dreams</a></h3>
                                </div>
                                <div class="media-body col-4">
                                    <span class="g-color-primary g-font-weight-500 g-font-size-40 g-line-height-0_7">24</span>
                                    <span class="g-line-height-0_7">Feb</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Event Listing -->
            </div>
        </div>
    </div>
@stop