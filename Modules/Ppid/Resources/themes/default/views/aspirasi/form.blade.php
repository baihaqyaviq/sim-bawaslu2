@extends('layouts.master')
{{--@extends('brackets/admin-ui::admin.layout.master')--}}
@section('content')
    <div class="container g-pt-50 g-pb-50 g-pb-130--lg">
        <div class="row align-items-lg-center no-gutters">
            <div class="col-lg-7 g-mb-50 g-mb-20--lg">
                <!-- Entry Fees & How to Apply -->
                <div class="u-shadow-v36" id="app">
                    <form-aspirasi
                            :action="'{{ url('admin/sim-aspirasis') }}'"
                            :dewan="{{ $dewan->toJson() }}"
                            :kategori="{{ $kategori->toJson() }}"
                            v-cloak>
                        <form method="post" @submit.prevent="onSubmit" action="/" novalidate>
                            <div class="g-bg-main-light-v2">
                                <header class="g-brd-bottom g-brd-white-opacity-0_1 text-center g-pa-30">
                                    <h3 class="h4 g-color-white g-font-primary g-font-weight-400 mb-0">Sampaikan
                                        Aspirasi Anda</h3>
                                </header>

                                <div class="g-pa-40">
                                    <p class="g-color-white-opacity-0_6 text-center g-mb-40">Aspirasi yang anda
                                        sampaikan ditujukan kepada wakil anda sesuai daerah pemilihan</p>

                                    <!-- Select Inputs -->
                                    <div class="row">
                                        <div class="col-6 g-mb-30">
                                            <!-- Start In -->
                                            <label class="g-color-white-opacity-0_5 g-font-size-15 mb-3">Kategori
                                                Aspirasi</label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-white-opacity-0_1 g-color-white-opacity-0_7 g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="2018"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up">
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="">2018 Spring
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="2018">2018 Fall
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="2019">2019 Spring
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="2019">2019 Fall
                                                </option>
                                            </select>
                                        {{--<multiselect
                                            v-model="form.kategori"
                                            :options="kategori"
                                            :multiple="false"
                                            track-by="id"
                                            label="nama_kategori"
                                            tag-placeholder="{{ __('Select kategori') }}"
                                            placeholder="{{ __('Nama kategori') }}">
                                        </multiselect>--}}
                                        <!-- End Start In -->
                                        </div>

                                        <div class="col-6 g-mb-30">
                                            <!-- I am -->
                                            <label class="g-color-white-opacity-0_5 g-font-size-15 mb-3">Anggota Dewan
                                                yang dituju </label>
                                            <select class="js-custom-select w-100 u-select-v2 g-brd-white-opacity-0_1 g-color-white-opacity-0_7 g-color-primary--hover g-bg-transparent text-left rounded g-pl-20 g-pr-40 g-py-10"
                                                    data-placeholder="a Canadian citizen"
                                                    data-open-icon="fa fa-angle-down"
                                                    data-close-icon="fa fa-angle-up">
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="">a Canadian citizen (including dual citizens)
                                                </option>
                                                <option class="g-brd-secondary-light-v2 g-color-text-light-v1 g-color-white--active g-bg-primary--active"
                                                        value="international">an International student
                                                </option>
                                            </select>
                                        {{--<multiselect
                                            v-model="form.caleg"
                                            :options="caleg"
                                            :multiple="false"
                                            track-by="id"
                                            label="nama_calon_full"
                                            tag-placeholder="{{ __('Select dewan') }}"
                                            placeholder="{{ __('Nama dewan') }}">
                                        </multiselect>--}}
                                        <!-- End I am -->
                                        </div>
                                    </div>

                                    <h3 class="g-color-white-opacity-0_5 g-font-primary g-font-weight-400 g-font-size-15 mb-3">
                                        Aspirasi</h3>

                                    <div class="row mb-0">
                                        <div class="col-12 g-mb-10">
                                            <textarea name="aspirasi" class="form-control form-control-md rounded-0"
                                                      rows="5" placeholder="Aspirasi Anda"></textarea>
                                            {{--                                            <wysiwyg v-model="form.aspirasi" v-validate="''" id="aspirasi" name="aspirasi" :config="mediaWysiwygConfig"></wysiwyg>--}}
                                        </div>
                                    </div>
                                    <!-- End Select Inputs -->

                                    <div class="form-group mb-0">
                                        <label class="g-mb-10 g-color-white-opacity-0_5 g-font-primary g-font-weight-400 g-font-size-15 mb-3">Lampiran</label>
                                        <input class="js-file-attachment" type="file" name="lampiran[]">
                                    </div>

                                    <p class="g-color-white-opacity-0_6 text-center mb-0">Aspirasi yang anda kirim akan
                                        divalidasi oleh petugas sebelum di teruskan ke Anggota Dewan yang dituju.</p>
                                </div>
                            </div>

                            <footer>
                                <button type="submit"
                                        class="btn btn-block g-color-blue g-color-white--hover g-bg-main-light-v1 g-font-size-16 text-center rounded-0 g-pa-20">
                                    Submit Aspirasi
                                    <i class="g-font-size-15 g-pos-rel g-top-4 ml-2 material-icons">arrow_forward</i>
                                </button>
                            </footer>
                        </form>
                    </form-aspirasi>
                </div>
                <!-- End Entry Fees & How to Apply -->
            </div>

            <div class="col-lg-5 g-mb-50 g-mb-20--lg">
                <!-- Facts & Figures -->
                <div class="u-shadow-v36">
                    <header class="g-brd-bottom g-brd-secondary-light-v2 g-bg-white text-center g-pa-30">
                        <h3 class="h4 g-font-primary g-font-weight-400 mb-0">Anggota Dewan</h3>
                        <h5 class="h5 g-font-primary mb-0">Daerah Pemilihan {{ Str::title($dapil->nama_dapil) }}</h5>
                    </header>

                    <div class="g-bg-white g-px-10 g-py-10">
                        <div class="js-carousel"
                         data-infinite="true"
                         data-slides-show="1"
                         data-slides-scroll="1"
                         data-arrows-classes="u-icon-v3 g-width-45 g-height-45 g-absolute-centered--y g-color-white g-color-white--hover g-bg-main g-bg-primary--hover rounded g-pa-12"
                         data-arrow-left-classes="fa fa-angle-left g-left-40"
                         data-arrow-right-classes="fa fa-angle-right g-right-40"
                         data-center-mode="true"
                         data-center-padding="10px"
                         data-responsive='[{
                           "breakpoint": 992,
                           "settings": {
                             "slidesToShow": 2,
                             "centerPadding": 30
                           }
                         }, {
                           "breakpoint": 768,
                           "settings": {
                             "slidesToShow": 2,
                             "centerPadding": 30
                           }
                         }, {
                           "breakpoint": 554,
                           "settings": {
                             "slidesToShow": 1,
                             "centerPadding": 30
                           }
                         }]'>

                        @foreach($dewan as $dewan)
                        <!-- Team Block -->
                        <figure class="js-slide">
                            <!-- Figure Image -->
                            <img class="w-50 g-relative-centered--x" src="{{ $dewan->foto}}" alt="Image Description">
                            <!-- End Figure Image-->

                            <!-- Figure Info -->
                            <div class="text-center g-bg-white g-pa-25">
                                <div class="g-mb-15">
                                    <h4 class="h5 g-color-black g-mb-5">({{$dewan->nomor_calon}}) {{$dewan->nama_calon}}</h4>
                                    <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">{{$dewan->parpol->nama_parpol}} ({{$dewan->parpol->singkatan}})</em>
                                </div>
                                <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>

                                <hr class="g-brd-gray-light-v4 my-4">

                                <!-- Figure Social Icons -->
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item g-mx-2">
                                        <a class="u-icon-v2 u-icon-size--xs g-color-facebook g-brd-facebook g-color-white--hover g-bg-facebook--hover rounded-circle" href="#" data-toggle="tooltip" data-original-title="Facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item g-mx-2">
                                        <a class="u-icon-v2 u-icon-size--xs g-color-twitter g-brd-twitter g-color-white--hover g-bg-twitter--hover rounded-circle" href="#" data-toggle="tooltip" data-original-title="Twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item g-mx-2">
                                        <a class="u-icon-v2 u-icon-size--xs g-color-google-plus g-brd-google-plus g-color-white--hover g-bg-google-plus--hover rounded-circle" href="#" data-toggle="tooltip" data-original-title="Google Plus">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- End Figure Social Icons -->
                            </div>
                            <!-- End Figure Info-->
                        </figure>
                        <!-- End Figure -->
                        @endforeach
                    </div>
                    </div>
                    <footer>
                        <a class="btn btn-block g-color-main g-color-primary--hover g-bg-secondary g-font-size-16 text-center rounded-0 g-pa-20"
                           href="{{route('front/dewan')}}">
                            Lihat Semua Anggota Dewan
                            <i class="g-font-size-15 g-pos-rel g-top-4 ml-2 material-icons">arrow_forward</i>
                        </a>
                    </footer>
                </div>
                <!-- End Facts & Figures -->
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <!-- JS Implementing Plugins -->
    <script src="{{url('vendor/jquery.filer/js/jquery.filer.min.js')}}"></script>
    <script src="{{url('themes/default/js/helpers/hs.focus-state.js')}}"></script>
    <script src="{{url('themes/default/js/components/hs.file-attachement.js')}}"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            // initialization of forms
            $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
            $.HSCore.helpers.HSFocusState.init();
        });
    </script>
@endpush