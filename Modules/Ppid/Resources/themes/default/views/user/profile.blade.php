@extends('layouts.master')
@section('content')
    <!-- Alumni Articles -->
    <div class="g-pos-rel">
        <div class="container">
            <div class="row justify-content-lg-between">
                <div class="col-md-4 col-lg-3 g-pt-100">
                    <h3 class="mb-4">Menu User</h3>

                    <!-- Links List -->
                    <ul class="nav list-unstyled mb-5 d-block" role="tablist" data-target="nav-profile"
                        data-tabs-mobile-type="slide-up-down" >
                        <li class="py-1 nav-item d-block">
                            <a class="nav-link d-block active u-link-v5 u-shadow-v35--active g-color-text-light-v1 g-color-main--hover g-color-primary--active g-bg-white--hover g-bg-white--active g-font-size-15 g-rounded-20 g-px-20 g-py-8"
                               data-toggle="tab" href="#edit-profile" role="tab">
                                <i class="align-middle mr-3 icon-hotel-restaurant-088 u-line-icon-pro"></i>
                                Edit Profil
                            </a>
                        </li>
                        <li class="py-1 nav-item d-block">
                            <a class="nav-link d-block u-link-v5 u-shadow-v35--active g-color-text-light-v1 g-color-main--hover g-color-primary--active g-bg-white--hover g-bg-white--active g-font-size-15 g-rounded-20 g-px-20 g-py-8"
                               data-toggle="tab" href="#setting-security" role="tab">
                                <i class="align-middle mr-3 icon-finance-258 u-line-icon-pro"></i>
                                Setting Security
                            </a>
                        </li>

                    </ul>
                    <!-- End Links List -->
                </div>

                <div class="col-md-8 g-pt-50 g-pt-50--md g-pb-70 tab-content" id="nav-profile">
                    <div class="tab-pane fade show active" id="edit-profile" role="tabpanel" data-parent="#nav-profile">
                        <h2 class="h4 g-font-weight-300">Manage your Name, ID and Email Addresses</h2>
                        <p>Below are name, email addresse, contacts and more on file for your account.</p>

                        <ul class="list-unstyled g-mb-30">
                            <!-- Name -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Name</strong>
                                    <span class="align-top">John Doe</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Name -->

                            <!-- Your ID -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Your
                                        ID</strong>
                                    <span class="align-top">FKJ-032440</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Your ID -->

                            <!-- Company Name -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Company
                                        name</strong>
                                    <span class="align-top">Htmlstream</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Company Name -->

                            <!-- Position -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Position</strong>
                                    <span class="align-top">Project Manager</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Position -->

                            <!-- Primary Email Address -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Primary
                                        email address</strong>
                                    <span class="align-top">john.doe@htmlstream.com</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Primary Email Address -->

                            <!-- Linked Account -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Linked
                                        account</strong>
                                    <span class="align-top">Facebook</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Linked Account -->

                            <!-- Website -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Website</strong>
                                    <span class="align-top">https://htmlstream.com</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Website -->

                            <!-- Phone Number -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Phone
                                        number</strong>
                                    <span class="align-top">(+123) 456 7890</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Phone Number -->

                            <!-- Office Number -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Office
                                        number</strong>
                                    <span class="align-top">(+123) 456 7891</span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Office Number -->

                            <!-- Address -->
                            <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                <div class="g-pr-10">
                                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Address</strong>
                                    <span class="align-top">795 Folsom Ave, Suite 600, San Francisco CA, US </span>
                                </div>
                                <span>
                        <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1"></i>
                      </span>
                            </li>
                            <!-- End Address -->
                        </ul>

                        <div class="text-sm-right">
                            <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#">Cancel</a>
                            <a class="btn u-btn-primary rounded-0 g-py-12 g-px-25" href="#">Save Changes</a>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="setting-security" role="tabpanel" data-parent="#nav-profile">
                        <h2 class="h4 g-font-weight-300">Security Settings</h2>
                        <p class="g-mb-25">Reset your password, change verifications and so on.</p>

                        <form>
                            <!-- Current Password -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Current
                                    password</label>
                                <div class="col-sm-9">
                                    <div class="input-group g-brd-primary--focus">
                                        <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                               type="password" placeholder="Current password">
                                        <div class="input-group-append">
                                            <span class="input-group-text g-bg-white g-color-gray-light-v1 rounded-0"><i
                                                        class="icon-lock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Current Password -->

                            <!-- New Password -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">New
                                    password</label>
                                <div class="col-sm-9">
                                    <div class="input-group g-brd-primary--focus">
                                        <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                               type="password" placeholder="New password">
                                        <div class="input-group-append">
                                            <span class="input-group-text g-bg-white g-color-gray-light-v1 rounded-0"><i
                                                        class="icon-lock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End New Password -->

                            <!-- Verify Password -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Verify
                                    password</label>
                                <div class="col-sm-9">
                                    <div class="input-group g-brd-primary--focus">
                                        <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                               type="password" placeholder="Verify password">
                                        <div class="input-group-append">
                                            <span class="input-group-text g-bg-white g-color-gray-light-v1 rounded-0"><i
                                                        class="icon-lock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Verify Password -->

                            <!-- Login Verification -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Login
                                    verification</label>
                                <div class="col-sm-9">
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                        <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        Verify login requests
                                    </label>
                                    <small class="d-block text-muted">You need to add a phone to your profile account to
                                        enable this feature.</small>
                                </div>
                            </div>
                            <!-- End Login Verification -->

                            <!-- Password Reset -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Password
                                    reset</label>
                                <div class="col-sm-9">
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                        <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        Require personal information to reset my password
                                    </label>
                                    <small class="d-block text-muted">When you check this box, you will be required to
                                        verify additional information before you can request a password reset with just
                                        your email address.</small>
                                </div>
                            </div>
                            <!-- End Password Reset -->

                            <!-- Save Password -->
                            <div class="form-group row g-mb-25">
                                <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Save
                                    password</label>
                                <div class="col-sm-9">
                                    <label class="form-check-inline u-check mx-0">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="savePassword"
                                               type="checkbox">
                                        <div class="u-check-icon-radio-v7">
                                            <i class="d-inline-block"></i>
                                        </div>
                                    </label>
                                    <small class="d-block text-muted">When you check this box, you will be saved
                                        automatically login to your profile account. Also, you will be always logged in
                                        all our services.</small>
                                </div>
                            </div>
                            <!-- End Save Password -->

                            <hr class="g-brd-gray-light-v4 g-my-25">

                            <div class="text-sm-right">
                                <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#">Cancel</a>
                                <a class="btn u-btn-primary rounded-0 g-py-12 g-px-25" href="#">Save Changes</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-5 col-lg-4 h-100 g-bg-secondary-gradient-v1 g-pos-abs g-top-0 g-left-0 g-z-index-minus-1"></div>
        </div>
    </div>
    <!-- End Alumni Articles -->
@stop

@push('scripts')
    <!-- JS Unify -->
    <script src="{{url('themes/default/js/components/hs.tabs.js') }}"></script>

    <!-- JS Plugins Init. -->
    <script>
        $(document).on('ready', function () {
            // initialization of tabs
            $.HSCore.components.HSTabs.init('[role="tablist"]');
        });

        $(window).on('resize', function () {
            setTimeout(function () {
                $.HSCore.components.HSTabs.init('[role="tablist"]');
            }, 200);
        });
    </script>
@endpush