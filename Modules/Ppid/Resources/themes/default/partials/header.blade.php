<!-- Header -->
<header id="js-header" class="u-header">
    <div class="u-header__section">
        <!-- Topbar -->
        <div class="g-bg-main g-pt-10 g-pb-10">
            <div class="container g-py-5">
                <ul class="list-inline d-flex align-items-center g-mb-0">
                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-brd-around g-brd-white-opacity-0_2 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 g-rounded-20 text-uppercase g-px-20 g-py-10"
                           href="{{route('aspirasi/create')}}">Sampaikan Aspirasi Anda</a>
                    </li>

                    <!-- Jump To -->
                    <li class="list-inline-item g-pos-rel ml-lg-auto">
                        <a id="jump-to-dropdown-invoker"
                           class="d-block d-lg-none u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-py-7"
                           href="#"
                           aria-controls="jump-to-dropdown"
                           aria-haspopup="true"
                           aria-expanded="false"
                           data-dropdown-event="hover"
                           data-dropdown-target="#jump-to-dropdown"
                           data-dropdown-type="css-animation"
                           data-dropdown-duration="0"
                           data-dropdown-hide-on-scroll="true"
                           data-dropdown-animation-in="fadeIn"
                           data-dropdown-animation-out="fadeOut">
                            Manu
                            <i class="g-ml-3 fa fa-angle-down"></i>
                        </a>
                        <ul id="jump-to-dropdown"
                            class="list-unstyled u-shadow-v39 g-brd-around g-brd-4 g-brd-white g-bg-secondary g-pos-abs g-left-0 g-z-index-99 g-mt-13"
                            aria-labelledby="jump-to-dropdown-invoker">
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('front/help')}}">Pusat Bantuan</a>
                            </li>
                            @auth
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('aspirasi/index')}}">Aspirasi Saya</a>
                            </li>
                            <li class="dropdown-item g-brd-bottom g-brd-2 g-brd-white g-px-0 g-py-2">
                                <a class="nav-link g-color-main g-color-primary--hover g-bg-secondary-dark-v2--hover g-font-size-default"
                                   href="{{route('profile/index')}}">User Profile</a>
                            </li>
                            @endauth
                            <li class="dropdown-item g-px-0 g-py-2">
                                @auth
                                    <a class="nav-link g-color-white g-bg-primary g-bg-primary-light-v1--hover g-font-size-default"
                                       href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('frm-logout1').submit();">Sign out</a>
                                    <form id="frm-logout1" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                @else
                                    <a class="nav-link g-color-white g-bg-primary g-bg-primary-light-v1--hover g-font-size-default"
                                       href="{{route('login')}}">Sign in</a>
                                @endauth
                            </li>
                        </ul>
                    </li>
                    <!-- End Jump To -->

                    <!-- Links -->
                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('front/help')}}">Pusat Bantuan</a>
                    </li>
                    @auth
                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('aspirasi/index')}}">Aspirasi Saya</a>
                    </li>
                    <li class="list-inline-item d-none d-lg-inline-block">
                        <a class="u-link-v5 g-color-white-opacity-0_7 g-color-white--hover g-font-size-12 text-uppercase g-px-10 g-py-15"
                           href="{{route('profile/index')}}">User Profile</a>
                    </li>
                    @endauth
                    <li class="list-inline-item d-none d-lg-inline-block">
                        @auth
                            <a class="u-link-v5 u-shadow-v19 g-color-black--hover g-bg-primary g-color-white g-bg-red--hover g-font-size-12 text-uppercase g-rounded-20 g-px-18 g-py-8 g-ml-10"
                               href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout2').submit();">Sign Out</a>
                            <form id="frm-logout2" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a class="u-link-v5 u-shadow-v19 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-12 text-uppercase g-rounded-20 g-px-18 g-py-8 g-ml-10"
                               href="{{ route('login') }}">Sign in</a>
                        @endauth
                    </li>
                    <!-- End Links -->

                    <!-- Search -->
                    <li class="list-inline-item g-ml-15--lg ml-auto">
                        <form id="searchform-1" class="input-group u-shadow-v19 g-brd-primary--focus g-rounded-20">
                            <input class="form-control g-brd-none g-bg-white g-font-size-12 text-uppercase g-rounded-left-20 g-pl-20 g-py-9"
                                   type="text" placeholder="Search here ...">
                            <button class="btn input-group-addon d-flex align-items-center g-brd-none g-color-white g-bg-primary g-bg-primary-light-v1--hover g-font-size-13 g-rounded-right-20 g-transition-0_2"
                                    type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </li>
                    <!-- End Search -->
                </ul>
            </div>
        </div>
        <!-- End Topbar -->

        <div class="container">
            <!-- Nav -->
            <nav class="js-mega-menu navbar navbar-expand-lg g-px-0 g-py-5 g-py-0--lg">
                <!-- Logo -->
                <a class="navbar-brand g-max-width-170 g-max-width-200--lg" href="home-page-1.html">
                    <img class="img-fluid g-hidden-lg-down" src="{{url('themes/default/img/logo/logo.png')}}"
                         alt="Logo">
                    <img class="img-fluid g-width-80 g-hidden-md-down g-hidden-xl-up"
                         src="{{url('themes/default/img/logo/logo-mini.png')}}" alt="Logo">
                    <img class="img-fluid g-hidden-lg-up" src="{{url('themes/default/img/logo/logo.png')}}" alt="Logo">
                </a>
                <!-- End Logo -->

                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0"
                        type="button"
                        aria-label="Toggle navigation"
                        aria-expanded="false"
                        aria-controls="navBar"
                        data-toggle="collapse"
                        data-target="#navBar">
                <span class="hamburger hamburger--slider g-px-0">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Navigation -->
                <div id="navBar" class="collapse navbar-collapse">
                    <ul class="navbar-nav align-items-lg-center g-py-30 g-py-0--lg ml-auto">
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('front/index')}}">
                                Home
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('front/dewan')}}">
                                Anggota Dewan
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('front/event')}}">
                                Kegiatan
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('front/galeri')}}">
                                Galeri
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-color-primary--hover g-font-size-15 g-font-size-17--xl g-px-15--lg g-py-10 g-py-30--lg"
                               href="{{route('front/statistik')}}">
                                Statistik
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Navigation -->
            </nav>
            <!-- End Nav -->
        </div>
    </div>
</header>
<!-- End Header -->