<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;

class RefAlasanKeberatan extends Model
{
    protected $table = 'ref_alasan_keberatan';
    protected $fillable = [];
}
