<?php


namespace Modules\Ppid\Entities;


use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class GoogleDriveFile implements Searchable
{
    private $file;

    public function __construct($files)
    {
        $this->file = $files;
    }

    public function getSearchResult(): SearchResult
    {
        $url = "https://drive.google.com/open?id={$this->file['basename']}";

        return new SearchResult(
            $this,
            Str::title($this->file['filename']),
            $url
        );
    }

    function __get($name)
    {
        if (isset($this->file[$name])) {
            return $this->file[$name];
        }
        return null;
    }

}