<?php

namespace Modules\Ppid\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes, Sluggable;

    public $table = 'categories';
    protected $fillable = [
        'name',
        'slug',
        'parent_id',
        'drive_id',
        'drive_parent_id',
        'is_google_drive',
        'is_published',
        'order'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/ppid-categories/' . $this->getKey());
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->with('children')
            ->orderBy('order', 'ASC')
            ->orderBy('name', 'ASC');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
