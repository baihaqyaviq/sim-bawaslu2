<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elifbyte\AdminGenerator\Generate\Traits\Uuid;

class Page extends Model
{
    use SoftDeletes;
use Uuid;

public $incrementing = false;

    protected $fillable = [
        'title',
        'slug',
        'views_count',
        'body',
        'template',
        'status',
        'icon',
    
    ];
    
    
    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/ppid-pages/'.$this->getKey());
    }
}
