<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';
    protected $fillable = [
        'title',
        'slug',
        'views_count',
        'body',
        'template',
        'status',
    ];
}
