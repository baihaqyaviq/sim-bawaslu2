<?php

namespace Modules\Ppid\Entities;

use App\RefPekerjaan;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class PermohonanInformasi extends Model implements HasMedia
{
    use ProcessMediaTrait;
    //use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $table = 'permohonan_informasi';
    protected $fillable = [
        'kategori_permohonan',
        'instansi',
        'nama',
        'alamat',
        'pekerjaan',
        'no_hp',
        'email',
        'rincian',
        'tujuan',
        'cara_memperoleh',
        'cara_mendapatkan',
    ];

    public function registerMediaCollections() {
        $this->addMediaCollection('ktp_pemohon_informasi');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->performOnCollections('ktp_pemohon_informasi');
    }

    public function refPekerjaan()
    {
        return $this->belongsTo(RefPekerjaan::class, 'pekerjaan', 'id');
    }

    public function kategoryPermohonan()
    {
        return $this->belongsTo(RefKategoriPermohonan::class, 'kategori_permohonan');
    }

    public function caraMemperoleh()
    {
        return $this->belongsTo(RefMemperolehInformasi::class, 'cara_memperoleh');
    }

    public function caraMendapatkan()
    {
        return $this->belongsTo(RefMendapatkanInformasi::class, 'cara_mendapatkan');
    }
}
