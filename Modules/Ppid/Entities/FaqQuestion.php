<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class FaqQuestion extends Model implements Searchable
{
    use SoftDeletes;

    public $table = 'faq_questions';

    protected $fillable = [
        'question',
        'answer',
        'category_id',
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */
    public function getSearchResult(): SearchResult
    {
        //$url = route('categories.show', $this->id);

        return new SearchResult(
            $this,
            $this->question
        );
    }


    public function getResourceUrlAttribute()
    {
        return url('/admin/ppid-faq-questions/'.$this->getKey());
    }

    public function category()
    {
        return $this->belongsTo(FaqCategory::class, 'category_id');
    }
}
