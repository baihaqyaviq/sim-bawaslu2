<?php

namespace Modules\Ppid\Entities;

use App\RefPekerjaan;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class PermohonanKeberatan extends Model implements HasMedia
{
    use ProcessMediaTrait;
    //use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $table = 'permohonan_keberatan';
    protected $fillable = [
        'instansi',
        'kategori_permohonan',
        'nama',
        'alamat',
        'pekerjaan',
        'no_hp',
        'email',
        'nama_kuasa',
        'alamat_kuasa',
        'no_hp_kuasa',
        'tujuan',
        'alasan',
    ];

    public function registerMediaCollections() {
        $this->addMediaCollection('dok_pemohon_keberatan');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->performOnCollections('dok_pemohon_keberatan');
    }

    public function refPekerjaan()
    {
        return $this->belongsTo(RefPekerjaan::class, 'pekerjaan', 'id');
    }

    public function kategoryPermohonan()
    {
        return $this->belongsTo(RefKategoriPermohonan::class, 'kategori_permohonan');
    }

    public function refAlasan()
    {
        return $this->belongsTo(RefAlasanKeberatan::class, 'alasan');
    }
}
