<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;

class RefMemperolehInformasi extends Model
{
    protected $table = 'ref_memperoleh_informasi';
    protected $fillable = [];
}
