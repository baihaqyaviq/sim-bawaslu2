<?php

namespace Modules\Ppid\Entities;

use Illuminate\Database\Eloquent\Model;

class RefMendapatkanInformasi extends Model
{
    protected $table = 'ref_mendapatkan_informasi';
    protected $fillable = [];
}
