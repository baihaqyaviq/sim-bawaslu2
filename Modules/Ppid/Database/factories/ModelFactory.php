<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\Article::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'short_text' => $faker->text(),
        'full_text' => $faker->text(),
        'category_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'views_count' => $faker->randomNumber(5),
        'slug' => $faker->unique()->slug,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\Category::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'slug' => $faker->unique()->slug,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\Tag::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'slug' => $faker->unique()->slug,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\FaqCategory::class, static function (Faker\Generator $faker) {
    return [
        'category' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\FaqQuestion::class, static function (Faker\Generator $faker) {
    return [
        'question' => $faker->text(),
        'answer' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        'category_id' => $faker->randomNumber(5),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Ppid\Entities\Page::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'views_count' => $faker->randomNumber(5),
        'body' => $faker->text(),
        'template' => $faker->sentence,
        'status' => $faker->randomNumber(5),
        'icon' => $faker->sentence,
        'updated_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
