<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('ppid-articles')->name('ppid-articles/')->group(static function () {
            Route::get('/', 'ArticlesController@index')->name('index');
            Route::get('/create', 'ArticlesController@create')->name('create');
            Route::post('/', 'ArticlesController@store')->name('store');
            Route::get('/{article}/edit', 'ArticlesController@edit')->name('edit');
            Route::post('/bulk-destroy', 'ArticlesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{article}', 'ArticlesController@update')->name('update');
            Route::delete('/{article}', 'ArticlesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('ppid-categories')->name('ppid-categories/')->group(static function () {
            Route::get('/', 'CategoriesController@index')->name('index');
            Route::get('/create', 'CategoriesController@create')->name('create');
            Route::post('/', 'CategoriesController@store')->name('store');
            Route::get('/{category}/edit', 'CategoriesController@edit')->name('edit');
            Route::post('/bulk-destroy', 'CategoriesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{category}', 'CategoriesController@update')->name('update');
            Route::delete('/{category}', 'CategoriesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('ppid-tags')->name('ppid-tags/')->group(static function () {
            Route::get('/', 'TagsController@index')->name('index');
            Route::get('/create', 'TagsController@create')->name('create');
            Route::post('/', 'TagsController@store')->name('store');
            Route::get('/{tag}/edit', 'TagsController@edit')->name('edit');
            Route::post('/bulk-destroy', 'TagsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{tag}', 'TagsController@update')->name('update');
            Route::delete('/{tag}', 'TagsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('ppid-faq-categories')->name('ppid-faq-categories/')->group(static function () {
            Route::get('/', 'FaqCategoriesController@index')->name('index');
            Route::get('/create', 'FaqCategoriesController@create')->name('create');
            Route::post('/', 'FaqCategoriesController@store')->name('store');
            Route::get('/{faqCategory}/edit', 'FaqCategoriesController@edit')->name('edit');
            Route::post('/bulk-destroy', 'FaqCategoriesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{faqCategory}', 'FaqCategoriesController@update')->name('update');
            Route::delete('/{faqCategory}', 'FaqCategoriesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function () {
        Route::prefix('ppid-faq-questions')->name('ppid-faq-questions/')->group(static function () {
            Route::get('/', 'FaqQuestionsController@index')->name('index');
            Route::get('/create', 'FaqQuestionsController@create')->name('create');
            Route::post('/', 'FaqQuestionsController@store')->name('store');
            Route::get('/{faqQuestion}/edit', 'FaqQuestionsController@edit')->name('edit');
            Route::post('/bulk-destroy', 'FaqQuestionsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{faqQuestion}', 'FaqQuestionsController@update')->name('update');
            Route::delete('/{faqQuestion}', 'FaqQuestionsController@destroy')->name('destroy');
        });
    });
});

### route front end
Route::domain(env('PPID_DOMAIN', 'ppid.wonosobo.bawaslu.go.id'))->name('ppid/')->group(function () {
    Route::get('/', 'PpidController@index')->name('home');
    Route::get('categories/{slug}', 'PpidController@show')->name('detail');

    //Route::get('categories/check_slug', 'CategoryController@check_slug')->name('categories/check_slug');
    //Route::get('categories/{slug}/{category}', 'CategoryController@show')->name('categories/show');
    //Route::get('tags/check_slug', 'TagController@check_slug')->name('tags/check_slug');
    //Route::get('tags/{slug}/{tag}', 'TagController@show')->name('tags/show');
    //Route::get('articles/check_slug', 'ArticleController@check_slug')->name('articles/check_slug');
    //Route::get('articles/{slug}/{article}', 'ArticleController@show')->name('articles/show');
    //Route::get('articles', 'ArticleController@index')->name('articles/index');
    Route::get('faq', 'FaqController@index')->name('faq/index');

    //Route::get('page/{slug}', 'PageController@index')->name('page');
    Route::get('profil', 'PageController@index')->name('page');
    Route::get('search', 'PageController@search')->name('search');
    Route::get('kontak', 'PageController@kontak')->name('get/kontak');
    Route::post('kontak', 'PageController@postKontak')->name('post/kontak');

    Route::get('permohonan-informasi', 'PageController@permohonanInformasi')->name('permohonan/informasi');
    Route::post('permohonan-informasi', 'PageController@postPermohonanInformasi')->name('post/permohonan/informasi');

    Route::get('permohonan-keberatan', 'PageController@permohonanKeberatan')->name('permohonan/keberatan');
    Route::post('permohonan-keberatan', 'PageController@postPermohonanKeberatan')->name('post/permohonan/keberatan');

    ### google drive
    //Route::get('drive/put', 'GoogleDriveController@putFile')->name('drive/put'); // retreive folders
    //Route::get('drive/put-existing', 'GoogleDriveController@uploadFile')->name('drive/put-existing'); // File upload form
    //Route::get('drive/list-files', 'GoogleDriveController@listFiles')->name('drive/list-files'); // File upload form
    //Route::get('drive/list-folders', 'GoogleDriveController@listFolders')->name('drive/list-folders'); // File upload form
    //Route::get('drive/put-larger-file', 'GoogleDriveController@putLargeFile')->name('drive/put-larger-file'); // File upload form
    //Route::get('drive/download-file', 'GoogleDriveController@downloadFile')->name('drive/download-file'); // File upload form
    //Route::get('drive/create-dir', 'GoogleDriveController@createDir')->name('drive/create-dir'); // File upload form
    //Route::get('drive/create-subdir', 'GoogleDriveController@createSubDir')->name('drive/create-subdir'); // File upload form
    //Route::get('drive/put-in-dir', 'GoogleDriveController@putIndDir')->name('drive/put-in-dir'); // File upload form
    //Route::get('drive/put-newest', 'GoogleDriveController@putNewest')->name('drive/put-newest'); // File upload form
    //Route::get('drive/delete-file', 'GoogleDriveController@deleteFile')->name('drive/delete-file'); // File upload form
    //Route::get('drive/delete-dir', 'GoogleDriveController@deleteDir')->name('drive/delete-dir'); // File upload form
    //Route::get('drive/rename-dir', 'GoogleDriveController@renameDir')->name('drive/rename-dir'); // File upload form
    //Route::get('drive/share', 'GoogleDriveController@share')->name('drive/share'); // File upload form
    //Route::get('drive/export-pdf', 'GoogleDriveController@exportToPdf')->name('drive/export-pdf'); // File upload form
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('ppid-pages')->name('ppid-pages/')->group(static function() {
            Route::get('/',                                             'PagesController@index')->name('index');
            Route::get('/create',                                       'PagesController@create')->name('create');
            Route::post('/',                                            'PagesController@store')->name('store');
            Route::get('/{page}/edit',                                  'PagesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PagesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{page}',                                      'PagesController@update')->name('update');
            Route::delete('/{page}',                                    'PagesController@destroy')->name('destroy');
        });
    });
});