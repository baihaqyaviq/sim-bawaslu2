<?php


namespace Modules\Ppid\SearchAspects;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Modules\Ppid\Entities\GoogleDriveFile;
use Spatie\Searchable\SearchAspect;

class LayananInformasi extends SearchAspect
{
    public function getResults(string $term): Collection
    {
        $contents = collect(Storage::cloud()->listContents('1MLkxVDu20S8JdOaWh4qeZHdPFpr23zzR', true));

        $files = $contents
            ->where('type', '=', 'file')
            ->filter(function ($item) use ($term) {
                return false !== stristr($item['filename'], $term);
            })->mapInto(GoogleDriveFile::class);

        return $files;
    }


}