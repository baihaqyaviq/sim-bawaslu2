<?php


namespace Modules\Ppid\SearchAspects;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Ppid\Entities\GoogleDriveFile;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchAspect;
use Spatie\Searchable\SearchResult;

class InformasiPublik extends SearchAspect
{
    public function getResults(string $term): Collection
    {
        //informasi public id
        $contents = collect(Storage::cloud()->listContents('1jf0u4wD6gYDQv6Jv9syRNGnkIQ8sdQ6X', true));

        $files = $contents
            ->where('type', '=', 'file')
            ->filter(function ($item) use ($term) {
                return false !== stristr($item['filename'], $term);
            })->mapInto(GoogleDriveFile::class);

        return $files;
    }

}