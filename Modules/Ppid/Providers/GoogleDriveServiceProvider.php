<?php

namespace Modules\Ppid\Providers;

use Illuminate\Support\ServiceProvider;

class GoogleDriveServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('google', function($app, $config) {
            $client = new \Google_Client();
            $client->setClientId($config['clientId']);
            $client->setClientSecret($config['clientSecret']);
            $client->refreshToken($config['refreshToken']);
            $service = new \Google_Service_Drive($client);

            $options = [
                'additionalFetchField' => 'description,hasThumbnail,thumbnailLink,imageMediaMetadata,iconLink'
            ];
            if(isset($config['teamDriveId'])) {
                $options['teamDriveId'] = $config['teamDriveId'];
            }

            //$adapter = new GoogleDriveAdapter($service, $config['folderId'], $options);

            /* Recommended cached adapter use */
             $adapter = new \League\Flysystem\Cached\CachedAdapter(
                 new GoogleDriveAdapter($service, $config['folderId'], $options),
                 new \League\Flysystem\Cached\Storage\Memory()
             );

            return new \League\Flysystem\Filesystem($adapter);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
