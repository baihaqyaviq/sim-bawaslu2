<?php

namespace Modules\Ppid\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Ppid\Composers\LayoutComposer;
use YAAP\Theme\Facades\Theme;

class PpidServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Ppid', 'Database/Migrations'));

        app('arrilot.widget-namespaces')->registerNamespace('ppid', '\Modules\Ppid\Widgets');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(GoogleDriveServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Ppid', 'Config/config.php') => config_path('ppid.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Ppid', 'Config/config.php'), 'ppid'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/ppid');

        $sourcePath = module_path('Ppid', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        /**
         * modified
         * @see https://github.com/nWidart/laravel-modules/issues/642
         */
        $this->loadViewsFrom(array_filter(array_merge(array_map(function ($path) {
            return $path . '/modules/ppid';
        }, \Config::get('view.paths')), [$sourcePath]), function ($path) {
            return file_exists($path);
        }), 'ppid');


        View::composer('ppid::layouts.master', LayoutComposer::class);

    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/ppid');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'ppid');
        } else {
            $this->loadTranslationsFrom(module_path('Ppid', 'Resources/lang'), 'ppid');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Ppid', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
