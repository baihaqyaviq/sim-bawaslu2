<?php

namespace Modules\Ppid\Http\Requests\Admin\Article;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.article.edit', $this->article);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['sometimes', 'string'],
            'short_text' => ['nullable', 'string'],
            'full_text' => ['nullable', 'string'],
            'category_id' => ['nullable', 'integer'],
            'views_count' => ['sometimes', 'integer'],
            'slug' => ['sometimes', Rule::unique('articles', 'slug')->ignore($this->article->getKey(), $this->article->getKeyName()), 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
