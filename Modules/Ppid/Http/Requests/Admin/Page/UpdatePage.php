<?php

namespace Modules\Ppid\Http\Requests\Admin\Page;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.page.edit', $this->page);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['sometimes', 'string'],
            'slug' => ['sometimes', Rule::unique('pages', 'slug')->ignore($this->page->getKey(), $this->page->getKeyName()), 'string'],
            'views_count' => ['sometimes', 'integer'],
            'body' => ['nullable', 'string'],
            'template' => ['nullable', 'string'],
            'status' => ['nullable', 'integer'],
            'icon' => ['nullable', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
