<?php

namespace Modules\Ppid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreKeberatanRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'instansi' => ['required', 'string'],
            'kategori_permohonan' => ['required', 'string'],
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'pekerjaan' => ['required', 'string'],
            'no_hp' => ['required', 'string'],
            'email' => ['required', 'string'],
            'nama_kuasa' => ['nullable'],
            'alamat_kuasa' => ['nullable'],
            'no_hp_kuasa' => ['nullable'],
            'tujuan' => ['required', 'string'],
            'alasan' => ['required', 'string'],
            'upload_doc' => 'required',
            'upload_doc.*' => 'required|mimes:png,jpg,jpeg,doc,docx,pdf|max:2048'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
