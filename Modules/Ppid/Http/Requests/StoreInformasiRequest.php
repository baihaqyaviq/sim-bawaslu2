<?php

namespace Modules\Ppid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInformasiRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kategori_permohonan' => ['required', 'string'],
            'instansi' => ['required', 'string'],
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'pekerjaan' => ['required', 'string'],
            'no_hp' => ['required', 'string'],
            'email' => ['required', 'string'],
            'rincian' => ['required', 'string'],
            'tujuan' => ['required', 'string'],
            'cara_memperoleh' => ['required', 'string'],
            'cara_mendapatkan' => ['required', 'string'],
            'ktp' => 'required|mimes:png,jpg,jpeg,doc,docx,pdf|max:2048',
            //'ktp_pemohon_informasi' => ['nullable']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
