<?php

namespace Modules\Ppid\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Ppid\Http\Requests\Admin\Article\BulkDestroyArticle;
use Modules\Ppid\Http\Requests\Admin\Article\DestroyArticle;
use Modules\Ppid\Http\Requests\Admin\Article\IndexArticle;
use Modules\Ppid\Http\Requests\Admin\Article\StoreArticle;
use Modules\Ppid\Http\Requests\Admin\Article\UpdateArticle;
use Modules\Ppid\Entities\Article;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ArticlesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexArticle $request
     * @return array|Factory|View
     */
    public function index(IndexArticle $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Article::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title', 'category_id', 'views_count'],

            // set columns to searchIn
            ['id', 'title', 'short_text', 'full_text', 'slug']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('ppid::admin.article.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.article.create');

        return view('ppid::admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreArticle $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreArticle $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Article
        $article = Article::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/ppid-articles'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/ppid-articles');
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @throws AuthorizationException
     * @return void
     */
    public function show(Article $article)
    {
        $this->authorize('admin.article.show', $article);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Article $article)
    {
        $this->authorize('admin.article.edit', $article);


        return view('ppid::admin.article.edit', [
            'article' => $article,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateArticle $request
     * @param Article $article
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateArticle $request, Article $article)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Article
        $article->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/ppid-articles'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/ppid-articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyArticle $request
     * @param Article $article
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyArticle $request, Article $article)
    {
        $article->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyArticle $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyArticle $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('articles')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
