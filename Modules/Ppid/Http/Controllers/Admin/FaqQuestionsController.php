<?php

namespace Modules\Ppid\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Ppid\Http\Requests\Admin\FaqQuestion\BulkDestroyFaqQuestion;
use Modules\Ppid\Http\Requests\Admin\FaqQuestion\DestroyFaqQuestion;
use Modules\Ppid\Http\Requests\Admin\FaqQuestion\IndexFaqQuestion;
use Modules\Ppid\Http\Requests\Admin\FaqQuestion\StoreFaqQuestion;
use Modules\Ppid\Http\Requests\Admin\FaqQuestion\UpdateFaqQuestion;
use Modules\Ppid\Entities\FaqQuestion;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FaqQuestionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFaqQuestion $request
     * @return array|Factory|View
     */
    public function index(IndexFaqQuestion $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(FaqQuestion::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'category_id'],

            // set columns to searchIn
            ['id', 'question', 'answer']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('ppid::admin.faq-question.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.faq-question.create');

        return view('ppid::admin.faq-question.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFaqQuestion $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFaqQuestion $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the FaqQuestion
        $faqQuestion = FaqQuestion::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/ppid-faq-questions'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/ppid-faq-questions');
    }

    /**
     * Display the specified resource.
     *
     * @param FaqQuestion $faqQuestion
     * @throws AuthorizationException
     * @return void
     */
    public function show(FaqQuestion $faqQuestion)
    {
        $this->authorize('admin.faq-question.show', $faqQuestion);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FaqQuestion $faqQuestion
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(FaqQuestion $faqQuestion)
    {
        $this->authorize('admin.faq-question.edit', $faqQuestion);


        return view('ppid::admin.faq-question.edit', [
            'faqQuestion' => $faqQuestion,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFaqQuestion $request
     * @param FaqQuestion $faqQuestion
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFaqQuestion $request, FaqQuestion $faqQuestion)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values FaqQuestion
        $faqQuestion->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/ppid-faq-questions'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/ppid-faq-questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFaqQuestion $request
     * @param FaqQuestion $faqQuestion
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFaqQuestion $request, FaqQuestion $faqQuestion)
    {
        $faqQuestion->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFaqQuestion $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFaqQuestion $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('faqQuestions')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
