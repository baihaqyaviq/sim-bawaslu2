<?php

namespace Modules\Ppid\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Ppid\Http\Requests\Admin\FaqCategory\BulkDestroyFaqCategory;
use Modules\Ppid\Http\Requests\Admin\FaqCategory\DestroyFaqCategory;
use Modules\Ppid\Http\Requests\Admin\FaqCategory\IndexFaqCategory;
use Modules\Ppid\Http\Requests\Admin\FaqCategory\StoreFaqCategory;
use Modules\Ppid\Http\Requests\Admin\FaqCategory\UpdateFaqCategory;
use Modules\Ppid\Entities\FaqCategory;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FaqCategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFaqCategory $request
     * @return array|Factory|View
     */
    public function index(IndexFaqCategory $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(FaqCategory::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'category'],

            // set columns to searchIn
            ['id', 'category']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('ppid::admin.faq-category.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.faq-category.create');

        return view('ppid::admin.faq-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFaqCategory $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFaqCategory $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the FaqCategory
        $faqCategory = FaqCategory::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/ppid-faq-categories'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/ppid-faq-categories');
    }

    /**
     * Display the specified resource.
     *
     * @param FaqCategory $faqCategory
     * @throws AuthorizationException
     * @return void
     */
    public function show(FaqCategory $faqCategory)
    {
        $this->authorize('admin.faq-category.show', $faqCategory);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FaqCategory $faqCategory
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(FaqCategory $faqCategory)
    {
        $this->authorize('admin.faq-category.edit', $faqCategory);


        return view('ppid::admin.faq-category.edit', [
            'faqCategory' => $faqCategory,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFaqCategory $request
     * @param FaqCategory $faqCategory
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFaqCategory $request, FaqCategory $faqCategory)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values FaqCategory
        $faqCategory->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/ppid-faq-categories'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/ppid-faq-categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFaqCategory $request
     * @param FaqCategory $faqCategory
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFaqCategory $request, FaqCategory $faqCategory)
    {
        $faqCategory->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFaqCategory $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFaqCategory $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('faqCategories')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
