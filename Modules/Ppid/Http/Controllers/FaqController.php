<?php

namespace Modules\Ppid\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Ppid\Entities\FaqCategory;

class FaqController extends Controller
{
    public function index()
    {
        $categories = FaqCategory::with('faqQuestions')->get();

        return view('ppid::public.faq', compact('categories'));
    }
}
