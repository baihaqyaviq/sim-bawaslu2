<?php

namespace Modules\Ppid\Http\Controllers;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Ppid\Entities\Tag;

class TagController extends Controller
{
    public function show($slug, Tag $tag)
    {
        $tag->loadCount('articles');

        $articles = $tag->articles()
            ->with('category')
            ->orderBy('id', 'desc')
            ->paginate(5);

        return view('ppid::public.tags.show', compact(['articles', 'tag']));
    }

    public function check_slug(Request $request)
    {
        $slug = SlugService::createSlug(Tag::class, 'slug', $request->input('name',''));

        return response()->json(['slug' => $slug]);
    }
}