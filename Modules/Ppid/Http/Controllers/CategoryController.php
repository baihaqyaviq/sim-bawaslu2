<?php

namespace Modules\Ppid\Http\Controllers;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Ppid\Entities\Category;

class CategoryController extends Controller
{
    public function show($slug, Category $category)
    {
        $category->loadCount('articles');

        $articles = $category->articles()
            ->paginate(5);

        return view('ppid::public.categories.show', compact(['category', 'articles']));
    }

    public function check_slug(Request $request)
    {
        $slug = SlugService::createSlug(Category::class, 'slug', $request->input('name', ''));

        return response()->json(['slug' => $slug]);
    }
}
