<?php

namespace Modules\Ppid\Http\Controllers;

use App\RefPekerjaan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Modules\Ppid\Entities\FaqQuestion;
use Modules\Ppid\Entities\Pages;
use Modules\Ppid\Entities\PermohonanInformasi;
use Modules\Ppid\Entities\PermohonanKeberatan;
use Modules\Ppid\Entities\RefAlasanKeberatan;
use Modules\Ppid\Entities\RefKategoriPermohonan;
use Modules\Ppid\Entities\RefMemperolehInformasi;
use Modules\Ppid\Entities\RefMendapatkanInformasi;
use Modules\Ppid\Http\Requests\KontakRequest;
use Modules\Ppid\Http\Requests\StoreInformasiRequest;
use Modules\Ppid\Http\Requests\StoreKeberatanRequest;
use Modules\Ppid\Mail\InformasiMailable;
use Modules\Ppid\Mail\KeberatanMailable;
use Modules\Ppid\Mail\KontakMailable;
use Modules\Ppid\SearchAspects\InformasiPublik;
use Modules\Ppid\SearchAspects\LayananInformasi;
use Spatie\Searchable\Search;

class PageController extends Controller
{
    public function index(Request $request)
    {
        //$page = Pages::where('slug', $slug)->firstOrFail();
        $pages = Pages::all();
        return view('ppid::public.page', compact('pages'));
    }

    public function search(Request $request)
    {
        $searchResults = (new Search())
            ->registerAspect(InformasiPublik::class)
            ->registerAspect(LayananInformasi::class)
            ->registerModel(FaqQuestion::class, ['question', 'answer'])
            ->perform($request->get('q'));

        $icons = [
            'faq_questions' => 'icon-education-172',
            'informasi_publiks' => 'icon-education-007',
            'layanan_informases' => 'icon-education-143',
        ];
        //dd($searchResults);

        return view('ppid::public.search_result', compact('searchResults', 'icons'));
    }

    public function permohonanInformasi()
    {
        $kategori = RefKategoriPermohonan::all()->pluck('nama', 'id');
        $info1 = RefMemperolehInformasi::all()->pluck('nama', 'id');
        $info2 = RefMendapatkanInformasi::all()->pluck('nama', 'id');
        $pekerjaan = RefPekerjaan::all()->pluck('nama', 'id');

        return view('ppid::public.permohonan_informasi', compact('kategori', 'info1', 'info2', 'pekerjaan'));
    }

    public function postPermohonanInformasi(StoreInformasiRequest $request)
    {

        $validated = $request->validated();

        if ($files = $validated['ktp']) {
            $destinationPath = storage_path('uploads\permohonan_informasi'); // upload path
            $fileName = Str::slug($validated['no_hp']) . "__ktp." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $fileName);
        }

        //dd($validated, $request->only([ "ktp_pemohon_informasi" => "ktp_pemohon_informasi"]));

        $created = PermohonanInformasi::create($validated);
        $created->addMedia($destinationPath . '\\' . $fileName)
            ->preservingOriginal()
            ->toMediaCollection('ktp_pemohon_informasi');

        //dd($validated, $created, $created->getMedia('ktp_pemohon_informasi'));

        Mail::to(env('MAIL_USERNAME'))->send(new InformasiMailable($created));
        return back()->withSuccess('Permohonan Infomasi berhasil dikirimkan ke email anda');
    }

    public function permohonanKeberatan()
    {
        $kategori = RefKategoriPermohonan::all()->pluck('nama', 'id');
        $alasan = RefAlasanKeberatan::all()->pluck('nama', 'id');
        $pekerjaan = RefPekerjaan::all()->pluck('nama', 'id');

        return view('ppid::public.permohonan_keberatan', compact('kategori', 'alasan', 'pekerjaan'));
    }

    public function postPermohonanKeberatan(StoreKeberatanRequest $request)
    {
        $validated = $request->validated();

        $created = PermohonanKeberatan::create($validated);

        if ($files = $validated['upload_doc']) {
            $destinationPath = storage_path('uploads\permohonan_keberatan'); // upload path

            foreach ($files as $file) {
                $fileName = $file->getClientOriginalName();
                $fileName = Str::slug($validated['no_hp']) . "_" . Str::slug($fileName, '_') . "." . $file->getClientOriginalExtension();
                $file->move($destinationPath, $fileName);

                $created->addMedia($destinationPath . '\\' . $fileName)
                    ->preservingOriginal()
                    ->toMediaCollection('dok_pemohon_keberatan');
            }
        }

        //dd($validated, $created, $created->getMedia('dok_pemohon_keberatan'));

        Mail::to(env('MAIL_USERNAME'))->send(new KeberatanMailable($created));
        return back()->withSuccess('Permohonan Keberatan berhasil dikirimkan ke email anda');
    }

    public function kontak()
    {
        return view('ppid::public.kontak');
    }

    public function postKontak(KontakRequest $request)
    {
        $validated = $request->validated();

        Mail::to(env('MAIL_USERNAME'))->send(new KontakMailable($validated));
        return back()->withSuccess('Pertanyaan/ masukan berhasil dikirimkan');
    }
}
