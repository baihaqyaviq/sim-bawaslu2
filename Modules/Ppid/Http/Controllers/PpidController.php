<?php

namespace Modules\Ppid\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Modules\Ppid\Entities\Category;

class PpidController extends Controller
{
    public function index()
    {
        /*$articleCategories = Category::withCount('articles')
            ->with(['articles' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->paginate(10);*/

        $documentCategories = Category::whereNull('parent_id')
            ->with('children')
            ->where('is_published', '1')
            ->orderBy('name', 'ASC')
            ->paginate(10);

        return view('ppid::public.home');
    }

    public function show($slug)
    {
        $files = [];
        $category = Category::where('slug', $slug)->first();
        $drive = $this->getFiles($category->drive_id);
        $fileGroup = [
            'dokumen' => 'pdf,doc,docx,ppt,pptx,xls,xlsx',
            'gambar' => 'jpg,jpeg,png,gif',
            'lainnya' => 'apk',
        ];

        if (isset($drive['file'])) {
            $files = collect($drive['file'])->mapToGroups(function ($item) use($fileGroup){
                $fileType = collect($fileGroup)->filter(function ($value, $key) use($item){
                    return in_array($item['extension'], explode(',', $value));
                })->keys()->first();

                return [$fileType => $item];
            });
        }

        $dir = $drive['dir'] ?? null;

        return view('ppid::public.show', compact('category', 'files', 'dir'));
    }

    private function getFiles($dirId)
    {
        $contents = collect(Storage::cloud()->listContents($dirId, true))
            ->sortBy('timestamp')
            ->sortBy('name');

        $files = $contents
            /*->map(function ($item) {
                $item['url'] = "https://drive.google.com/open?id={$item['basename']}";
                if ($item['type'] == 'dir') {
                    $item['children'] = $this->getFiles($item['basename']);
                }
                return $item;
            })*/
            ->mapToGroups(function ($item) {
                $item['url'] = "https://drive.google.com/open?id={$item['basename']}";
                return [$item['type'] => $item];
            });

        return $files;
    }
}
