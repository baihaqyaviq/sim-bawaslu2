<?php


namespace Modules\Ppid\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Ppid\Entities\PermohonanInformasi;

class InformasiMailable extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public function __construct(PermohonanInformasi $data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = $this->data->email;
        $name = $this->data->name;
        $subject = 'Permohonan Informasi';
        $media = $this->data->getMedia('ktp_pemohon_informasi')->first();

        return $this->view('ppid::emails.informasi')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'body' => $this->data,
            ])
            ->attach(storage_path('uploads/permohonan_informasi/'.$media->file_name));

    }
}