<?php


namespace Modules\Ppid\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Ppid\Entities\PermohonanKeberatan;

class KeberatanMailable extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public function __construct(PermohonanKeberatan $data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = $this->data->email;
        $name = $this->data->name;
        $subject = 'Permohonan Keberatan';
        $media = $this->data->getMedia('dok_pemohon_keberatan')->map(function ($item) {
            return storage_path('uploads\permohonan_keberatan\\' . $item->file_name);
        })->toArray();


        $email = $this->view('ppid::emails.keberatan')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'body' => $this->data,
            ]);

        foreach ($media as $media) {
            $email->attach($media);
        }
        return $email;
    }
}