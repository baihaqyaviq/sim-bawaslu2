<?php


namespace Modules\Ppid\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class KontakMailable extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = $this->data['email'];
        $name = $this->data['name'];
        $subject = 'Pertanyaan-' . $this->data['subject'];

        return $this->view('ppid::emails.kontak')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'name' => $this->data['name'],
                'body' => $this->data['message'],
            ]);

    }
}