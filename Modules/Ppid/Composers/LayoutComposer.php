<?php

namespace Modules\Ppid\Composers;

use Illuminate\View\View;
use Modules\Ppid\Entities\Article;
use Modules\Ppid\Entities\Category;
use Modules\Ppid\Entities\Tag;

class LayoutComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('popularArticles', Article::orderBy('views_count', 'desc')->take(6)->get());
        $view->with('latestArticles', Article::orderBy('id', 'desc')->take(6)->get());
        $view->with('popularTags', Tag::withCount('articles')->orderBy('articles_count', 'desc')->take(20)->get());
        $view->with('footerCategories', Category::where('is_published','1')->take(20)->get()->split(4));
    }
}