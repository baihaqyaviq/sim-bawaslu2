<?php

namespace Modules\Pemilu\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use App\Traits\WilayahTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tps extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait, WilayahTrait;

    protected $table = 'sim_tps';

    protected $fillable = [
        'nama',
        'alamat',
        'rt',
        'rw',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/pemilu-tps/' . $this->getKey());
    }

    /* ************************ RELATIONS ************************ */

}
