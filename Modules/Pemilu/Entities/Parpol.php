<?php

namespace Modules\Pemilu\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parpol extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    public $incrementing = false;

    protected $table = 'sim_parpol';

    protected $fillable = [
        'nomor_parpol',
        'nama_parpol',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/pemilu-parpols/' . $this->getKey());
    }
}
