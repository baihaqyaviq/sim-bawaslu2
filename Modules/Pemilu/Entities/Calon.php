<?php

namespace Modules\Pemilu\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calon extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    public $incrementing = false;

    protected $table = 'sim_calon';

    protected $fillable = [
        'nomor_calon',
        'nama_calon',
        'parpol_id',
        'dapil_id',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];
    protected $with = ['parpol', 'dapil'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/pemilu-calons/' . $this->getKey());
    }

    /* ************************ RELATIONS ************************ */

    /**
     * Relation to SimParpol
     *
     * @return BelongsTo
     */
    public function parpol()
    {
        return $this->belongsTo(Parpol::class, 'parpol_id', 'id');
    }


    /**
     * Relation to SimDapil
     *
     * @return BelongsTo
     */
    public function dapil()
    {
        return $this->belongsTo(Dapil::class, 'dapil_id', 'id');
    }


}
