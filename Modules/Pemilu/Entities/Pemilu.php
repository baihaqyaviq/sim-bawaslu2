<?php

namespace Modules\Pemilu\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use App\Traits\WilayahTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemilu extends Model
{
    use SoftDeletes, UserTrait, WilayahTrait;

    public $incrementing = false;

    protected $table = 'sim_pemilu';

    protected $fillable = [
        'nama_pemilu',
        'tahun',
        'keterangan',
        'id_wilayah',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url', 'full_nama'];
    protected $with = ['parent'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/pemilu-pemilus/' . $this->getKey());
    }

    public function getFullNamaAttribute()
    {
        return "{$this->nama_pemilu} {$this->tahun}";
    }

    /* ************************ RELATIONS ************************ */

    public function parent()
    {
        $dad = $this->belongsTo(Pemilu::class, 'parent_id', 'id')
            ->with('parent')
            ->select(['id', 'nama_pemilu']);

        return $dad;
    }

}
