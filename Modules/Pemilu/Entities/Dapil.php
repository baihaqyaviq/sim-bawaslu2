<?php

namespace Modules\Pemilu\Entities;

use App\Traits\PemiluTrait;
use App\Traits\UserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dapil extends Model
{
    use SoftDeletes, PemiluTrait, UserTrait;

    public $incrementing = false;

    protected $table = 'sim_dapil';

    protected $fillable = [
        'nama_dapil',
        'jml_kursi',
        'wilayah',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',

    ];

    protected $appends = ['resource_url'];

    protected $casts = [
        'wilayah' => 'array'
    ];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/pemilu-dapils/' . $this->getKey());
    }
}
