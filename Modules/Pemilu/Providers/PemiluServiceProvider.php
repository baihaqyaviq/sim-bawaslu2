<?php

namespace Modules\Pemilu\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PemiluServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Pemilu', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Pemilu', 'Config/config.php') => config_path('pemilu.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Pemilu', 'Config/config.php'), 'pemilu'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/pemilu');

        $sourcePath = module_path('Pemilu', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        /**
         * modified
         * @see https://github.com/nWidart/laravel-modules/issues/642
         */
        $this->loadViewsFrom(array_filter(array_merge(array_map(function ($path) {
            return $path . '/modules/pemilu';
        }, \Config::get('view.paths')), [$sourcePath]), function ($path) {
            return file_exists($path);
        }), 'pemilu');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/pemilu');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'pemilu');
        } else {
            $this->loadTranslationsFrom(module_path('Pemilu', 'Resources/lang'), 'pemilu');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Pemilu', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
