<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Pemilu\Entities\Pemilu::class, static function (Faker\Generator $faker) {
    return [
        'nama_pemilu' => $faker->sentence,
        'tahun' => $faker->date(),
        'keterangan' => $faker->text(),
        'id_wilayah' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Pemilu\Entities\Tps::class, static function (Faker\Generator $faker) {
    return [
        'nama' => $faker->sentence,
        'alamat' => $faker->sentence,
        'id_wilayah' => $faker->sentence,
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Pemilu\Entities\Parpol::class, static function (Faker\Generator $faker) {
    return [
        'nomor_parpol' => $faker->sentence,
        'nama_parpol' => $faker->sentence,
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Pemilu\Entities\Calon::class, static function (Faker\Generator $faker) {
    return [
        'nomor_calon' => $faker->sentence,
        'nama_calon' => $faker->sentence,
        'type' => $faker->sentence,
        'parpol_id' => $faker->randomNumber(5),
        'dapil_id' => $faker->randomNumber(5),
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Modules\Pemilu\Entities\Dapil::class, static function (Faker\Generator $faker) {
    return [
        'nama_dapil' => $faker->sentence,
        'pemilu_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});
