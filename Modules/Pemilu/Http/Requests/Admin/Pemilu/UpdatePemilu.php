<?php

namespace Modules\Pemilu\Http\Requests\Admin\Pemilu;


use App\Traits\WilayahRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePemilu extends FormRequest
{
    use WilayahRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.pemilu.edit', $this->pemilu);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent' => ['nullable'],
            'nama_pemilu' => ['nullable', 'string'],
            'tahun' => ['required', 'digits:4', 'integer', 'min:' . date('Y') ],
            'keterangan' => ['nullable', 'string'],
            'wilayah' => ['required'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getParentId()
    {
        if ($this->has('parent')) {
            return $this->get('parent')['id'];
        }
        return null;
    }

}
