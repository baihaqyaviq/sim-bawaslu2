<?php

namespace Modules\Pemilu\Http\Requests\Admin\Parpol;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateParpol extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.parpol.edit', $this->parpol);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nomor_parpol' => ['nullable', 'string'],
            'nama_parpol' => ['nullable', 'string'],
            'pemilu' => ['required'],
            'user_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
