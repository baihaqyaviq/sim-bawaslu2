<?php
namespace Modules\Pemilu\Http\Requests\Admin\Dapil;


use App\Traits\PemiluRequestTrait;
use App\Traits\WilayahRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreDapil extends FormRequest
{
    use PemiluRequestTrait, WilayahRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.dapil.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nama_dapil' => ['nullable', 'string'],
            'wilayah' => ['nullable'],
            'pemilu' => ['required'],

        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
