<?php

namespace Modules\Pemilu\Http\Requests\Admin\Calon;


use App\Traits\PemiluRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCalon extends FormRequest
{
    use PemiluRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.calon.edit', $this->calon);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nomor_calon' => ['nullable', 'string'],
            'nama_calon' => ['nullable', 'string'],
            'type' => ['nullable', 'string'],
            'parpol_id' => ['nullable', 'integer'],
            'dapil_id' => ['nullable', 'integer'],
            'pemilu' => ['required'],
            'user_id' => ['nullable', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getParpolId(){
        if ($this->has('parpol')){
            return $this->get('parpol')['id'];
        }
        return null;
    }

    public function getDapilId(){
        if ($this->has('dapil')){
            return $this->get('dapil')['id'];
        }
        return null;
    }
}
