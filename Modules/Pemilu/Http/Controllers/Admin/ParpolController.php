<?php

namespace Modules\Pemilu\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Pemilu\Http\Requests\Admin\Parpol\BulkDestroyParpol;
use Modules\Pemilu\Http\Requests\Admin\Parpol\DestroyParpol;
use Modules\Pemilu\Http\Requests\Admin\Parpol\IndexParpol;
use Modules\Pemilu\Http\Requests\Admin\Parpol\StoreParpol;
use Modules\Pemilu\Http\Requests\Admin\Parpol\UpdateParpol;
use Modules\Pemilu\Entities\Parpol;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ParpolController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexParpol $request
     * @return array|Factory|View
     */
    public function index(IndexParpol $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Parpol::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nomor_parpol', 'nama_parpol', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nomor_parpol', 'nama_parpol']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('pemilu::admin.parpol.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.parpol.create');

        return view('pemilu::admin.parpol.create',[
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreParpol $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreParpol $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Store the Parpol
        $parpol = Parpol::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/pemilu-parpols'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/pemilu-parpols');
    }

    /**
     * Display the specified resource.
     *
     * @param Parpol $parpol
     * @throws AuthorizationException
     * @return void
     */
    public function show(Parpol $parpol)
    {
        $this->authorize('admin.parpol.show', $parpol);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Parpol $parpol
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Parpol $parpol)
    {
        $this->authorize('admin.parpol.edit', $parpol);


        return view('pemilu::admin.parpol.edit', [
            'parpol' => $parpol,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateParpol $request
     * @param Parpol $parpol
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateParpol $request, Parpol $parpol)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();

        // Update changed values Parpol
        $parpol->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/pemilu-parpols'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/pemilu-parpols');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyParpol $request
     * @param Parpol $parpol
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyParpol $request, Parpol $parpol)
    {
        $parpol->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyParpol $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyParpol $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('parpols')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
