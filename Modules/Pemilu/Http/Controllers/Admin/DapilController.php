<?php

namespace Modules\Pemilu\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Pemilu\Http\Requests\Admin\Dapil\BulkDestroyDapil;
use Modules\Pemilu\Http\Requests\Admin\Dapil\DestroyDapil;
use Modules\Pemilu\Http\Requests\Admin\Dapil\IndexDapil;
use Modules\Pemilu\Http\Requests\Admin\Dapil\StoreDapil;
use Modules\Pemilu\Http\Requests\Admin\Dapil\UpdateDapil;
use Modules\Pemilu\Entities\Dapil;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DapilController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexDapil $request
     * @return array|Factory|View
     */
    public function index(IndexDapil $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Dapil::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nama_dapil', 'jml_kursi', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nama_dapil']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('pemilu::admin.dapil.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.dapil.create');

        return view('pemilu::admin.dapil.create',[
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDapil $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreDapil $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['wilayah'] = $request->getWilayahId();

        // Store the Dapil
        $dapil = Dapil::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/pemilu-dapils'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/pemilu-dapils');
    }

    /**
     * Display the specified resource.
     *
     * @param Dapil $dapil
     * @throws AuthorizationException
     * @return void
     */
    public function show(Dapil $dapil)
    {
        $this->authorize('admin.dapil.show', $dapil);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Dapil $dapil
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Dapil $dapil)
    {
        $this->authorize('admin.dapil.edit', $dapil);

        return view('pemilu::admin.dapil.edit', [
            'dapil' => $dapil,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDapil $request
     * @param Dapil $dapil
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateDapil $request, Dapil $dapil)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['wilayah'] = $request->getWilayahIds();

        // Update changed values Dapil
        $dapil->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/pemilu-dapils'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/pemilu-dapils');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyDapil $request
     * @param Dapil $dapil
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyDapil $request, Dapil $dapil)
    {
        $dapil->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyDapil $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyDapil $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('dapils')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
