<?php

namespace Modules\Pemilu\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RefWilayah;
use Modules\Pemilu\Http\Requests\Admin\Pemilu\BulkDestroyPemilu;
use Modules\Pemilu\Http\Requests\Admin\Pemilu\DestroyPemilu;
use Modules\Pemilu\Http\Requests\Admin\Pemilu\IndexPemilu;
use Modules\Pemilu\Http\Requests\Admin\Pemilu\StorePemilu;
use Modules\Pemilu\Http\Requests\Admin\Pemilu\UpdatePemilu;
use Modules\Pemilu\Entities\Pemilu;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PemiluController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexPemilu $request
     * @return array|Factory|View
     */
    public function index(IndexPemilu $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Pemilu::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nama_pemilu', 'tahun', 'id_wilayah', 'parent_id'],

            // set columns to searchIn
            ['id', 'nama_pemilu', 'keterangan', 'id_wilayah']
        );

        //dd($data->toArray());

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('pemilu::admin.pemilu.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.pemilu.create');

        return view('pemilu::admin.pemilu.create', [
            'parent' => Pemilu::whereNull('parent_id')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePemilu $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StorePemilu $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // relation field
        $sanitized['id_wilayah'] = $request->getWilayahId();
        $sanitized['parent_id'] = $request->getParentId();

        // Store the Pemilu
        $pemilu = Pemilu::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/pemilu-pemilus'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/pemilu-pemilus');
    }

    /**
     * Display the specified resource.
     *
     * @param Pemilu $pemilu
     * @throws AuthorizationException
     * @return void
     */
    public function show(Pemilu $pemilu)
    {
        $this->authorize('admin.pemilu.show', $pemilu);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pemilu $pemilu
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Pemilu $pemilu)
    {
        $this->authorize('admin.pemilu.edit', $pemilu);

        return view('pemilu::admin.pemilu.edit', [
            'pemilu' => $pemilu,
            'parent' => Pemilu::whereNull('parent_id')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePemilu $request
     * @param Pemilu $pemilu
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdatePemilu $request, Pemilu $pemilu)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // relation field
        $sanitized['id_wilayah'] = $request->getWilayahId();
        $sanitized['parent_id'] = $request->getParentId();

        // Update changed values Pemilu
        $pemilu->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/pemilu-pemilus'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/pemilu-pemilus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyPemilu $request
     * @param Pemilu $pemilu
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyPemilu $request, Pemilu $pemilu)
    {
        $pemilu->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyPemilu $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyPemilu $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('pemilus')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
