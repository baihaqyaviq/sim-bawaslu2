<?php

namespace Modules\Pemilu\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Dapil;
use Modules\Pemilu\Entities\Parpol;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Pemilu\Http\Requests\Admin\Calon\BulkDestroyCalon;
use Modules\Pemilu\Http\Requests\Admin\Calon\DestroyCalon;
use Modules\Pemilu\Http\Requests\Admin\Calon\IndexCalon;
use Modules\Pemilu\Http\Requests\Admin\Calon\StoreCalon;
use Modules\Pemilu\Http\Requests\Admin\Calon\UpdateCalon;
use Modules\Pemilu\Entities\Calon;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CalonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCalon $request
     * @return array|Factory|View
     */
    public function index(IndexCalon $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Calon::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nomor_calon', 'nama_calon', 'parpol_id', 'dapil_id', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nomor_calon', 'nama_calon']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('pemilu::admin.calon.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.calon.create');

        return view('pemilu::admin.calon.create', [
            'pemilu' => Pemilu::all(),
            'dapil' => Dapil::all(),
            'parpol' => Parpol::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCalon $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCalon $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['parpol_id'] = $request->getParpolId();
        $sanitized['dapil_id'] = $request->getDapilId();

        // Store the Calon
        $calon = Calon::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/pemilu-calons'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/pemilu-calons');
    }

    /**
     * Display the specified resource.
     *
     * @param Calon $calon
     * @throws AuthorizationException
     * @return void
     */
    public function show(Calon $calon)
    {
        $this->authorize('admin.calon.show', $calon);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Calon $calon
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Calon $calon)
    {
        $this->authorize('admin.calon.edit', $calon);


        return view('pemilu::admin.calon.edit', [
            'calon' => $calon,
            'pemilu' => Pemilu::all(),
            'dapil' => Dapil::all(),
            'parpol' => Parpol::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCalon $request
     * @param Calon $calon
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCalon $request, Calon $calon)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['parpol_id'] = $request->getParpolId();
        $sanitized['dapil_id'] = $request->getDapilId();

        // Update changed values Calon
        $calon->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/pemilu-calons'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/pemilu-calons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCalon $request
     * @param Calon $calon
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCalon $request, Calon $calon)
    {
        $calon->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCalon $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCalon $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('calons')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
