<?php

namespace Modules\Pemilu\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Pemilu\Entities\Pemilu;
use Modules\Pemilu\Http\Requests\Admin\Tps\BulkDestroyTps;
use Modules\Pemilu\Http\Requests\Admin\Tps\DestroyTps;
use Modules\Pemilu\Http\Requests\Admin\Tps\IndexTps;
use Modules\Pemilu\Http\Requests\Admin\Tps\StoreTps;
use Modules\Pemilu\Http\Requests\Admin\Tps\UpdateTps;
use Modules\Pemilu\Entities\Tps;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TpsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTps $request
     * @return array|Factory|View
     */
    public function index(IndexTps $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Tps::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'nama', 'alamat', 'rt', 'rw', 'id_wilayah', 'pemilu_id'],

            // set columns to searchIn
            ['id', 'nama', 'alamat', 'id_wilayah']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('pemilu::admin.tps.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.tps.create');

        return view('pemilu::admin.tps.create', [
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTps $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTps $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // relation field
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['id_wilayah'] = $request->getWilayahId();

        // Store the Tps
        $tps = Tps::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/pemilu-tps'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/pemilu-tps');
    }

    /**
     * Display the specified resource.
     *
     * @param Tps $tps
     * @throws AuthorizationException
     * @return void
     */
    public function show(Tps $tps)
    {
        $this->authorize('admin.tps.show', $tps);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tps $tps
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Tps $tps)
    {
        $this->authorize('admin.tps.edit', $tps);

        return view('pemilu::admin.tps.edit', [
            'tps' => $tps,
            'pemilu' => Pemilu::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTps $request
     * @param Tps $tps
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTps $request, Tps $tps)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // relation field
        $sanitized['pemilu_id'] = $request->getPemiluId();
        $sanitized['id_wilayah'] = $request->getWilayahId();

        // Update changed values Tps
        $tps->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/pemilu-tps'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/pemilu-tps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTps $request
     * @param Tps $tps
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTps $request, Tps $tps)
    {
        $tps->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTps $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTps $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('tps')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
