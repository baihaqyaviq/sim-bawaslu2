import AppListing from '@/app-components/Listing/AppListing';

Vue.component('tps-listing', {
    mixins: [AppListing]
});