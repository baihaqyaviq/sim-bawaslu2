import AppListing from '@/app-components/Listing/AppListing';

Vue.component('dapil-listing', {
    mixins: [AppListing]
});