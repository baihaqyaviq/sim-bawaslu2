<?php

return [
    'sidebar' => 'Pemilu',
    'pemilu' => [
        'title' => 'Pemilu',

        'actions' => [
            'index' => 'Pemilu',
            'create' => 'New Pemilu',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'parent' => 'Induk Pemilu',
            'nama_pemilu' => 'Nama pemilu',
            'tahun' => 'Tahun',
            'keterangan' => 'Keterangan',
            'id_wilayah' => 'Wilayah',
            
        ],
    ],

    'tps' => [
        'title' => 'Tps',

        'actions' => [
            'index' => 'Tps',
            'create' => 'New Tps',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'rt' => 'Rt',
            'rw' => 'Rw',
            'id_wilayah' => 'Wilayah',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    'parpol' => [
        'title' => 'Parpol',

        'actions' => [
            'index' => 'Parpol',
            'create' => 'New Parpol',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nomor_parpol' => 'Nomor parpol',
            'nama_parpol' => 'Nama parpol',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    'calon' => [
        'title' => 'Calon',

        'actions' => [
            'index' => 'Calon',
            'create' => 'New Calon',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nomor_calon' => 'Nomor calon',
            'nama_calon' => 'Nama calon',
            'type' => 'Type',
            'parpol_id' => 'Parpol',
            'dapil_id' => 'Dapil',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    'dapil' => [
        'title' => 'Dapil',

        'actions' => [
            'index' => 'Dapil',
            'create' => 'New Dapil',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'nama_dapil' => 'Nama dapil',
            'jml_kursi' => 'Jumlah kursi',
            'pemilu_id' => 'Pemilu',
            'user_id' => 'User',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];