<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nomor_parpol'), 'has-success': fields.nomor_parpol && fields.nomor_parpol.valid }">
    <label for="nomor_parpol" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.parpol.columns.nomor_parpol') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nomor_parpol" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nomor_parpol'), 'form-control-success': fields.nomor_parpol && fields.nomor_parpol.valid}" id="nomor_parpol" name="nomor_parpol" placeholder="{{ trans('pemilu::admin.parpol.columns.nomor_parpol') }}">
        <div v-if="errors.has('nomor_parpol')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nomor_parpol') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama_parpol'), 'has-success': fields.nama_parpol && fields.nama_parpol.valid }">
    <label for="nama_parpol" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.parpol.columns.nama_parpol') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama_parpol" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama_parpol'), 'form-control-success': fields.nama_parpol && fields.nama_parpol.valid}" id="nama_parpol" name="nama_parpol" placeholder="{{ trans('pemilu::admin.parpol.columns.nama_parpol') }}">
        <div v-if="errors.has('nama_parpol')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama_parpol') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>
