@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('pemilu::admin.parpol.actions.create'))

@section('body')

    <div class="container-xl">

        <div class="card">

            <parpol-form
                    :action="'{{ url('admin/pemilu-parpols') }}'"
                    :pemilu="{{$pemilu->toJson()}}"
                    v-cloak
                    inline-template>

                <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action"
                      novalidate>

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('pemilu::admin.parpol.actions.create') }}
                    </div>

                    <div class="card-body">
                        @include('pemilu::admin.parpol.components.form-elements')
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </parpol-form>

        </div>

    </div>


@endsection