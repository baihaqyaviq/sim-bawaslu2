@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('pemilu::admin.tps.actions.edit', ['name' => $tps->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <tps-form
                :action="'{{ $tps->resource_url }}'"
                :data="{{ $tps->toJson() }}"
                :pemilu="{{$pemilu->toJson()}}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('pemilu::admin.tps.actions.edit', ['name' => $tps->id]) }}
                    </div>

                    <div class="card-body">
                        @include('pemilu::admin.tps.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </tps-form>

        </div>
    
</div>

@endsection