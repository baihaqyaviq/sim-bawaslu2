<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama'), 'has-success': fields.nama && fields.nama.valid }">
    <label for="nama" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.nama') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama'), 'form-control-success': fields.nama && fields.nama.valid}" id="nama" name="nama" placeholder="{{ trans('pemilu::admin.tps.columns.nama') }}">
        <div v-if="errors.has('nama')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('alamat'), 'has-success': fields.alamat && fields.alamat.valid }">
    <label for="alamat" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.alamat') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.alamat" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('alamat'), 'form-control-success': fields.alamat && fields.alamat.valid}" id="alamat" name="alamat" placeholder="{{ trans('pemilu::admin.tps.columns.alamat') }}">
        <div v-if="errors.has('alamat')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('alamat') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('rt'), 'has-success': fields.rt && fields.rt.valid }">
    <label for="rt" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.rt') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.rt" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('rt'), 'form-control-success': fields.rt && fields.rt.valid}" id="rt" name="rt" placeholder="{{ trans('pemilu::admin.tps.columns.rt') }}">
        <div v-if="errors.has('rt')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('rt') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('rw'), 'has-success': fields.rw && fields.rw.valid }">
    <label for="rw" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.rw') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.rw" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('rw'), 'form-control-success': fields.rw && fields.rw.valid}" id="rw" name="rw" placeholder="{{ trans('pemilu::admin.tps.columns.rw') }}">
        <div v-if="errors.has('rw')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('rw') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('wilayah'), 'has-success': this.fields.wilayah && this.fields.wilayah.valid }">
    <label for="wilayah"
           class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.id_wilayah') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

        <multiselect v-model="form.wilayah"
                     id="ajax"
                     label="wilayah"
                     track-by="id"
                     placeholder="Type to search"
                     open-direction="bottom"
                     :options="wilayah"
                     :multiple="false"
                     :searchable="true"
                     :loading="isLoading"
                     :internal-search="false"
                     :options-limit="30"
                     :limit="3"
                     :limit-text="limitText"
                     :max-height="300"
                     :show-no-results="false"
                     :hide-selected="true"
                     @search-change="asyncFind">
            <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
        </multiselect>

        <div v-if="errors.has('wilayah')" class="form-control-feedback form-text" v-cloak>
            @{{errors.first('wilayah') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>

