@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('pemilu::admin.pemilu.actions.edit', ['name' => $pemilu->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <pemilu-form
                    :action="'{{ $pemilu->resource_url }}'"
                    :data="{{ $pemilu->toJson() }}"
                    :parent="{{ $parent->toJson() }}"
                    v-cloak
                    inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                      novalidate>

                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('pemilu::admin.pemilu.actions.edit', ['name' => $pemilu->id]) }}
                    </div>

                    <div class="card-body">
                        @include('pemilu::admin.pemilu.components.form-elements')
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </pemilu-form>

        </div>

    </div>

@endsection