<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('parent'), 'has-success': fields.parent && fields.parent.valid }">
    <label for="parent" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.parent') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.parent"
                :options="parent"
                :multiple="false"
                track-by="id"
                label="full_nama"
                tag-placeholder="{{ __('Select Pemilu Induk') }}"
                placeholder="{{ __('Nama Pemilu Induk') }}">
        </multiselect>

        <div v-if="errors.has('parent')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('parent') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('nama_pemilu'), 'has-success': fields.nama_pemilu && fields.nama_pemilu.valid }">
    <label for="nama_pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.nama_pemilu') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama_pemilu" v-validate="''" @input="validate($event)" class="form-control"
               :class="{'form-control-danger': errors.has('nama_pemilu'), 'form-control-success': fields.nama_pemilu && fields.nama_pemilu.valid}"
               id="nama_pemilu" name="nama_pemilu"
               placeholder="{{ trans('pemilu::admin.pemilu.columns.nama_pemilu') }}">
        <div v-if="errors.has('nama_pemilu')" class="form-control-feedback form-text" v-cloak>@{{
            errors.first('nama_pemilu') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('tahun'), 'has-success': fields.tahun && fields.tahun.valid }">
    <label for="tahun" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.tahun') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <input type="number" v-model="form.tahun" v-validate="''" @input="validate($event)" class="form-control"
               :class="{'form-control-danger': errors.has('tahun'), 'form-control-success': fields.tahun && fields.tahun.valid}"
               id="tahun" name="tahun" placeholder="{{ trans('pemilu::admin.pemilu.columns.tahun') }}">
        <div v-if="errors.has('tahun')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tahun') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('keterangan'), 'has-success': fields.keterangan && fields.keterangan.valid }">
    <label for="keterangan" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.keterangan') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.keterangan" v-validate="''" id="keterangan" name="keterangan"
                     :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('keterangan')" class="form-control-feedback form-text" v-cloak>@{{
            errors.first('keterangan') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('wilayah'), 'has-success': this.fields.wilayah && this.fields.wilayah.valid }">
    <label for="wilayah"
           class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.id_wilayah') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">

        <multiselect v-model="form.wilayah"
                     id="ajax"
                     label="wilayah"
                     track-by="id"
                     placeholder="Type to search"
                     open-direction="bottom"
                     :options="wilayah"
                     :multiple="false"
                     :searchable="true"
                     :loading="isLoading"
                     :internal-search="false"
                     :options-limit="30"
                     :limit="3"
                     :limit-text="limitText"
                     :max-height="300"
                     :show-no-results="false"
                     :hide-selected="true"
                     @search-change="asyncFind">
            <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
        </multiselect>

        <div v-if="errors.has('wilayah')" class="form-control-feedback form-text" v-cloak>@{{
            errors.first('wilayah') }}
        </div>
    </div>
</div>


