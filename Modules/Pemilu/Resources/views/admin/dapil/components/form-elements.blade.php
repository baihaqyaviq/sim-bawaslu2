<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama_dapil'), 'has-success': fields.nama_dapil && fields.nama_dapil.valid }">
    <label for="nama_dapil" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.dapil.columns.nama_dapil') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama_dapil" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama_dapil'), 'form-control-success': fields.nama_dapil && fields.nama_dapil.valid}" id="nama_dapil" name="nama_dapil" placeholder="{{ trans('pemilu::admin.dapil.columns.nama_dapil') }}">
        <div v-if="errors.has('nama_dapil')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama_dapil') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('wilayah'), 'has-success': this.fields.wilayah && this.fields.wilayah.valid }">
    <label for="wilayah"
           class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.pemilu.columns.id_wilayah') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.wilayah"
                     id="ajax"
                     label="wilayah"
                     track-by="id"
                     placeholder="Type to search"
                     open-direction="bottom"
                     :options="wilayah"
                     :multiple="true"
                     :searchable="true"
                     :loading="isLoading"
                     :internal-search="false"
                     :options-limit="30"
                     :limit="10"
                     :limit-text="limitText"
                     :max-height="300"
                     :show-no-results="false"
                     :hide-selected="true"
                     @search-change="asyncFind">
            <span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
        </multiselect>

        <div v-if="errors.has('wilayah')" class="form-control-feedback form-text" v-cloak>
            @{{errors.first('wilayah') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.tps.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>


