@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('pemilu::admin.dapil.actions.edit', ['name' => $dapil->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <dapil-form
                    :action="'{{ $dapil->resource_url }}'"
                    :data="{{ $dapil->toJson() }}"
                    :pemilu="{{$pemilu->toJson()}}"
                    v-cloak
                    inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action"
                      novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('pemilu::admin.dapil.actions.edit', ['name' => $dapil->id]) }}
                    </div>

                    <div class="card-body">
                        @include('pemilu::admin.dapil.components.form-elements')
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </dapil-form>

        </div>

    </div>

@endsection