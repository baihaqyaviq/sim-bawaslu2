<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nomor_calon'), 'has-success': fields.nomor_calon && fields.nomor_calon.valid }">
    <label for="nomor_calon" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.calon.columns.nomor_calon') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nomor_calon" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nomor_calon'), 'form-control-success': fields.nomor_calon && fields.nomor_calon.valid}" id="nomor_calon" name="nomor_calon" placeholder="{{ trans('pemilu::admin.calon.columns.nomor_calon') }}">
        <div v-if="errors.has('nomor_calon')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nomor_calon') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('nama_calon'), 'has-success': fields.nama_calon && fields.nama_calon.valid }">
    <label for="nama_calon" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.calon.columns.nama_calon') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.nama_calon" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('nama_calon'), 'form-control-success': fields.nama_calon && fields.nama_calon.valid}" id="nama_calon" name="nama_calon" placeholder="{{ trans('pemilu::admin.calon.columns.nama_calon') }}">
        <div v-if="errors.has('nama_calon')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('nama_calon') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('parpol_id'), 'has-success': fields.parpol_id && fields.parpol_id.valid }">
    <label for="parpol_id" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.calon.columns.parpol_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.parpol"
                :options="parpol"
                :multiple="false"
                track-by="id"
                label="nama_parpol"
                tag-placeholder="{{ __('Select parpol') }}"
                placeholder="{{ __('Nama parpol') }}">
        </multiselect>

        <div v-if="errors.has('parpol_id')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('parpol_id') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('dapil_id'), 'has-success': fields.dapil_id && fields.dapil_id.valid }">
    <label for="dapil_id" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.calon.columns.dapil_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.dapil"
                :options="dapil"
                :multiple="false"
                track-by="id"
                label="nama_dapil"
                tag-placeholder="{{ __('Select dapil') }}"
                placeholder="{{ __('Nama dapil') }}">
        </multiselect>

        <div v-if="errors.has('dapil_id')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('dapil_id') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('pemilu'), 'has-success': fields.pemilu && fields.pemilu.valid }">
    <label for="pemilu" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('pemilu::admin.calon.columns.pemilu_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
                v-model="form.pemilu"
                :options="pemilu"
                :multiple="false"
                track-by="id"
                label="nama_pemilu"
                tag-placeholder="{{ __('Select Pemilu') }}"
                placeholder="{{ __('Nama Pemilu') }}">
        </multiselect>

        <div v-if="errors.has('pemilu')" class="form-control-feedback form-text" v-cloak>
            @{{ errors.first('pemilu') }}
        </div>
    </div>
</div>