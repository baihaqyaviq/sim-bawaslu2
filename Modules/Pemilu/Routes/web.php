<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('pemilu-pemilus')->name('pemilu-pemilus/')->group(static function() {
            Route::get('/',                                             'PemiluController@index')->name('index');
            Route::get('/create',                                       'PemiluController@create')->name('create');
            Route::post('/',                                            'PemiluController@store')->name('store');
            Route::get('/{pemilu}/edit',                                'PemiluController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'PemiluController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{pemilu}',                                    'PemiluController@update')->name('update');
            Route::delete('/{pemilu}',                                  'PemiluController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('pemilu-tps')->name('pemilu-tps/')->group(static function() {
            Route::get('/',                                             'TpsController@index')->name('index');
            Route::get('/create',                                       'TpsController@create')->name('create');
            Route::post('/',                                            'TpsController@store')->name('store');
            Route::get('/{tps}/edit',                                    'TpsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TpsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{tps}',                                        'TpsController@update')->name('update');
            Route::delete('/{tps}',                                      'TpsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('pemilu-parpols')->name('pemilu-parpols/')->group(static function() {
            Route::get('/',                                             'ParpolController@index')->name('index');
            Route::get('/create',                                       'ParpolController@create')->name('create');
            Route::post('/',                                            'ParpolController@store')->name('store');
            Route::get('/{parpol}/edit',                                'ParpolController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ParpolController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{parpol}',                                    'ParpolController@update')->name('update');
            Route::delete('/{parpol}',                                  'ParpolController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('pemilu-calons')->name('pemilu-calons/')->group(static function() {
            Route::get('/',                                             'CalonController@index')->name('index');
            Route::get('/create',                                       'CalonController@create')->name('create');
            Route::post('/',                                            'CalonController@store')->name('store');
            Route::get('/{calon}/edit',                                 'CalonController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CalonController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{calon}',                                     'CalonController@update')->name('update');
            Route::delete('/{calon}',                                   'CalonController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
        Route::prefix('pemilu-dapils')->name('pemilu-dapils/')->group(static function() {
            Route::get('/',                                             'DapilController@index')->name('index');
            Route::get('/create',                                       'DapilController@create')->name('create');
            Route::post('/',                                            'DapilController@store')->name('store');
            Route::get('/{dapil}/edit',                                 'DapilController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'DapilController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{dapil}',                                     'DapilController@update')->name('update');
            Route::delete('/{dapil}',                                   'DapilController@destroy')->name('destroy');
        });
    });
});