const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.webpackConfig({
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': __dirname + '/resources/js/admin',
            '@Laporan': __dirname + '/Modules/Laporan/Resources/assets/js/admin',
            '@Pemilu': __dirname + '/Modules/Pemilu/Resources/assets/js/admin',
            '@Situng': __dirname + '/Modules/Situng/Resources/assets/js/admin',
            '@Ppid': __dirname + '/Modules/Ppid/Resources/assets/js/admin'
           // Do not delete me :) I'm used for auto-generation configuration
        },
    },
});

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js(['resources/js/admin/admin.js'], 'public/js')
    .sass('resources/sass/admin/admin.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
