import AppForm from '@/app-components/Form/AppForm';

Vue.component('article-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                short_text:  '' ,
                full_text:  '' ,
                category_id:  '' ,
                views_count:  '' ,
                slug:  '' ,
                
            }
        }
    }

});