import AppForm from '@/app-components/Form/AppForm';
import {APIService} from '@/app-components/Listing/APIService'

const apiService = new APIService();

Vue.component('dapil-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function() {
        return {
            wilayah: [],
            isLoading: false,
            form: {
                nama_dapil:  '' ,
                jml_kursi:  '' ,
                wilayah:  '' ,
                pemilu:  '' ,
            }
        }
    },
    methods: {
        limitText(count) {
            return `and ${count} other wilayah`
        },
        asyncFind(query) {
            if (query.length > 3) {
                this.isLoading = true;
                apiService.getAllWilayah(query).then(response => {
                    this.wilayah = response.data;
                    this.isLoading = false;
                })
            }
        },
        clearAll() {
            this.form.wilayah = [];
        }
    }

});