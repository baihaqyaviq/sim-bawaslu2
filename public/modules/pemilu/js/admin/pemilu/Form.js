import AppForm from '@/app-components/Form/AppForm';
import { APIService } from '@/app-components/Listing/APIService'

const apiService = new APIService();

Vue.component('pemilu-form', {
    mixins: [AppForm],
    props:['parent'],
    data: function () {
        return {
            // selectedWilayah: [],
            wilayah: [],
            isLoading: false,
            form: {
                parent: '',
                nama_pemilu: '',
                tahun: '',
                keterangan: '',
                wilayah: '',
            }
        }
    },
    methods: {
        limitText(count) {
            return `and ${count} other wilayah`
        },
        asyncFind(query) {
            if (query.length > 3) {
                this.isLoading = true;
                apiService.getWilayah(query).then(response => {
                    this.wilayah = response.data;
                    this.isLoading = false;
                })
            }
        },
        clearAll() {
            this.form.wilayah = [];
        }
    }

});