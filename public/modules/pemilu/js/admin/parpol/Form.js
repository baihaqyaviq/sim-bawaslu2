import AppForm from '@/app-components/Form/AppForm';

Vue.component('parpol-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function() {
        return {
            form: {
                nomor_parpol:  '' ,
                nama_parpol:  '' ,
                pemilu:  '' ,
            }
        }
    }

});