import AppForm from '@/app-components/Form/AppForm';

Vue.component('calon-form', {
    mixins: [AppForm],
    props: ['pemilu','dapil', 'parpol'],
    data: function() {
        return {
            form: {
                nomor_calon:  '' ,
                nama_calon:  '' ,
                parpol:  '' ,
                dapil:  '' ,
                pemilu:  '' ,
            }
        }
    }

});