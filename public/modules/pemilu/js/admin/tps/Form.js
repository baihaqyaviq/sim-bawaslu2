import AppForm from '@/app-components/Form/AppForm';
import {APIService} from '@/app-components/Listing/APIService'

const apiService = new APIService();

Vue.component('tps-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function () {
        return {
            wilayah: [],
            isLoading: false,
            form: {
                nama: '',
                alamat: '',
                pemilu: '',
                wilayah: '',
                rt: '',
                rw: ''
            }
        }
    },
    methods: {
        limitText(count) {
            return `and ${count} other wilayah`
        },
        asyncFind(query) {
            if (query.length > 3) {
                this.isLoading = true;
                apiService.getWilayah(query).then(response => {
                    this.wilayah = response.data;
                    this.isLoading = false;
                })
            }
        },
        clearAll() {
            this.form.wilayah = [];
        }
    }

});