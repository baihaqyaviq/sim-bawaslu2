import AppForm from '@/app-components/Form/AppForm';
import {APIService} from '@/app-components/Listing/APIService'

const apiService = new APIService();

Vue.component('pelanggaran-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function () {
        return {
            pengawasan: [],
            isLoading: false,
            form: {
                parent_id: '',
                pengawasan: '',
                peristiwa: '',
                tempat_kejadian: '',
                pelaku: '',
                waktu_kejadian: '',
                alamat_peristiwa: '',
                saksi_saksi: [{
                    nama: '',
                    alamat: ''
                }],
                alat_bukti: [{
                    keterangan: '',
                    photo: ''
                }],
                barang_bukti: [{
                    keterangan: '',
                    photo: ''
                }],
                uraian: '',
                pemilu: '',
            },
            mediaCollections: ['alat_bukti', 'barang_bukti']
        }
    },
    methods: {
        /**
         * override from base form
         * @returns {*}
         */
        getPostData: function getPostData() {
            var _this3 = this;

            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (Array.isArray(_this3.$refs[collection + '_uploader'])) {
                        _this3.$refs[collection + '_uploader'].forEach(function (item, index, arr) {
                            _this3.form[collection][index]['photo'] = _this3.$refs[collection + '_uploader'][index].getFiles();
                        });
                    } else {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }
            this.form['wysiwygMedia'] = this.wysiwygMedia;

            return this.form;
        },
        addSaksi() {
            this.form.saksi_saksi.push({
                nama: '',
                alamat: ''
            })
        },
        removeSaksi() {
            this.form.saksi_saksi.pop();
        },

        addAlatBukti() {
            this.form.alat_bukti.push({
                keterangan: '',
                photo: ''
            })
        },
        removeAlatBukti() {
            this.form.alat_bukti.pop();
        },

        addBarangBukti() {
            this.form.barang_bukti.push({
                keterangan: '',
                photo: ''
            })
        },
        removeBarangBukti() {
            this.form.barang_bukti.pop();
        },

        limitText(count) {
            return `and ${count} other pengawasan`
        },
        asyncFind(query) {
            if (query.length > 3) {
                this.isLoading = true;
                apiService.getPengawasan(query).then(response => {
                    this.pengawasan = response.data.data;
                    this.isLoading = false;
                })
            }
        },
        clearAll() {
            this.form.pengawasan = [];
        }
    }

});