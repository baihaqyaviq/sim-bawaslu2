import AppForm from '@/app-components/Form/AppForm';

Vue.component('panwas-form', {
    mixins: [AppForm],
    props: ['pemilu'],
    data: function () {
        return {
            form: {
                nik: '',
                nama: '',
                alamat: '',
                tempat_lahir: '',
                tanggal_lahir: '',
                jenis_kelamin: '',
                agama_id: '',
                status_kawin_id: '',
                no_spt: '',
                tgl_spt: '',
                tingkat: '',
                jabatan: '',
                pemilu: '',
                pendidikan: [{
                    nama_sekolah: '',
                    jenjang: '',
                    tahun_lulus: ''
                }],
                pekerjaan: [{
                    nama_pekerjaan: '',
                    tahun_masuk: '',
                    tahun_keluar: ''
                }],
                organisasi: [{
                    nama_organisasi: '',
                    tahun_awal: '',
                    tahun_akhir: ''
                }],
                penghargaan: [{
                    nama_penghargaan: '',
                    tahun: ''
                }],
                karyatulis: [{
                    nama_karya: '',
                    tahun: ''
                }],

            }
        }
    },
    methods: {
        addPendidikan() {
            this.form.pendidikan.push({
                nama_sekolah: '',
                jenjang: '',
                tahun_lulus: ''
            })
        },
        removePendidikan() {
            this.form.pendidikan.pop();
        },
        addPekerjaan() {
            this.form.pekerjaan.push({
                nama_pekerjaan: '',
                tahun_masuk: '',
                tahun_keluar: ''
            })
        },
        removePekerjaan() {
            this.form.pekerjaan.pop();
        },
        addOrganisasi() {
            this.form.organisasi.push({
                nama_organisasi: '',
                tahun_awal: '',
                tahun_akhir: ''
            })
        },
        removeOrganisasi() {
            this.form.organisasi.pop();
        },
        addPenghargaan() {
            this.form.penghargaan.push({
                nama_penghargaan: '',
                tahun: ''
            })
        },
        removePenghargaan() {
            this.form.penghargaan.pop();
        },
        addKaryatulis() {
            this.form.karyatulis.push({
                nama_karya: '',
                tahun: ''
            })
        },
        removeKaryatulis() {
            this.form.karyatulis.pop();
        },
    }

});